import os
import pickle
import random
import time

import gym
import numpy as np
import pandas as pd
import torch as th
import tqdm
import yaml

import util
from DSO import DSO
from Grid import Grid
from SpotMarket import SpotMarket

pd.options.mode.chained_assignment = None

if __name__ == '__main__':
    st = time.time()
    random.seed(0)
    np.random.seed(0)
    th.manual_seed(0)

    # Read out config.yaml
    with open("config.yaml", 'r') as stream:
        config = yaml.safe_load(stream)

    """specify the algorithm - MADDPG, DDPG, DQN
       specify the start and end days of the usecases"""
    alg = config["alg"]
    startDay = config["Times"]["startDay"]
    endDay = config["Times"]["endDay"]

    input_path = config["input_path"]
    result_path = config["result_path"]

    agentsDict, nameDict, networkDict, numAgents, loadingSeriesHP, chargingSeriesEV, \
    genSeriesPV, genSeriesWind, loadingSeriesDSM, nAgents = \
        util.agentsInit(alg, startDay, endDay, input_folder=input_path, numAgentsEachType=1)
    nameList = [agent.id for agent in agentsDict.values()]
    typeList = [agent.type for agent in agentsDict.values()]
    """Initialize the grid"""
    grid = Grid(numAgents, nameList, loadingSeriesHP, chargingSeriesEV, genSeriesPV, genSeriesWind, loadingSeriesDSM,
                input_folder=input_path, numCPU=20)
    """Initialize the spot market"""
    sm = SpotMarket(input_path)
    agentsList = [obj for name, obj in agentsDict.items()]
    sm.addParticipants(agentsList)
    """Initialize the DSO"""
    dso = DSO(grid, startDay, endDay)
    dso.addflexAgents(agentsList)

    """load the train Gym environment"""
    env = gym.make("gym_LocalFlexMarketEnv:LocalFlexMarketEnv-v0", **{'SpotMarket': sm, 'DSO': dso, 'alg': alg})
    env.startDay = startDay
    env.endDay = endDay

    """saving the names of the agents"""
    if not os.path.exists(result_path + alg):
        os.makedirs(result_path + alg)
    filename = result_path + alg + "/agentList.pkl"
    with open(filename, "wb") as f:
        pickle.dump(nameList, f)

    """Training parameters"""
    num_iterations = config["TrainingParameters"]["num_iterations"]
    gradient_steps = config["TrainingParameters"]["gradient_steps"]
    collect_steps_per_iteration = config["TrainingParameters"]["collect_steps_per_iteration"]
    eval_interval = config["TrainingParameters"]["eval_interval"]
    eval_steps = config["TrainingParameters"]["eval_steps"]
    checkpoint_interval = config["TrainingParameters"]["checkpoint_interval"]
    batch_size = config["TrainingParameters"]["batch_size"]
    assert eval_steps <= 24 * (endDay - startDay), "Resetting the environment while evaluation is not supported yet"

    """initialize the RL agents"""
    parameterDict = None
    util.RLAgentInit(env, networkDict, nameDict, nameList, alg, parameterDict, num_iterations, gradient_steps)
    RLAgents = util.getAgentObjects(networkDict, nameDict, nameList)

    """If specified the agents will be updated to the saved ones"""
    if config["Checkpoint"]:
        # Get the models and replay buffer from the latest saved model
        RLAgents, networkDict = util.restoreCheckpoint(result_path, len(env.agents), nameList, networkDict, nameDict, alg)

    """Lists to append and save data for analysis"""
    totalReturns, loss, lActor_loss, lCritic_loss, lActions = [], [], [], [], []
    episode_rewards = [0.0]  # sum of rewards for all agents
    agent_rewards = [[0.0] for _ in range(len(env.agents))]
    episode_number = 0
    t_start = time.time()

    max_mean_reward = -1e6
    max_evaluation_reward = [-1e6 for _ in range(len(env.agents))]

    obs_n = env.reset()
    for i, agent in enumerate(RLAgents):
        agent._last_obs = obs_n[i]

    if alg == 'DQN':
        for agent in RLAgents:
            agent._total_timesteps = num_iterations * collect_steps_per_iteration

    for num_iter in tqdm.trange(1, num_iterations + 1):
        episode_number += 1
        """Collect a few steps using collect_policy and save to the replay buffer."""
        for _ in range(collect_steps_per_iteration):
            obs_n, rew_n, done_n = util.collect_step(env, RLAgents, obs_n, alg)

            for i, rew in enumerate(rew_n):
                episode_rewards[-1] += rew
                agent_rewards[i][-1] += rew

            if any(done_n):
                # Reset environment
                # TODO: LG What happens if we comment that out?
                obs_n = env.reset()
                for i, agent in enumerate(RLAgents):
                    agent._last_obs = obs_n[i]

                # Reset action and parameter noise
                [agent.reset() for agent in RLAgents]

                # Append a new element to all lists in order to shift to the next index
                episode_rewards.append(0)
                for i in range(len(agent_rewards)):
                    agent_rewards[i].append(0)

        """so that the reset step with 0 rewards is not added to the replay buffer"""
        if eval_interval >= (endDay - startDay):
            if num_iter % (endDay - startDay) == 0:
                obs_n = env.reset()
                for i, agent in enumerate(RLAgents):
                    agent._last_obs = obs_n[i]

        if RLAgents[0].learning_starts < RLAgents[0].num_timesteps:
            if alg == 'MADDPG' or alg == 'DDPG':
                for _ in range(gradient_steps):
                    actor_loss, critic_loss, actions = \
                        util.trainRLAgents(batch_size, RLAgents, nameDict, networkDict, nameList, alg)
                    lActions.append(actions)
                    lActor_loss.append(actor_loss)
                    lCritic_loss.append(critic_loss)
            elif alg == 'DQN':
                for _ in range(gradient_steps):
                    _, critic_loss, _ = util.trainRLAgents(batch_size, RLAgents, nameDict, networkDict, nameList, alg)

        printFreq = 1
        mean_reward = np.mean(episode_rewards[-printFreq - 1:-1])
        max_mean_reward = max_mean_reward * int(max_mean_reward >= mean_reward) + \
                          mean_reward * int(max_mean_reward < mean_reward)
        if episode_number % printFreq == 0:
            print("steps: {}, episodes: {}, mean episode reward: {}, time: {}".format(
                num_iter * collect_steps_per_iteration, len(episode_rewards) - 1,
                mean_reward, round(time.time() - t_start, 3)))
            startTrain = True
            t_start = time.time()

        if num_iter % checkpoint_interval == 0 and RLAgents[0].learning_starts < RLAgents[0].num_timesteps:
            util.saveCheckpoint(RLAgents, nameList, alg, result_path)

        if num_iter % eval_interval == 0 and RLAgents[0].learning_starts < RLAgents[0].num_timesteps:
            reward, avg_action = util.compute_avg_return(env, RLAgents, alg, num_steps=eval_steps,
                                                         result_path=result_path)
            if any(reward >= max_evaluation_reward):
                for i, rew in enumerate(reward):
                    if rew >= max_evaluation_reward[i]:
                        max_evaluation_reward[i] = rew
                        RLAgents[i].save("Best_Model_Ag" + str(i))

            totalReturns.append(reward)
            print("-------------------------------------Evaluation-------------------------------------")
            print("Total cumulative market reward: ", reward.sum())
            print("Reward per Agent over episode: ", reward)
            print("Average Action taken: \n", avg_action)
            """save the results for further evaluation"""
            util.saveToFile(totalReturns, loss, lActor_loss, lCritic_loss, dso.congestionDetails, alg, result_path)
            # totalReturns, loss, lActor_loss, lCritic_loss = [], [], [], []
            obs_n = env.reset()
            for i, agent in enumerate(RLAgents):
                agent._last_obs = obs_n[i]

    duration = (time.time() - st) / 60
    print("---------------------------------------------%.2f minutes-----------------------------------\n\n" % duration)
    util.saveToFile([], [], [], [], dso.congestionDetails, alg, result_path)
    print("Maximaler mean reward beim Training: ", max_mean_reward)
