from EVehicle import EVehicle
from PVGen import PVG
from WindGen import WG
from HeatPump import HeatPump
from BatteryStorage import BatStorage
from DSM import DSM
from AgentNeuralNet import MADDPGAgent, DQN_Agent, DDPG_Agent
import numpy as np
import pandas as pd
import torch as th
from stable_baselines3 import DDPG, TD3, DQN
import os
import time
import re
import random
import pickle
from typing import Any, Dict, List
import gym


########################################################################################################################
# ----------------------- Setting up the power system and correspondingly the env--------------------------------------#
########################################################################################################################

def agentsInit(alg, startDay=0, endDay=365, requiredNodes=None, input_folder=None, numAgentsEachType=None):
    '''generation is -ve qty and load is +ve qty'''
    path = os.getcwd()
    if os.path.isfile(input_folder + "/RA_RD_Import_.pkl"):
        data = pd.read_pickle(input_folder + "/RA_RD_Import_.pkl")
    else:
        datapath = input_folder + "/RA_RD_Import_.csv"
        data = pd.read_csv(datapath, sep=';', comment='#', header=0, skiprows=0,
                           usecols=['Name', 'Location', 'Un'], error_bad_lines=False)
        data.columns = ['Name', 'Location', 'Un_kV']
        data.reset_index(inplace=True, drop=True)
        data['Un_kV'] = data['Un_kV'].apply(pd.to_numeric)
        data.to_pickle(input_folder + "/RA_RD_Import_.pkl")

    print('Reading Heat Pump series...')
    loadingSeriesHP = getHPSeries(input_folder)
    print('Reading EV series...')
    chargingSeriesEV, capacitySeriesEV, absenceSeriesEV, consumptionSeriesEV = getEVSeries(input_folder)
    relativePathCSV = input_folder + "/PV_Zeitreihe_nnf_1h.csv"
    relativePathPickle = input_folder + "/PV_Zeitreihe_nnf_1h.pkl"
    print('Reading PV series...')
    genSeriesPV = getGenSeries(relativePathCSV, relativePathPickle)
    relativePathCSV = input_folder + "/WEA_nnf_1h.csv"
    relativePathPickle = input_folder + "/WEA_nnf_1h.pkl"
    print('Reading Wind series...')
    genSeriesWind = getGenSeries(relativePathCSV, relativePathPickle)
    print('Reading DSM series...')
    loadingSeriesDSM = getDSMSeries(input_folder)

    """contains names of the respective agents"""
    homeStorageList = []
    for name in data['Name']:
        if name.endswith('_nsHeimSpeicherErsatzeinsp'):
            homeStorageList.append(name)
    startHour = startDay * 24
    endHour = endDay * 24 - 1
    agentsDict = {}

    """details of congested lines and the nodes having maximum sensitivities for them"""
    """days 33-39"""
    lines = ['L_Knoten_105_Stichabgang_R_Knoten_101_Stichabgang',
             'Knoten_79_Stich_boost_1',
             'Trafo_60',
             'L_Knoten_69_Stichabgang_R_Knoten_74_boost',
             'Knoten_79_Stich',
             'L_Knoten_94_Stichabgang_R_Knoten_79_boost_boost',
             'L_29R_39',
             'L_74R_39',
             'L_0R_36_boost_1',
             'L_0R_36',
             'L_Knoten_94_Stichabgang_R_Knoten_79_boost',
             'Trafo_14',
             'L_29R_39_boost_2',
             'L_33R_39_boost',
             'L_0R_36_boost',
             'L_29R_39_boost',
             'Trafo_19',
             'L_74R_39_boost_boost',
             'Knoten_79_Stich_boost',
             'L_Knoten_69_Stichabgang_R_Knoten_74_boost_1_boost',
             'Trafo_62',
             'L_Knoten_105_Stichabgang_R_Knoten_101_Stichabgang_boost',
             'L_Knoten_94_Stichabgang_R_Knoten_79',
             'L_33R_39',
             'L_33R_39_boost_1',
             'L_0R_39',
             'L_0R_39_boost',
             'L_33R_39_boost_2',
             'L_Knoten_69_Stichabgang_R_Knoten_74',
             'L_74R_39_boost',
             'L_29R_39_boost_1',
             'L_0R_39_boost_1_boost',
             'L_Knoten_69_Stichabgang_R_Knoten_74_boost_1',
             'L_0R_39_boost_1',
             'L_74R_39_boost_boost_1']
    requiredNodes = ['Standort_33',
                     'Standort_94',
                     'Standort_19',
                     'Standort_117',
                     'Standort_60',
                     'Standort_69',
                     'Standort_25',
                     'Standort_36',
                     'Standort_122',
                     'Standort_14',
                     'Standort_109',
                     'Standort_49',
                     'Standort_79',
                     'Standort_72',
                     'Standort_113',
                     'Standort_29',
                     'Standort_57',
                     'Standort_62',
                     'Standort_70',
                     'Standort_74']

    print("Hello World! Names not random and agentDict with endhour +1 ")

    """PV agents"""
    if requiredNodes:
        """get agents from the provided nodes"""
        names = getAgentsFromNodes(list(genSeriesPV.columns), requiredNodes)[:numAgentsEachType]
    else:
        """get random agents"""
        names = genSeriesPV.columns.to_series().sample(n=numAgentsEachType).values
    names = ['k24n6h87_18_nsPVErsatzeinsp']  # , 'k36n25h852_58_nsPVErsatzeinsp', 'k33n10h198_142_nsPVErsatzeinsp']
    # 'k49n16h456_129_nsPVErsatzeinsp', 'k37dea_247_solar']
    for name in names:
        name = re.search('k.*', name).group(0)
        loc, voltage_level, min_power, max_power = getAgentDetails(data, name)
        colName = genSeriesPV.filter(like=name).columns.values[0]
        agentsDict[name] = PVG(id=name, location=loc, minPower=min_power, maxPower=max_power,
                               voltageLevel=voltage_level, genSeries=genSeriesPV.loc[startHour:endHour + 1, colName],
                               startDay=startDay, endDay=endDay)

    """Wind agents"""
    if requiredNodes:
        """get agents from the provided nodes"""
        names = getAgentsFromNodes(list(genSeriesWind.columns), requiredNodes)[:numAgentsEachType]
    else:
        """get random agents"""
        names = genSeriesWind.columns.to_series().sample(n=numAgentsEachType).values
    names = ['k59dea_274_wea']  # , 'k69dea_262_wea', 'k79dea_267_wea', 'k86dea_255_wea', 'k57dea_249_wea']
    for name in names:
        name = re.search('k.*', name).group(0)
        loc, voltage_level, min_power, max_power = getAgentDetails(data, name)
        colName = genSeriesWind.filter(like=name).columns.values[0]
        agentsDict[name] = WG(id=name, location=loc, minPower=min_power, maxPower=max_power,
                              voltageLevel=voltage_level, genSeries=genSeriesWind.loc[startHour:endHour + 1, colName],
                              startDay=startDay, endDay=endDay)

    """Battery agents"""
    if requiredNodes:
        """get agents from the provided nodes"""
        names = getAgentsFromNodes(homeStorageList, requiredNodes)[:numAgentsEachType]
    else:
        """get random agents"""
        names = random.sample(homeStorageList, numAgentsEachType)
    if not names:
        names = []
        for n in requiredNodes[:numAgentsEachType]:
            names.append('k' + n[9:] + 'd_BatteryStorage')
    names = ['k33d_BatteryStorage']  # , 'k94d_BatteryStorage'] #, 'k117d_BatteryStorage',
    # 'k69d_BatteryStorage', 'k122d_BatteryStorage']
    for name in names:
        name = re.search('k.*', name).group(0)
        loc, voltage_level, min_power, max_power = getAgentDetails(data, name)
        agentsDict[name] = BatStorage(id=name, location=loc, minPower=min_power, maxPower=max_power,
                                      voltageLevel=voltage_level, maxCapacity=10 * max_power, marginalCost=30,
                                      startDay=startDay, endDay=endDay)
    """EV agents"""
    if requiredNodes:
        """get agents from the provided nodes"""
        names = getAgentsFromNodes(list(chargingSeriesEV.columns), requiredNodes)[:numAgentsEachType]
    else:
        """get random agents"""
        names = chargingSeriesEV.columns.to_series().sample(n=numAgentsEachType).values

    names = ['k60n22h742_330_nsEmobErsatzLast']  # , 'k33n10h176_716_nsEmobErsatzLast.1',

    # 'k49n16h409_663_nsEmobErsatzLast', 'k49n16h467_533_nsEmobErsatzLast.1']
    #  'k33n10h183_63_nsEmobErsatzLast.2',
    # 'k14n3h32_438_nsEmobErsatzLast.1', 'k14n3h32_438_nsEmobErsatzLast',
    # 'k49n16h409_663_nsEmobErsatzLast',  'k33n10h176_716_nsEmobErsatzLast.1',

    for name in names:
        name = re.search('k.*', name).group(0)
        colName = capacitySeriesEV.filter(like=name[:-5]).columns.values[0]

        consumption = consumptionSeriesEV.loc[startDay:endDay, colName]
        if names[0] == 'k60n22h742_330_nsEmobErsatzLast':
            consumption = consumption
            consumption.loc[330] = '[0.013]'
            consumption.loc[331] = '[0.0125]'
            consumption.loc[333] = '[0.0086]'
            consumption.loc[334] = '[0.0094]'
            consumption.loc[335] = '[0.0094, 0.011552]'
            consumption.loc[336] = '[0.0111]'
        agentsDict[name] = EVehicle(id=name, maxCapacity=capacitySeriesEV.loc[0, colName],
                                    absenceTimes=absenceSeriesEV.loc[startDay:endDay, colName],
                                    consumption=consumption, marginalCost=30, startDay=startDay, endDay=endDay)
    """Heat pump agents"""
    if requiredNodes:
        """get agents from the provided nodes"""
        names = getAgentsFromNodes(list(loadingSeriesHP.columns), requiredNodes)[:numAgentsEachType]
    else:
        """get random agents"""
        names = loadingSeriesHP.columns.to_series().sample(n=numAgentsEachType).values
    names = ['k14n3h29_73_nsWPErsatzlast_flex']  # , 'k49n16h429_161_nsWPErsatzlast_flex',
    # 'k36n25h832_128_nsWPErsatzlast_flex', 'k14n3h29_73_nsWPErsatzlast_flex',
    # 'k33n10h176_16_nsWPErsatzlast_flex']
    # names = ['k60n22h752_120_nsWPErsatzlast_flex', 'k49n16h429_161_nsWPErsatzlast_flex',
    #          'k49n16h454_135_nsWPErsatzlast_flex', 'k36n25h832_128_nsWPErsatzlast_flex',
    #          'k36n25h847_101_nsWPErsatzlast_flex', 'k49n16h459_46_nsWPErsatzlast_flex',
    #          'k33n10h176_16_nsWPErsatzlast_flex', 'k49n16h460_137_nsWPErsatzlast_flex',
    #          'k14n3h29_73_nsWPErsatzlast_flex', 'k33n10h172_117_nsWPErsatzlast_flex']
    for name in names:
        name = re.search('k.*', name).group(0)
        loc, voltage_level, min_power, max_power = getAgentDetails(data, name)
        colName = loadingSeriesHP.filter(like=name).columns.values[0]
        agentsDict[name] = HeatPump(id=name, maxPower=round(loadingSeriesHP.loc[:, colName].max(), 5),
                                    maxStorageLevel=25 * round(loadingSeriesHP.loc[:, colName].max(), 5),
                                    scheduledLoad=loadingSeriesHP.loc[startHour:endHour + 1, colName], marginalCost=30,
                                    voltageLevel=voltage_level, startDay=startDay, endDay=endDay)
    """DSM agents"""
    if requiredNodes:
        """get agents from the provided nodes"""
        names = getAgentsFromNodes(list(loadingSeriesDSM.columns), requiredNodes)[:numAgentsEachType]
    else:
        """get random agents"""
        names = loadingSeriesDSM.columns.to_series().sample(n=numAgentsEachType).values

    # names = ['k14n3b95_nsBetrieb_BusinessPeak_flex', 'k29n8b27_nsBetrieb_BusinessPeak_flex',
    #          'k49n16b64_nsBetrieb_produzierendes Gewerbe_flex', 'k60n22b62_nsBetrieb_BusinessPeak_flex',
    #          'k49n16b28_nsBetrieb_produzierendes Gewerbe_flex', 'k60n22b116_nsBetrieb_BusinessPeak_flex',
    #          'k60n22b68_nsBetrieb_produzierendes Gewerbe_flex', 'k62n27b100_nsBetrieb_BusinessPeak_flex',
    #          'k49n16b58_nsBetrieb_Business Base_flex', 'k14n3b59_nsBetrieb_produzierendes Gewerbe_flex',
    #          'k29n8b102_nsBetrieb_Gastronomie_flex', 'k36n25b39_nsBetrieb_Business Base_flex',
    #          'k33n10b119_nsBetrieb_Business Base_flex', 'k36n25b105_nsBetrieb_produzierendes Gewerbe_flex',
    #          'k60n22b133_nsBetrieb_Business Base_flex', 'k62n27b11_nsBetrieb_produzierendes Gewerbe_flex',
    #          'k19n9b122_nsBetrieb_Business Base_flex', 'k14n3b73_nsBetrieb_produzierendes Gewerbe_flex']

    names = ['k19n9b122_nsBetrieb_Business Base_flex']  # , 'k19n9b122_nsBetrieb_Business Base_flex']
    # 'k29n8b27_nsBetrieb_BusinessPeak_flex', 'k33n10b119_nsBetrieb_Business Base_flex',
    # 'k60n22b68_nsBetrieb_produzierendes Gewerbe_flex', 'k62n27b100_nsBetrieb_BusinessPeak_flex',
    # ]
    for name in names:
        name = re.search('k.*', name).group(0)
        # TODO check if the latest RA_RD_Import_ file contains maxpower
        colName = loadingSeriesDSM.filter(like=name).columns.values[0]
        agentsDict[name] = DSM(id=name, maxPower=round(loadingSeriesDSM.loc[:, colName].max(), 5),
                               scheduledLoad=loadingSeriesDSM.loc[startHour:endHour + 1, colName], marginalCost=30,
                               startDay=startDay, endDay=endDay)

    """Initialize the RL network for agents"""
    nameDict, networkDict, nAgents = RLNetworkInit(agentsDict, alg)

    return agentsDict, nameDict, networkDict, numAgentsEachType, loadingSeriesHP, chargingSeriesEV, genSeriesPV, \
           genSeriesWind, loadingSeriesDSM, nAgents


def getMaxSensiNodes(times, grid, reqLines):
    """get the nodes (standorts) with maximum sensitivity to the lines provided at the given time
        times: list of times to consider
        grid: the grid object
        reqLines: list of  lines for which the sensitivity is to be considered"""
    maxSensiNodes = []
    for time in times:
        sensi = grid.getSensitivity(time).loc[reqLines, :]
        maxSensiNodes.append(sensi.abs().idxmax(axis='columns').values)
    flattenedList = [item for sublist in maxSensiNodes for item in sublist]
    return list(set(flattenedList))


def getEVSeries(input_folder):
    path = os.getcwd()
    """EV timeseries"""
    if os.path.isfile(input_folder + "/EMob_Zeitreihe_nnf_1h.pkl"):
        chargingSeries = pd.read_pickle(input_folder + "/EMob_Zeitreihe_nnf_1h.pkl")
    else:
        datapath = input_folder + "/EMob_Zeitreihe_nnf_1h.csv"
        chargingSeries = pd.read_csv(datapath, sep=';', comment='#', header=0, skiprows=0, error_bad_lines=False,
                                     encoding='unicode_escape')
        chargingSeries.drop('Unnamed: 0', axis=1, inplace=True)
        chargingSeries = chargingSeries.apply(pd.to_numeric)
        chargingSeries.to_pickle(input_folder + "/EMob_Zeitreihe_nnf_1h.pkl")

    """EV capacity timeseries"""
    if os.path.isfile(input_folder + "/EMob_Zeitreihe_capacity.pkl"):
        capacitySeries = pd.read_pickle(input_folder + "/EMob_Zeitreihe_capacity.pkl")
    else:
        datapath = input_folder + "/EMob_Zeitreihe_capacity.csv"
        capacitySeries = pd.read_csv(datapath, sep=';', comment='#', header=0, skiprows=0, error_bad_lines=False,
                                     encoding='unicode_escape')
        capacitySeries.drop('Unnamed: 0', axis=1, inplace=True)
        capacitySeries = capacitySeries.apply(pd.to_numeric)
        capacitySeries.to_pickle(input_folder + "/EMob_Zeitreihe_capacity.pkl")

    """EV absence timeseries"""
    if os.path.isfile(input_folder + "/EMob_Zeitreihe_absence.pkl"):
        absenceSeries = pd.read_pickle(input_folder + "/EMob_Zeitreihe_absence.pkl")
    else:
        datapath = input_folder + "/EMob_Zeitreihe_absence.csv"
        absenceSeries = pd.read_csv(datapath, sep=';', comment='#', header=0, skiprows=0, error_bad_lines=False,
                                    encoding='unicode_escape')
        absenceSeries.drop('Unnamed: 0', axis=1, inplace=True)
        absenceSeries.columns = capacitySeries.columns
        absenceSeries.to_pickle(input_folder + "/EMob_Zeitreihe_absence.pkl")

    """EV consumption timeseries"""
    if os.path.isfile(input_folder + "/EMob_Zeitreihe_consumption.pkl"):
        consumptionSeries = pd.read_pickle(input_folder + "/EMob_Zeitreihe_consumption.pkl")
    else:
        datapath = input_folder + "/EMob_Zeitreihe_consumption.csv"
        consumptionSeries = pd.read_csv(datapath, sep=';', comment='#', header=0, skiprows=0, error_bad_lines=False,
                                        encoding='unicode_escape')
        consumptionSeries.drop('Unnamed: 0', axis=1, inplace=True)
        consumptionSeries.to_pickle(input_folder + "/EMob_Zeitreihe_consumption.pkl")
    return chargingSeries, capacitySeries, absenceSeries, consumptionSeries


def getHPSeries(input_folder):
    """Heat pump loading timeseries"""
    path = os.getcwd()
    if os.path.isfile(input_folder + "/WP_Zeitreihe_nnf.pkl"):
        loadingSeries = pd.read_pickle(input_folder + "/WP_Zeitreihe_nnf.pkl")
    else:
        datapath = input_folder + "/WP_Zeitreihe_nnf.csv"
        loadingSeries = pd.read_csv(datapath, sep=';', comment='#', header=0, skiprows=0, error_bad_lines=False,
                                    encoding='unicode_escape')
        """cleaning the dataframe"""
        loadingSeries.drop('NNF', axis=1, inplace=True)
        loadingSeries.drop(loadingSeries.index[0], inplace=True)
        # TODO check if there is this extra row in every HP series
        loadingSeries.drop(loadingSeries.index[8760], inplace=True)
        loadingSeries.reset_index(drop=True, inplace=True)
        loadingSeries = editNames(loadingSeries)
        loadingSeries = loadingSeries.apply(pd.to_numeric)
        loadingSeries.to_pickle(input_folder + "/WP_Zeitreihe_nnf.pkl")
    return loadingSeries


def getDSMSeries(input_folder):
    """DSM scheduled load timeseries"""
    path = os.getcwd()
    if os.path.isfile(input_folder + "/ang_Kunden_GHD_nnf_1h.pkl"):
        loadingSeries = pd.read_pickle(input_folder + "/ang_Kunden_GHD_nnf_1h.pkl")
    else:
        datapath = input_folder + "/ang_Kunden_GHD_nnf_1h.csv"
        loadingSeries = pd.read_csv(datapath, sep=';', comment='#', header=0, skiprows=0, error_bad_lines=False,
                                    encoding='unicode_escape')
        """cleaning the dataframe"""
        loadingSeries.drop('NNF', axis=1, inplace=True)
        loadingSeries.drop(loadingSeries.index[0], inplace=True)
        loadingSeries.reset_index(drop=True, inplace=True)
        loadingSeries = editNames(loadingSeries)
        loadingSeries = loadingSeries.apply(pd.to_numeric)
        loadingSeries.to_pickle(input_folder + "/ang_Kunden_GHD_nnf_1h.pkl")
    return loadingSeries


def editNames(loadingSeries):
    """helper function to edit the names for compatibility with new naming convention"""
    if not loadingSeries.columns[0].endswith('_flex'):
        colNames = []
        for name in loadingSeries:
            colNames.append(name + '_flex')
        loadingSeries.columns = colNames
    return loadingSeries


def getGenSeries(relativePathCSV, relativePathPickle):
    """data: dataframe containing the data of all agents in the grid
        name: name of the considered agent"""
    path = os.getcwd()
    if os.path.isfile(relativePathPickle):
        genSeries = pd.read_pickle(relativePathPickle)
    else:
        datapath = os.path.join(path, relativePathCSV)
        genSeries = pd.read_csv(datapath, sep=';', comment='#', header=0, skiprows=0, error_bad_lines=False,
                                encoding='unicode_escape', nrows=0)
        genSeries.drop('NNF', axis=1, inplace=True)
        columnNames = list(genSeries)
        genSeries = pd.read_csv(datapath, sep=';', comment='#', header=0, skiprows=2, error_bad_lines=False,
                                encoding='unicode_escape', dtype=float)
        genSeries.drop('NNF', axis=1, inplace=True)
        genSeries.columns = columnNames
        """converting to negative value for generation"""
        genSeries = -genSeries
        genSeries.to_pickle(relativePathPickle)
    return genSeries


def getAgentsFromNodes(names: List[str], requiredNodes: List[str]) -> List[str]:
    """
    Get list of all agents which can be obtained from the provided nodes
    :param names: list of names of all the agents in a particular type
    :param requiredNodes: the nodes(standorts) from which the agents are to be selected
    :return agentNames: agents from the provided nodes
    """

    agentNames = []
    random.shuffle(names)
    for name in names:
        for node in requiredNodes:
            match = re.search(rf'SO-·{node}.*', name)
            if match:
                agentNames.append(re.search('k.*', match.group(0)).group(0))
    return agentNames


########################################################################################################################
# -------------------------Initializing, getting and setting of the RL Agents -----------------------------------------#
########################################################################################################################

def RLNetworkInit(
        agentsDict: Dict[str, Any],
        alg: str,
) -> [[Dict[Dict[Any, str], str]], Dict[Dict[str, Any], Any], int]:
    """
    Parameter sharing of RL Network for same types of agents in same node
    :param agentsDict: Dictionary containing the location name and the object of the flexbility
    :param alg: Name of the algorithm
    :returns nameDict, networkDict, countUniqueAgents:
    """
    networkDict = {}
    nameDict = {}
    countUniqueAgents = 0
    for name, agent in agentsDict.items():
        agent.node = 'Standort_' + re.search("k(\d+)[n,d,l]", agent.id).group(1)
        if networkDict.get(agent.node) is None:
            networkDict[agent.node] = {}
            nameDict[agent.node] = {}
        if nameDict[agent.node].get(agent.type) is None:
            nameDict[agent.node][agent.type] = [name]
        else:
            nameDict[agent.node][agent.type].append(name)
        if networkDict[agent.node].get(agent.type) is None:
            if alg == 'MADDPG':
                networkDict[agent.node][agent.type] = MADDPGAgent()
                countUniqueAgents += 1
            elif alg == 'DDPG':
                networkDict[agent.node][agent.type] = DDPG_Agent()
                countUniqueAgents += 1
            elif alg == 'MAPPO':
                pass
            elif alg == 'DQN':
                networkDict[agent.node][agent.type] = DQN_Agent()
                countUniqueAgents += 1
    return nameDict, networkDict, countUniqueAgents


def getAgentDetails(data: pd.DataFrame, name: str) -> [str, float, float, float]:
    """
    :param data: Pandas dataframe containing the power system's data
    :param name: Name to search for
    :returns loc, voltage_level, min_power, max_power: Location, the minimum and maximum voltage level and the maximum
    power
    """

    details = data.loc[data['Name'] == name]
    if len(details) == 0:
        loc = 'Standort_' + re.search("k(\d+)[n,d,l]", name).group(1)
        voltage_level = data.loc[data['Location'] == loc, 'Un_kV'].values[0]
    else:
        loc = details['Location'].values[0]
        voltage_level = details['Un_kV'].values[0]
    """no information on maximum and minimum power in the latest csv"""
    min_power = 0
    max_power = 2 if voltage_level == 10 else 0.1
    return loc, voltage_level, min_power, max_power


def RLAgentInit(
        env: gym.Env,
        networkDict: Dict[Dict[str, Any], Any],
        nameDict: [Dict[Dict[Any, str], str]],
        nameList: List[str],
        alg: str,
        parameterDict: Dict[str, Any],
        num_iterations: int,
        gradient_steps: int,
) -> None:
    """
    Set hyperparameters, used in combination with Optuna

    :param env: Used gym environment
    :param networkDict: Dictionary containing the agents, used for parameter sharing
    :param nameDict: Dictionary containing the names of the agents mapped to the specific node
    :param nameList: List containing the names of the agents
    :param alg: Name of the used algorithm
    :param parameterDict: Dictionary containing
    :param num_iterations: Number of iterations
    :param gradient_steps: Number of gradient_steps
    """
    for node in networkDict:
        for type, network in networkDict[node].items():
            """name of the first agent in that particular type in this node"""
            name = nameDict[node][type][0]
            if isinstance(network, list):
                for net in network:
                    net.hyperParameterInit(parameterDict)
                    if alg == 'MADDPG':
                        net.initialize(env, nameList.index(name), num_iterations, gradient_steps)
                    elif alg == 'DDPG':
                        net.initialize(env, nameList.index(name), gradient_steps)
                    elif alg == 'DQN':
                        net.initialize(env, nameList.index(name))
            else:
                network.hyperParameterInit(parameterDict)
                if alg == 'MADDPG':
                    network.initialize(env, nameList.index(name), num_iterations, gradient_steps)
                elif alg == 'DDPG':
                    network.initialize(env, nameList.index(name), gradient_steps)
                elif alg == 'DQN':
                    network.initialize(env, nameList.index(name))


def getAgentObjects(
        networkDict: Dict[Dict[str, Any], Any],
        nameDict: [Dict[Dict[Any, str], str]],
        nameList: List[str],
) -> List[Any]:
    """
    Function for returning all agent objects to iterate over them

    :param networkDict: Dictionary containing the agents, used for parameter sharing
    :param nameDict: Dictionary containing the names of the agents mapped to the specific node
    :param nameList: List containing the names of the agents
    :return: List of Multi Agent Objects
    """
    RL_Agents = []
    for agentName in nameList:
        agentNode = 'Standort_' + re.search("k(\d+)[n,d,l]", agentName).group(1)
        for type, names in nameDict[agentNode].items():
            if agentName in names:
                RL_Agents.append(networkDict[agentNode][type].agent)
    return RL_Agents


########################################################################################################################
# ----------------------- Environment interaction and training of the RL Agents ---------------------------------------#
########################################################################################################################
def one_step(
        environment: gym.Env,
        RLAgents: List[Any],
        obs_n: List[List[float]],
        alg: str,
        eval_step: bool = False,
        result_path: str = None,
) -> [List[List[float]], List[List[float]], List[List[float]], List[float], List[bool], List[Dict[str, str]]]:
    """
    Helper function of the collect step and the evaluation function

    :param environment: Gym environment to step through
    :param RLAgents: List of maddpg agents
    :param obs_n: List of observation for each agent
    :param alg: Name of algorithm
    :param eval_step: Bool, whether this is called from the collect function or evaluation function
    :param result_path: Save location
    :returns action_n, buffer_action_n, new_obs_n, rew_n, done_n, info_n: used actions, scaled actions for the replay
    buffer, list of new observations for each agent, list of rewards for each agent, list of dones for each agent,
    list of informations for each agent
    """

    # Get actions from agents
    action_n, buffer_action_n = [], []

    if alg == 'MADDPG' or alg == 'DDPG' or alg == 'DQN':
        if not eval_step:
            for agent in RLAgents:
                action, buffer_action = \
                    agent._sample_action(learning_starts=agent.learning_starts, action_noise=agent.action_noise)
                action_n.append(action)
                buffer_action_n.append(buffer_action)
                agent.num_timesteps += 1
                if alg == 'DQN':
                    agent._update_current_progress_remaining(agent.num_timesteps, agent._total_timesteps)
                    # For DQN, check if the target network should be updated
                    # and update the exploration schedule
                    # For DDPG/TD3, the update is done as the same time as the gradient update
                    agent._on_step()

        else:
            for i, agent in enumerate(RLAgents):
                # Returns from actor predicted action without Noise since the policy is evaluated
                if alg == 'MADDPG' or alg == 'DDPG':
                    action, _ = agent.predict(obs_n[i], param_noise=None)
                    action_n.append(action)
                elif alg == 'DQN':
                    action, _ = agent.predict(obs_n[i], deterministic=True)
                    action_n.append(action)

    if alg == 'MAPPO':
        pass

    new_obs_n, rew_n, done_n, info_n = environment.step(action_n)
    return action_n, buffer_action_n, new_obs_n, rew_n, done_n, info_n


def collect_step(
        environment: gym.Env,
        RLAgents: List[Any],
        obs_n: List[List[float]],
        alg: str
) -> [List[List[float]], List[float], List[bool]]:
    """
    Collect step function for off-policy algorithm

    :param environment: Gym environment
    :param RLAgents: List of maddpg agents
    :param obs_n: List of observations for each agent
    :param alg: Name of the used algorithm
    :returns obs_n, rew_n, done_n: List of new observations for each agent which serves in the next step as input,
    list of reward per agent, list of dones per agent
    """
    action_n, buffer_action_n, new_obs_n, rew_n, done_n, info_n = one_step(environment, RLAgents, obs_n, alg)

    # Store experience in Replay Buffer
    for i, agent in enumerate(RLAgents):
        agent._last_obs = new_obs_n[i]
        agent.replay_buffer.add(obs_n[i], new_obs_n[i], buffer_action_n[i], rew_n[i], done_n[i])

    # Set new actions to old actions and return them, so they can be used as input in the next iteration
    obs_n = new_obs_n
    return obs_n, rew_n, done_n


def trainRLAgents(
        batch_size: int,
        RLAgents: List[Any],
        nameDict: [Dict[Dict[Any, str], str]],
        networkDict: [Dict[Dict[Any, str], Any]],
        nameList: List[str],
        alg: str,
) -> [List[float], List[float], List[float]]:
    """
    Calls the train function with the object in networkDict which is used for parameter sharing

    :param batch_size: Number of batches to consider while training
    :param RLAgents: list of MADDPG objects
    :param nameDict: Dictionary containing the names of the agents mapped to the specific node
    :param networkDict: Dictionary containing the agents, used for parameter sharing
    :param nameList: List containing the names of the agents
    :param alg: Name of the used algorithm
    :returns lActorloss, lCriticloss, lActions: List of actor and critic loss and additionally a list of the average
    actor action
    """
    lActions, lActorloss, lCriticloss = [], [], []
    for i, agentName in enumerate(nameList):
        agentNode = 'Standort_' + re.search("k(\d+)[n,d,l]", agentName).group(1)
        for type, names in nameDict[agentNode].items():
            if agentName in names:
                if alg == 'MADDPG':
                    # Adapt param noise, if necessary.
                    if networkDict[agentNode][type].agent.param_noise is not None and \
                            networkDict[agentNode][type].agent._n_updates % \
                            networkDict[agentNode][type].agent.param_noise_adaption_interval == 0:
                        distance = networkDict[agentNode][type].agent._adapt_param_noise(batch_size)
                    actor_loss, critic_loss, action = \
                        networkDict[agentNode][type].agent.trainMADDPG(RLAgents, batch_size)
                    lActions.append(action)
                    lActorloss.append(actor_loss)
                    lCriticloss.append(critic_loss)
                    break
                elif alg == 'DDPG':
                    # Adapt param noise, if necessary.
                    if networkDict[agentNode][type].agent.param_noise is not None and \
                            networkDict[agentNode][type].agent._n_updates % \
                            networkDict[agentNode][type].agent.param_noise_adaption_interval == 0:
                        distance = networkDict[agentNode][type].agent._adapt_param_noise(batch_size)
                    actor_loss, critic_loss = networkDict[agentNode][type].agent.train(batch_size=batch_size)
                    lActorloss.append(actor_loss)
                    lCriticloss.append(critic_loss)
                    break
                elif alg == 'DQN':
                    critic_loss = networkDict[agentNode][type].agent.train(batch_size)
                    lCriticloss.append(critic_loss)
                    break
    return lActorloss, lCriticloss, lActions


########################################################################################################################
# -------------------------------------Evaluation and saving of results------------------------------------------------#
########################################################################################################################

def compute_avg_return(
        environment: gym.Env,
        RLAgents: List[Any],
        alg: str,
        num_steps: int = 10,
        result_path: str = None,
) -> [np.ndarray, np.ndarray, np.ndarray]:
    """
    computes the average return by performing the required number of steps in the environment

    :param environment: gym environment
    :param RLAgents: list of MADDPG objects
    :param alg: algorithm to consider
    :param num_steps: number of required steps
    :param result_path: path to save the results
    :returns market_return, step_reward, avg_action: Numpy Arrays containing the true spot market return of each agent
    # this deviates from the step reward for some agents because the step rewards is engineered
    """

    reward = 0
    total_action = 0
    # in here obs_n is not set as an attribute of the agent object because we use the prediction function
    # and not sample action which does not use agent._last_obs to compute the actions
    obs_n = environment.reset()

    for step in range(num_steps):
        action_n, _, obs_n, rew, _, _ = one_step(environment, RLAgents, obs_n, alg, eval_step=True,
                                                 result_path=result_path)
        if step == 0:
            reward = np.array(rew)
            total_action = np.array(action_n)
        else:
            reward += np.array(rew)
            total_action += np.array(action_n)

    avg_action = total_action / num_steps

    return reward, avg_action


def saveToFile(
        totalReturns: List[float],
        loss: List[float],
        lActor_loss: List[float],
        lCritic_loss: List[float],
        congestionDetails: pd.DataFrame,
        alg: str,
        result_path: str,
) -> None:
    """
    Function to save simulation details for further analysis

    :param totalReturns: List of returns reached while evaluation the environment
    :param loss: List of total loss (=actor + critic loss)
    :param lActor_loss: List of actor losses
    :param lCritic_loss: List of critic losses
    :param congestionDetails: Pandas DataFrame indicating the congestion details
    :param alg: Name of the algorithm
    :param result_path: Save location
    """
    if not os.path.exists(result_path + alg):
        os.makedirs(result_path + alg)
    filename = result_path + alg + "/totalReturns.pkl"
    with open(filename, "ab") as f:
        pickle.dump(totalReturns, f)
    filename = result_path + alg + "/loss.pkl"
    with open(filename, "ab") as f:
        pickle.dump(loss, f)
    filename = result_path + alg + "/Actorloss.pkl"
    with open(filename, "ab") as f:
        pickle.dump(lActor_loss, f)
    filename = result_path + alg + "/Criticloss.pkl"
    with open(filename, "ab") as f:
        pickle.dump(lCritic_loss, f)
    filename = result_path + alg + "/congested.pkl"
    with open(filename, "ab") as f:
        pickle.dump(congestionDetails.loc[:, 'congested'].values, f)
    filename = result_path + alg + "/top 5 percent loading.pkl"
    with open(filename, "ab") as f:
        pickle.dump(congestionDetails.loc[:, 'top 5 percent loading'].values, f)
    filename = result_path + alg + "/top 2.5 percent loading.pkl"
    with open(filename, "ab") as f:
        pickle.dump(congestionDetails.loc[:, 'top 2.5 percent loading'].values, f)
    filename = result_path + alg + "/volume.pkl"
    with open(filename, "ab") as f:
        pickle.dump(congestionDetails.loc[:, 'volume'].values, f)


########################################################################################################################
# ---------------------------------- Hyperparameter Optimization ------------------------------------------------------#
########################################################################################################################

def hyperParameterOpt(trial: Any, alg: str) -> Dict[str, Any]:
    """
    Hyperparameter optimization with optuna

    :param trial: a single optuna trial object to select the hyperparamters
    :param alg: considered algorithm
    :return parameterDict: Dictionary containing the parameters of the specific trial chosen by optuna optimizer
    """
    parameterDict = {}
    parameterDict['batch_size'] = trial.suggest_categorical('batch_size', [32, 128, 256, 1024])
    if alg == 'MADDPG':
        parameterDict['learning_rate'] = trial.suggest_float("adam_learning_rate", 1e-4, 1e-2, log=True)

        parameterDict['fc_layer_params_actor'] = []
        actor_n_layers = trial.suggest_int('actor_n_layers', 1, 3)
        for i in range(actor_n_layers):
            num_hidden = trial.suggest_int("actor_n_units_L{}".format(i + 1), 50, 500)
            parameterDict['fc_layer_params_actor'].append(num_hidden)

        parameterDict['fc_layer_params_critic'] = []
        critic_n_layers = trial.suggest_int('critic_n_layers', 1, 4)
        for i in range(critic_n_layers):
            num_hidden = trial.suggest_int("fc_layer_params_critic{}".format(i + 1), 50, 500)
            parameterDict['fc_layer_params_critic'].append(num_hidden)

        parameterDict['tf_agents_weights'] = trial.suggest_categorical('tf_agents_weights', [True, False])
        parameterDict['tau'] = trial.suggest_categorical('tau', [0.005, 0.05])
        parameterDict['ParamNoise'] = trial.suggest_categorical('ParamNoise', [True, False])
        parameterDict['sigma'] = trial.suggest_categorical('sigma', [0.1, 0.2])
        parameterDict['TD3'] = trial.suggest_categorical('TD3', [True, False])
        parameterDict['gradient_steps'] = trial.suggest_categorical('gradient_steps', [84, 168, 252])

    elif alg == 'DDPG':
        parameterDict['learning_rate'] = trial.suggest_float("adam_learning_rate", 1e-4, 1e-2, log=True)

    elif alg == 'DQN':
        parameterDict['learning_rate'] = trial.suggest_float("adam_learning_rate", 1e-4, 1e-2, log=True)

    return parameterDict


########################################################################################################################
# --------------------------------------Checkpoint setting and getting -------------------------------------------------#
########################################################################################################################
def restoreCheckpoint(
        result_path: str,
        nAgents: int,
        nameList: List[str],
        networkDict: [Dict[Dict[Any, str], Any]],
        nameDict: [Dict[Dict[Any, str], str]],
        alg: str
) -> [List[Any], [Dict[Dict[Any, str], str]]]:
    """
    :param result_path: String of the path you want to save into
    :param nAgents: Number of MADDPG Agents
    :param nameList: List containing the names of the agents
    :param networkDict: Dictionary containing the agents, used for parameter sharing
    :param nameDict: Dictionary containing the names of the agents mapped to the specific node
    :param alg: Name of the used algorithm
    :returns RLAgents, networkDict: List of MADDPG Agents and the dict with the agents for parameter sharing
    """
    RLAgents = []
    checkpointpath = result_path + "checkpoint_" + alg
    for i in range(nAgents):
        if alg == 'MADDPG' or alg == "DDPG":
            agent = DDPG.load(checkpointpath + "/" + nameList[i] + "/Agent_" + str(i), **{'agent_index': i})
        elif alg == 'DQN':
            agent = DQN.load(checkpointpath + "/" + nameList[i] + "/Agent_" + str(i))
        agent.load_replay_buffer(checkpointpath + "/" + nameList[i] + "/Buffer_Agent_" + str(i))
        assert agent.replay_buffer.size() > 0, "Loaded Buffer does not contain any transitions!"
        RLAgents.append(agent)

    # Update the agents in the network Dict. This is necessary for parameter sharing in the case of muliple (different)
    # agents on one node. At the beginning of the programm the RL Agents list was created using the function below
    # so now we step through for loops and just replace it in the same order as it was created

    iCounter = 0
    for node in networkDict:
        for type, network in networkDict[node].items():
            """name of the first agent in that particular type in this node"""
            name = nameDict[node][type][0]
            if isinstance(network, list):
                for net in network:
                    net.agent = RLAgents[iCounter]
                    iCounter += 1
            else:
                network.agent = RLAgents[iCounter]
                iCounter += 1

    return RLAgents, networkDict


def saveCheckpoint(RLAgents: List[Any], nameList: List[str], alg: str, result_path: str) -> None:
    """
    Function to save the momentary model and/or Replay Buffer for further usage. For instance resuming training

    :param RLAgents: List of RLAgents
    :param nameList: List containing the names of the agents
    :param alg: Name of the used algorithm
    :param result_path: String of the path you want to save into
    """
    if not os.path.exists(result_path + "checkpoint_" + alg):
        os.makedirs(result_path + "checkpoint_" + alg)
    if not os.path.exists(result_path + "checkpoint_" + alg + "/" + nameList[0]):
        for i, agent in enumerate(RLAgents):
            os.makedirs(result_path + "checkpoint_" + alg + "/" + nameList[i])
            agent.save(result_path + "checkpoint_" + alg + "/" + nameList[i] + "/" + "Agent_" + str(i))
            agent.save_replay_buffer(
                result_path + "checkpoint_" + alg + "/" + nameList[i] + "/" + "Buffer_Agent_" + str(i))
    else:
        for i, agent in enumerate(RLAgents):
            agent.save(result_path + "checkpoint_" + alg + "/" + nameList[i] + "/" + "Agent_" + str(i))
            agent.save_replay_buffer(
                result_path + "checkpoint_" + alg + "/" + nameList[i] + "/" + "Buffer_Agent_" + str(i))


########################################################################################################################
# -------------------------------------------------Helper---------------------------------------------------------------#
########################################################################################################################

def checkTime(lastTime: float, process: str) -> float:
    """
    Helper function to print the time a process tales

    :param lastTime: Last time which was measured before the process
    :param process: Name of the process
    :return: Momentary time
    """
    presentTime = time.time()
    print(process, presentTime - lastTime)
    return presentTime

    # def getMaxEffectNodes():
    """helper function code to find the nodes with maximum sensitivity
        DEbug mode, Breakpoint in line 110 in Grid.py"""
    # lines = ['Trafo_19', 'Trafo_14', 'Trafo_60', 'Knoten_79_Stich', 'L_0R_36', 'Trafo_62', 'Trafo_39', 'L_29R_39_boost',
    #          'L_33R_39', 'L_0R_39']
    # times = [24 * d + h for d in days for h in range(24)]
    # agents = []
    # for t in times:
    #     agents.append(self.getSensitivity(t).loc[self.getSensitivity(t).index.isin(lines), self.sensitivity.columns[1:]].abs().idxmax(axis="columns").values)
    # agents = [a for array in agents for a in array]
    # agents = list(set(agents))
    # nodes = ['Standort_' + re.search("k(\d+)[n,d,l]", a).group(1) for a in agents]
    # pass
