# Multi-Agent Reinforcement Learning Model for Energy Prosumers in a Bi-Level Electricity Market Framework

This is a OpenAI based DDPG reinforcement learning model to simulate the behavior of wind power plants, pv power plants, flexible household consumers, electric vehicles, battery storages, heat pumps, and industrial consumers in a bi-level market setup.
They can sell or buy energy at the spot market and provide flexibility in a flexibility market. Unlike the spot market, the flexibility market demand is limited. Flexible consumers have to compete for earnings.
However, the demand can be influenced by the spot market bevhior. By cooperating flexible consumers can increase the demand. This model helps identifiying strategical bidding to manipulate the markets - also called gaming.

## Setup

To create a new environment to run this project: 

    conda create --name test_env --file conda_requirements.txt

activate the new environment and do

    pip install -r pip_requirements.txt

(If there is error for tensorflow, install tensorflow-estimator version 2.1.0)

Install the Local flexibility market gym environment

goto folder gym-LocalFlexMarketEnv and 

    pip install -e .

copy the inputs folder
