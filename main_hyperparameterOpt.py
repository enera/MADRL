from SpotMarket import SpotMarket
from DSO import DSO
from Grid import Grid
import gym
import numpy as np
import pandas as pd
import os
import time
import re
import util
import pickle
import tqdm
import random
pd.options.mode.chained_assignment = None
import torch as th
import yaml
import optuna
import joblib

if __name__ == '__main__':
    st = time.time()
    random.seed(0)
    np.random.seed(0)
    th.manual_seed(0)

    # Read out config.yaml
    with open("config.yaml", 'r') as stream:
        config = yaml.safe_load(stream)

    """specify the algorithm - MADDPG, MAPPO
       specify the start and end days of the usecases"""
    alg = config["alg"]
    startDay = config["Times"]["startDay"]
    endDay = config["Times"]["endDay"]

    input_path = config["input_path"]
    result_path = config["result_path"]

    networkDict = {}

    agentsDict, nameDict, networkDict, numAgents, loadingSeriesHP, chargingSeriesEV, \
    genSeriesPV, genSeriesWind, loadingSeriesDSM, nMADDPGAgents = \
        util.agentsInit(alg, startDay, endDay, input_folder = input_path, numAgentsEachType = 1)

    networkDictonary = networkDict
    nameList = [agent.id for agent in agentsDict.values()]
    typeList = [agent.type for agent in agentsDict.values()]
    """Initialize the grid"""
    grid = Grid(numAgents, nameList, loadingSeriesHP, chargingSeriesEV, genSeriesPV, genSeriesWind, loadingSeriesDSM,
                         input_folder = input_path, numCPU=20)
    """Initialize the spot market"""
    sm = SpotMarket()
    agentsList = [obj for name, obj in agentsDict.items()]
    sm.addParticipants(agentsList)
    """Initialize the DSO"""
    dso = DSO(grid, startDay, endDay)
    dso.addflexAgents(agentsList)

    """load the train Gym environment"""
    env = gym.make("gym_LocalFlexMarketEnv:LocalFlexMarketEnv-v0", **{'SpotMarket': sm, 'DSO': dso, 'alg': alg})
    env.startDay = startDay
    env.endDay = endDay
    env.reset()

    """saving the names of the agents"""
    if not os.path.exists(result_path + alg):
        os.makedirs(result_path + alg)
    filename = result_path + alg + "/agentList.pkl"
    with open(filename, "wb") as f:
        pickle.dump(nameList, f)

    """Training parameters"""
    num_iterations = config["TrainingParameters"]["num_iterations"]
    collect_steps_per_iteration = config["TrainingParameters"]["collect_steps_per_iteration"]
    eval_interval = config["TrainingParameters"]["eval_interval"]
    eval_steps = config["TrainingParameters"]["eval_steps"]
    checkpoint_interval = config["TrainingParameters"]["checkpoint_interval"]
    batch_size = config["TrainingParameters"]["batch_size"]
    assert eval_steps <= 24*(endDay - startDay), "Resetting the environment while evaluation is not supported yet"


    def objective(trial):
        parameterDict = util.hyperParameterOpt(trial, alg)
        gradient_steps = parameterDict['gradient_steps']
        batch_size = parameterDict['batch_size']

        """initialize the RL agents"""
        util.RLAgentInit(env, networkDictonary, nameDict, nameList, parameterDict, nMADDPGAgents)
        RLAgents = util.getAgentObjects(networkDictonary, nameDict, nameList, alg)

        """Lists to append and save data for analysis"""
        totalReturns, loss, lActor_loss, lCritic_loss, lActions = [], [], [], [], []
        episode_rewards = [0.0]  # sum of rewards for all agents
        agent_rewards = [[0.0] for _ in range(len(env.agents))]
        episode_number = 0
        t_start = time.time()

        obs_n = env.reset()
        for i, agent in enumerate(RLAgents):
            agent._last_obs = obs_n[i]

        for num_iter in tqdm.trange(1, num_iterations + 1):
            episode_number += 1
            """Collect a few steps using collect_policy and save to the replay buffer."""
            for _ in range(collect_steps_per_iteration):
                obs_n, rew_n, done_n = util.collect_step(env, RLAgents, obs_n, alg)

                for i, rew in enumerate(rew_n):
                    episode_rewards[-1] += rew
                    agent_rewards[i][-1] += rew

                if any(done_n):
                    # Reset environment
                    # TODO: LG What happens if we comment that out?
                    obs_n = env.reset()
                    for i, agent in enumerate(RLAgents):
                        agent._last_obs = obs_n[i]

                    # Reset action and parameter noise
                    [agent.reset() for agent in RLAgents]

                    # Append a new element to all lists in order to shift to the next index
                    episode_rewards.append(0)
                    for i in range(len(agent_rewards)):
                        agent_rewards[i].append(0)

            """so that the reset step with 0 rewards is not added to the replay buffer"""
            if eval_interval >= (endDay - startDay):
                if num_iter % (endDay - startDay) == 0:
                    obs_n = env.reset()
                    for i, agent in enumerate(RLAgents):
                        agent._last_obs = obs_n[i]

            if alg == 'MADDPG':
                for _ in range(gradient_steps):
                    actor_loss, critic_loss, actions = \
                        util.trainMADDPGAgents(batch_size, RLAgents, nameDict, networkDict, nameList)
                    lActions.append(actions)
                    lActor_loss.append(actor_loss)
                    lCritic_loss.append(critic_loss)

            elif alg == 'MAPPO':
                pass

            printFreq = 10
            if episode_number % printFreq == 0:
                print("steps: {}, episodes: {}, mean episode reward: {}, time: {}".format(
                    num_iter, len(episode_rewards) - 1, np.mean(episode_rewards[-printFreq - 1:-1]),
                    round(time.time() - t_start, 3)))
                t_start = time.time()

            # Make sure to only check every n steps
            if num_iter % eval_interval == 0 and num_iter != config["TrainingParameters"]["num_iterations"]:
                """unpromising trials at the early stages of the training"""
                obs_n = env.reset()
                for i, agent in enumerate(RLAgents):
                    agent._last_obs = obs_n[i]
                intermediate_return, avg_action = util.compute_avg_return(env, RLAgents, alg, num_steps=eval_steps,
                                                                   result_path=result_path)
                obs_n = env.reset()
                for i, agent in enumerate(RLAgents):
                    agent._last_obs = obs_n[i]
                trial.report(intermediate_return.sum(), num_iter)
                if trial.should_prune():
                    raise optuna.TrialPruned()

            if num_iter == config["TrainingParameters"]["num_iterations"]:
                obs_n = env.reset()
                for i, agent in enumerate(RLAgents):
                    agent._last_obs = obs_n[i]
                total_return, avg_action = util.compute_avg_return(env, RLAgents, alg, num_steps=eval_steps,
                                                                   result_path=result_path)
                obs_n = env.reset()
                for i, agent in enumerate(RLAgents):
                    agent._last_obs = obs_n[i]
                print('step: {0}, Avg Return: {1}'.format(num_iter, total_return))
                print("=====================================================================================")
                joblib.dump(study, '../results/new_study.pkl')
                return total_return.sum()

    study = optuna.create_study(direction="maximize", pruner=optuna.pruners.MedianPruner(n_startup_trials=6, n_warmup_steps=10))
    study.optimize(objective, n_trials=60, n_jobs=1)


    duration = (time.time()-st)/60
    print("---------------------------------------------%.2f minutes-----------------------------------" % duration)
    util.saveToFile([], [], [], [], dso.congestionDetails, alg, result_path)