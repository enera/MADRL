import numpy as np
import pandas as pd
from FlexAgent import FlexAgent


class HeatPump(FlexAgent):
    """
        Battery Storage Class

        :param id: ID of agent
        :param location: Location of the HeatPump agent
        :param marginalCost: Marginal cost
        :param voltageLevel: Voltage level of Heat pump agent
        :param maxHeatLoad: Maximum heat load
        :param maxStorageLevel: Maximum StorageLevel, which is an operational constraint
        :param minStorageLevel: Minimum StorageLevel, which is an operational constraint
        :param scheduledLoad: Needed heating load
        :param startDay: Starting Day of the simulation
        :param endDay: Ending Day of the simulation
        """

    def __init__(
            self,
            id,
            location=None,
            maxPower: float = 0.,
            marginalCost: float = 0.,
            voltageLevel: float = 0.,
            maxStorageLevel: float = 0.,
            maxHeatLoad: float = 0.,
            minStorageLevel: float = None,
            scheduledLoad: pd.DataFrame = None,
            startDay: int = 0,
            endDay: int = 365
    ):

        super().__init__(
            id=id,
            location=location,
            maxPower=maxPower,
            marginalCost=marginalCost,
            startDay=startDay,
            endDay=endDay
        )
        self.maxPower = self.maxPower

        self.type = "Heat Pump"
        self.maxStorageLevel = maxStorageLevel  # Storage capacity in MWh
        """
        How much energy is stored in the reservoir after the spot market bids
        and after the spot market dispatch
        used to update the energyTable
        """
        self.storageLevel = None  # how much energy is stored in the reservoir
        self.maxHeatLoad = maxHeatLoad
        if minStorageLevel is None:
            self.minStorageLevel = 0.2 * self.maxStorageLevel

        self.ambTemp = None
        self.voltageLevel = voltageLevel
        self.scheduledLoad = pd.DataFrame(data={'time': np.arange(self.spotTimePeriod + 1),
                                                'load': scheduledLoad,
                                                'loaded': np.full(self.spotTimePeriod + 1, False)})
        self.energyTable = None
        self.spotChangedEnergy = 0
        self.flexChangedEnergy = 0
        """As the reward values are small because of low power"""
        if self.voltageLevel == 10:
            self.penaltyViolation = -100

        """in flex , can "sell" qty ie, to reduce the qty from the spot dispatched amount 
                    can buy qty to increase the qty from the spot dispatched amount"""
        self.lowFlexBidLimit = -1
        self.highFlexBidLimit = 1

        self.lowSpotBidLimit = 0
        self.highSpotBidLimit = 1

        self.obs_space_size = 3

        self.action_dict = \
            {
                0: [0, 0, 1],
                1: [0, 0, 2],
                2: [0, -1, 1],
                3: [0, -1, 2],
                4: [0, 1, 1],
                5: [0, 1, 2],
                6: [1, 0, 1],
                7: [1, 0, 2],
                8: [1, -1, 1],
                9: [1, -1, 2],
                10: [1, 1, 1],
                11: [1, 1, 2],
            }
        self.mean_MCP = 0

        self.penalizeTimes = []
        self.reset(startDay)

    def reset(self, startDay: int = 0) -> None:
        """
        Reset function for the Heat Pump agent

        :param startDay: Starting day
        """
        super().reset(startDay)
        self.storageLevel = self.maxStorageLevel / 2
        self.spotChangedEnergy = 0
        self.flexChangedEnergy = 0

        self.energyTable = pd.DataFrame(data={'time': np.arange(self.spotHour + 1),
                                              'after_spot': np.full(self.spotHour + 1, self.storageLevel,
                                                                    dtype=float),
                                              'before_flex': np.full(self.spotHour + 1, self.storageLevel,
                                                                     dtype=float),
                                              'after_flex': np.full(self.spotHour + 1, self.storageLevel,
                                                                    dtype=float)})

    def printInfo(self) -> None:
        super().printInfo()
        print("Type: {}".format(self.type))

    def updateEnergyTable(self, index: int, status: str, value: float) -> None:
        """
        Table that updates the energy stored in the battery

        :param index: the hour in the day
        :param status: whether after spot bids, or when making flex bids, or after flex bids
        :param value: energy
        """
        if not index == self.spotTimePeriod:
            self.energyTable.loc[self.energyTable['time'] == index, status] = value

    def canStore(self, power: float, time: int, status: str) -> [bool, float]:
        """
        Check whether storing energy is possible, time in hrs and power in MW (+ve)
        If False, returns by how much amount

        :param power: charging power
        :param time: charging time
        :param status:  whether after spot bids, or when making flex bids, or after flex bids
        :return: Whether the storing energy is possible or not, and if not by how much
        """
        if power > self.maxPower:
            power = self.maxPower

        energy = power * time
        if status == 'before_spot':
            if self.storageLevel + energy > self.maxStorageLevel:
                return False, self.storageLevel + energy - self.maxStorageLevel
            else:
                return True, None
        elif status == 'before_flex':
            """check whether additional this energy can be stored
                calculate how much energy was stored in this time in spot and update storageLevelBeforeFlex"""
            if self.storageLevel + self.spotChangedEnergy + energy > self.maxStorageLevel:
                return False, self.storageLevel + self.spotChangedEnergy + energy - self.maxStorageLevel
            else:
                return True, None
        elif status == 'after_flex':
            if self.storageLevel + self.flexChangedEnergy > self.maxStorageLevel:
                return False, self.storageLevel + self.flexChangedEnergy - self.maxStorageLevel
            else:
                return True, None

    def store(self, power: float, time: int, status: str, index: int) -> None:
        """
        Updates the storage level and energy table

        :param power: charging power
        :param time: charging time
        :param status: whether after spot bids, or when making flex bids, or after flex bids
        :param index: the hour in the day
        """
        energy = power * time
        if status == 'before_spot':
            self.storageLevel += energy
            self.updateEnergyTable(index, 'after_spot', self.storageLevel)
        elif status == 'before_flex':
            self.storageLevel = self.storageLevel + self.spotChangedEnergy + energy
            self.updateEnergyTable(index, status, self.storageLevel)
        elif status == 'after_flex':
            self.storageLevel = self.storageLevel + self.flexChangedEnergy
            self.updateEnergyTable(index, status, self.storageLevel)

    def checkLoading(self, status, index) -> bool:
        """
        checks if the scheduled loading of heat pump is possible or not

        :param status:  whether after spot bids, or when making flex bids, or after flex bids
        :param index: the hour in the day
        :return: Bool whether loading is possible of not
        """
        if status == 'before_spot':
            load = self.scheduledLoad.loc[index, 'load']
        elif status == 'after_flex':
            load = self.flexChangedEnergy / self.spotTimeInterval  # Division is not necessary in hourly environment

        if load == 0:
            return True
        possible, _ = self.canLoad(load, self.spotTimeInterval, status)
        if possible:
            """only load before spot since others already take into account the loading via 
            spotChangedEnergy and flexChangedEnergy"""
            if status == 'before_spot':
                """
                energyTable holds energy at the start of the specified time
                so, a load in current time affects the energy at the next time
                """
                self.load(load=load, time=self.spotTimeInterval, status='after_spot', index=index + 1)
            return True
        else:
            return False

    def canLoad(self, load: float, time: int, status: str) -> [bool, float]:
        """
        Checks if the loading for this much time is possible or not

        :param load: Quantity to be loaded
        :param time: Time
        :param status: see above
        :return: Bool whether it is possible or not
        """
        if load > self.maxPower:
            load = self.maxPower

        energy = load * time

        if status == 'before_spot':
            if self.storageLevel - energy < self.minStorageLevel:
                return False, self.minStorageLevel - (self.storageLevel - energy)
            else:
                return True, None
        elif status == 'before_flex':
            """check whether additional this energy can be loaded
                calculate how much energy was stored in this time in spot and update storageLevelBeforeFlex"""
            if self.storageLevel + self.spotChangedEnergy - energy < self.minStorageLevel:
                return False, self.minStorageLevel - (self.storageLevel + self.spotChangedEnergy - energy)
            else:
                return True, None
        elif status == 'after_flex':
            if self.storageLevel + self.flexChangedEnergy < self.minStorageLevel:
                return False, self.minStorageLevel - (self.storageLevel + self.flexChangedEnergy)
            else:
                return True, None
        else:
            print("Error, HeatPump can not act as a generation unit in Spot Market")

    def load(self, load: float, time: int, status: str, index: int) -> None:
        """
        loads the heatpump and decreases the stored energy

        :param load: Quantity to be loaded
        :param time: Time of the day
        :param status: Status description see above
        :param index: Index of time
        """

        energy = load * time
        if status == 'before_spot' or status == 'after_spot':
            self.storageLevel -= energy
            self.updateEnergyTable(index, 'after_spot', self.storageLevel)
        elif status == 'before_flex':
            self.storageLevel = self.storageLevel + self.spotChangedEnergy - energy
            self.updateEnergyTable(index, status, self.storageLevel)
        elif status == 'after_flex':
            self.storageLevel = self.storageLevel + self.flexChangedEnergy
            self.updateEnergyTable(index, status, self.storageLevel)

    def makeSpotBid(self) -> None:
        """
        Function to make the spot bid
        """
        status = 'before_spot'
        self.spotChangedEnergy = 0.
        self.flexChangedEnergy = 0.
        self.boundSpotBidMultiplier(low=self.lowSpotBidLimit, high=self.highSpotBidLimit)
        self.energyTable['time'] = np.concatenate([self.hours, np.array([self.hours[-1] + 1])])
        super().makeSpotBid()
        self.penalizeTimes = []
        self.fullSpotBid.loc[[self.time], :] = self.makeBid(self.fullSpotBid.loc[[self.time], :], status)

    def spotMarketEnd(self):
        """
        Function to end the spot market and set the reward
        """
        spotDispatchedTimes, spotDispatchedQty = super().spotMarketEnd()
        self.spotMarketReward(spotDispatchedTimes, spotDispatchedQty)
        """change storage level to that of the starting time for this day to use in flex market"""
        self.storageLevel = self.energyTable.loc[self.energyTable['time'] == self.hours[0], 'after_spot'].values[0]

    def spotMarketReward(self, time: int, qty: float) -> None:
        """
        Calculates the spot market reward

        :param time: spotDispatchedTimes
        :param qty: spotDispatchedQty
        """
        # Here negative of qty is used for reward because generation is negative qty
        self.fullRewardTable.loc[time, 'reward_spot'] = self.fullSpotBid.loc[time, 'MCP'] * -qty
        self.fullRewardTable.loc[self.penalizeTimes, 'reward_spot'] += self.penaltyViolation

        self.penalizeTimes = []

    def makeFlexBid(self, reqdFlexTimes: np.ndarray):
        """
        Function to make the flex bid. HP can bid on the flex market without having bid on the spot market

        :param reqdFlexTimes: Numpy array with the flex time
        """
        status = 'before_flex'
        self.boundFlexBidMultiplier(low=self.lowFlexBidLimit, high=self.highFlexBidLimit)
        # Copy spot bids so that the flexbid multiplier is applied on this instead of maxPower but only in case
        # the already bought energy should be reduced. If the spot bid was to buy but the reservoir is not full, then
        # the agent can also just buy on the flex market, and there it can buy with maximum power
        if self.flexBidMultiplier[0] < 0:
            self.fullFlexBid.loc[self.time, 'qty_bid'] = \
                self.fullSpotBid.loc[self.time, 'qty_bid']
        super().makeFlexBid(reqdFlexTimes)
        self.penalizeTimes = []
        self.fullFlexBid.loc[[self.time], :] = self.makeBid(self.fullFlexBid.loc[[self.time], :], status)

    def flexMarketEnd(self) -> None:
        """
        Function that updates the energy tables, checks for penalties and sets the reward
        """
        flexDispatchedTimes, flexDispatchedQty, flexDispatchedPrice = super().flexMarketEnd()
        """change storage level to that of the starting time for this day to use in flex market"""
        self.storageLevel = self.energyTable.loc[self.energyTable['time'] == self.time, 'after_spot'].values[0]

        time = self.time
        qty = self.fullFlexBid.loc[self.time, 'qty_bid']
        dispatched = self.fullFlexBid.loc[self.time, 'dispatched']

        """amount of energy changed in the spot and flex dispatch used to update the energy table for after_flex 
                    times """
        self.flexChangedEnergy = \
            self.energyTable.loc[self.energyTable['time'] == time + 1, 'before_flex'].values[0] -\
            self.energyTable.loc[self.energyTable['time'] == time, 'before_flex'].values[0]

        self.spotChangedEnergy = \
            self.energyTable.loc[self.energyTable['time'] == time + 1, 'after_spot'].values[0] -\
            self.energyTable.loc[self.energyTable['time'] == time, 'after_spot'].values[0]

        """checkLoading updates the energyTable with the load, if cannot be loaded --> high negative rewards"""

        loaded = self.checkLoading(status='after_flex', index=time)
        if not loaded:
            self.penalizeTimes.append(time)
        """
        Mod LG: added self.startDay*24 to self.spotTimePeriod, with this the if constraint is not true in case
                time + 1 is equal the last time + 1 (because python starts counting at zero)
        """
        if not time + 1 == self.startDay * 24 + self.spotTimePeriod:
            if dispatched:
                if qty > 0:
                    possible, _ = self.canStore(power=qty, time=self.spotTimeInterval, status='after_flex')
                    if possible and self.flexChangedEnergy <= self.maxPower:
                        self.store(power=qty, time=self.spotTimeInterval, status='after_flex', index=time + 1)
                    else:
                        """if the list is empty or if time has not been added"""
                        if not self.penalizeTimes or not self.penalizeTimes[-1] == time:
                            self.penalizeTimes.append(time)
                        self.storageLevel = self.storageLevel + self.spotChangedEnergy
                        self.store(power=0, time=self.spotTimeInterval, status='after_flex', index=time + 1)
                else:
                    """consider negative bid as load"""
                    possible, _ = self.canLoad(load=-qty, time=self.spotTimeInterval, status='after_flex')
                    if possible and self.flexChangedEnergy >= -self.maxPower:
                        self.load(load=-qty, time=self.spotTimeInterval, status='after_flex', index=time + 1)
                    else:
                        if not self.penalizeTimes or not self.penalizeTimes[-1] == time:
                            self.penalizeTimes.append(time)
                        self.storageLevel = self.storageLevel + self.spotChangedEnergy
                        self.load(load=0, time=self.spotTimeInterval, status='after_flex', index=time + 1)
            else:
                self.storageLevel = self.storageLevel + self.spotChangedEnergy
                self.updateEnergyTable(time + 1, 'after_flex', self.storageLevel)

        self.flexMarketReward(flexDispatchedTimes, flexDispatchedQty, flexDispatchedPrice)

        """After flex market ends for a day, the final energy must be updated for all columns in energy table as it is 
        the final realised energy after the day"""
        nextTime = (self.time + 1) * self.spotHour
        self.energyTable.loc[self.energyTable['time'] == nextTime, ['after_spot', 'before_flex']] = \
            self.energyTable.loc[self.energyTable['time'] == nextTime, 'after_flex']
        self.storageLevel = self.energyTable.loc[self.energyTable['time'] == nextTime, 'after_flex'].values[0]
        """keep the energy at the end of last day as the starting energy"""
        self.energyTable.loc[0, ['after_spot', 'before_flex', 'after_flex']] = self.storageLevel

    def flexMarketReward(self, time: int, qty: float, price: float) -> None:
        """
        :param time: flexDispatchedTimes
        :param qty: flexDispatchedQty
        :param price: price for dispatched time
        """
        self.fullRewardTable.loc[self.penalizeTimes, 'reward_flex'] = self.penaltyViolation
        # Either way, if dispatched, the DSO pays the agents
        self.fullRewardTable.loc[time, 'reward_flex'] += price * qty.abs()

    def makeBid(self, hourlyBid: pd.Series, status: str) -> None:
        """
        Makes the bid and checks whether constraints are violated

        :param hourlyBid: Pandas Series containing the daily bid
        :param status: see above
        """
        time = hourlyBid['time'].values[0]
        qty = hourlyBid['qty_bid'].values[0]

        if status == 'before_flex':
            # amount of energy changed in the spot dispatch used to update the energy table for before_flex times
            self.spotChangedEnergy = \
                self.energyTable.loc[self.energyTable['time'] == time + 1, 'after_spot'].values[0] -\
                self.energyTable.loc[self.energyTable['time'] == time, 'after_spot'].values[0]

        """checkLoading updates the energyTable with the load if cannot be loaded --> high negative rewards"""
        if status == 'before_spot':
            """if the energy level is too low for the energy which is needed for this hour -> penalty"""
            loaded = self.checkLoading(status=status, index=time)
            if not loaded:
                self.penalizeTimes.append(time)

        if qty >= 0:
            possible, _ = self.canStore(power=qty, time=self.spotTimeInterval, status=status)
            if possible and self.spotChangedEnergy + qty <= self.maxPower:
                self.store(power=qty, time=self.spotTimeInterval, status=status, index=time + 1)
            else:
                """storing not possible: we are not modifying the bid, penalizing"""
                hourlyBid.loc[time, 'qty_bid'] = 0
                self.store(power=0, time=self.spotTimeInterval, status=status, index=time + 1)
                if not self.penalizeTimes:
                    self.penalizeTimes.append(time)
            # TODO check other possible options here
        else:
            """In case of Flexbid: check whether it can reduce this particular amount from spot bid
                if possible, consider it as if it was a load
                reverse the sign as it is already negative"""
            possible, _ = self.canLoad(load=-qty, time=self.spotTimeInterval, status=status)
            if possible and self.spotChangedEnergy + qty >= -self.maxPower:
                self.load(load=-qty, time=self.spotTimeInterval, status=status, index=time + 1)
            else:
                hourlyBid.loc[time, 'qty_bid'] = 0
                self.load(load=0, time=self.spotTimeInterval, status=status, index=time + 1)
                if not self.penalizeTimes:
                    self.penalizeTimes.append(time)

        assert -self.maxPower <= hourlyBid.loc[time, 'qty_bid'] <= self.maxPower, 'Qty bid cannot be more than maxPower'

        return hourlyBid
