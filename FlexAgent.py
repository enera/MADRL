import numpy as np
import pandas as pd


class FlexAgent:
    def __init__(self, id, location=None, minPower=0, maxPower=0, marginalCost=30,
                 spotHour=1, needCounterTrade=False,
                 startDay=0, endDay=365):
        self.id = id
        self.node = ""
        self.type = "unknown"
        self.location = location
        self.minPower = minPower
        self.maxPower = maxPower
        self.marginalCost = marginalCost
        self.startDay = startDay
        self.endDay = endDay
        self.spotHour = spotHour
        # self.dailySpotTime = 1
        self.spotTimePeriod = (self.endDay - self.startDay) * self.spotHour * 24
        self.day = startDay
        self.time = 0
        self.hours = np.arange(self.day * self.spotHour * 24, (self.day * (self.spotHour) * 24) + 1)
        self.totalHours = np.arange(self.startDay * self.spotHour * 24, self.endDay * self.spotHour * 24)
        self.flexHour = self.spotHour
        self.flexTimePeriod = (self.endDay - self.startDay) * self.spotHour * 24
        self.spotTimeInterval = 1  # in hours
        self.needCounterTrade = needCounterTrade  # Is counter trading responsibility of FlexAgent?
        self.fullSpotBid = None
        self.fullFlexBid = None
        self.fullRewardTable = None
        '''
        spotBidMultiplier is used to adapt the qty_bid in spot market
        '''
        self.spotBidMultiplier = None
        '''
        flexQtyMultiplier is used to adapt the qty_bid in flex market
        flexBidPriceMultiplier is used to adapt the price of bid in flex market
        '''
        self.flexBidMultiplier = None
        self.flexBidPriceMultiplier = None

        """during flex bid some agents like 
            DSM may need to bid -ve qty to reduce the qty already purchased in spot market 
            eV may need to bid -ve qty to (eg, to stop/reduce charging)
            and some agents like PV may need to bid +ve qty to reduce the generation dispatched in spot market
                Assumption: the time series data for PV and Wind contains -ve qty (generation)"""
        self.lowSpotBidLimit = 0
        self.highSpotBidLimit = 1
        self.lowFlexBidLimit = -1
        self.highFlexBidLimit = 0
        self.lowPriceLimit = 1
        self.highPriceLimit = 2
        self.penaltyViolation = -10
        self.spotState = None
        """current day MCP"""
        self.MCP = []

    def reset(self, *args, **kwargs):
        self.day = args[0]
        self.time = self.day * 24
        self.hours = np.arange(self.time, self.time + 1)
        self.totalHours = np.arange(self.startDay * self.spotHour * 24, self.endDay * self.spotHour * 24)
        self.fullSpotBid = pd.DataFrame(data={'time': self.totalHours,
                                              'qty_bid': np.full(self.spotTimePeriod, self.maxPower, dtype=float),
                                              'dispatched': np.full(self.spotTimePeriod, False),
                                              'MCP': np.full(self.spotTimePeriod, 0.0, dtype=float)},
                                        index=self.totalHours)
        self.fullFlexBid = pd.DataFrame(data={'time': self.totalHours,
                                              'qty_bid': np.full(self.flexTimePeriod, self.maxPower, dtype=float),
                                              'price': np.full(self.flexTimePeriod, self.marginalCost, dtype=float),
                                              'flex_capacity': np.full(self.flexTimePeriod, self.maxPower, dtype=float),
                                              'dispatched': np.full(self.flexTimePeriod, False)},
                                        index=self.totalHours)
        self.fullRewardTable = pd.DataFrame(data={'time': self.totalHours,
                                                  'reward_spot': np.full(self.spotTimePeriod, 0, dtype=float),
                                                  'reward_flex': np.full(self.spotTimePeriod, 0, dtype=float)},
                                            index=self.totalHours)
        self.MCP = list(np.random.randint(0, 20, size=1))

    def printInfo(self):
        print("Agent ID: {}\nLocation: {}\nMaximum Power: {}"
              .format(self.id, self.location, self.maxPower))

    def getID(self):
        return self.id

    def makeSpotBid(self):
        self.spotBidMultiplier = self.getSpotBidMultiplier()
        self.fullSpotBid.loc[self.time, 'qty_bid'] = \
            int((self.fullSpotBid.loc[self.time, 'qty_bid']*self.spotBidMultiplier)*(10**8))/(10.**8)

    def boundSpotBidMultiplier(self, low, high):
        """
        if -1, agent sells the maxPower
        if 1, agent buys the maxPower
        """
        self.spotBidMultiplier[self.spotBidMultiplier < low] = low
        self.spotBidMultiplier[self.spotBidMultiplier > high] = high

    def getSpotBidMultiplier(self):
        return self.spotBidMultiplier

    def spotMarketEnd(self):
        spotDispatchedTimes = self.fullSpotBid.query('dispatched == True').loc[self.time, 'time']
        spotDispatchedQty = self.fullSpotBid.loc[self.time, 'qty_bid']
        if self.type not in ["PV Generation", "Wind Generation"]:
            return spotDispatchedTimes, spotDispatchedQty

    def makeFlexBid(self, reqdFlexTimes):

        if self.needCounterTrade:
            spotDispatchedTimes = self.fullSpotBid.query('dispatched == True').loc[self.time, 'time']
            spotDispatchedQty = self.fullSpotBid.loc[spotDispatchedTimes, 'qty_bid']
            self.fullFlexBid.loc[self.fullFlexBid['time'].isin(spotDispatchedTimes), ['flex_capacity']] = \
                self.maxPower - spotDispatchedQty

        self.flexBidMultiplier, self.flexBidPriceMultiplier = self.getFlexBidMultiplier()
        self.fullFlexBid.loc[self.time, 'qty_bid'] = \
            int((self.fullFlexBid.loc[self.time, 'qty_bid']*self.flexBidMultiplier)*(10**8))/(10.**8)
        self.fullFlexBid.loc[self.time, 'price'] *= self.flexBidPriceMultiplier

    def boundFlexBidMultiplier(self, low, high, priceLimit=None):
        """
        flexBidMultiplier should be between low and high
        flexBidPriceMultiplier maximum limit
        """

        if priceLimit is None:
            priceLimit = self.highPriceLimit
        self.flexBidMultiplier[self.flexBidMultiplier < low] = low
        self.flexBidMultiplier[self.flexBidMultiplier > high] = high

        if self.type not in ["PV Generation", "Wind Generation"]:
            self.flexBidPriceMultiplier[self.flexBidPriceMultiplier > priceLimit] = priceLimit

    def getFlexBidMultiplier(self):
        return self.flexBidMultiplier, self.flexBidPriceMultiplier

    def flexMarketEnd(self):
        flexDispatchedTimes = self.fullFlexBid.loc[[self.time], :].query('dispatched == True').loc[:, 'time']
        flexDispatchedQty = self.fullFlexBid.loc[flexDispatchedTimes, 'qty_bid']
        flexDispatchedPrice = self.fullFlexBid.loc[flexDispatchedTimes, 'price']
        return flexDispatchedTimes, flexDispatchedQty, flexDispatchedPrice

    def getSpotBid(self):
        return self.fullSpotBid.loc[[self.time], :]

    def getFlexBid(self):
        return self.fullFlexBid.loc[[self.time], :]

    def spotMarketReward(self, *args, **kwargs):
        pass

    def flexMarketReward(self, *args, **kwargs):
        pass

    def updateReward(self, reward):
        pass

    def getTotalReward(self):
        pass

    def setMCP(self, price):
        self.MCP = price

    def getActionLimits(self, alg):
        """returns the min and max limits for the action of the agent"""

        if alg == "MADDPG" or alg == "DDPG":
            dataType = float
            minSpotBidArray = np.full(self.spotHour, self.lowSpotBidLimit, dtype=dataType)
            maxSpotBidArray = np.full(self.spotHour, self.highSpotBidLimit, dtype=dataType)
            minFlexBidArray = np.full(self.spotHour, self.lowFlexBidLimit, dtype=dataType)
            maxFlexBidArray = np.full(self.spotHour, self.highFlexBidLimit, dtype=dataType)
            minPriceArray = np.full(self.spotHour, self.lowPriceLimit, dtype=dataType)
            maxPriceArray = np.full(self.spotHour, self.highPriceLimit, dtype=dataType)

        return np.hstack((minSpotBidArray, minFlexBidArray, minPriceArray)), \
               np.hstack((maxSpotBidArray, maxFlexBidArray, maxPriceArray))

    def getObservation(self, multiAgent):
        """
        returns the current observation of the environment:
        """

        MCP = (self.MCP - self.minMCP) / (self.maxMCP - self.minMCP)

        if self.type == 'Demand Side Management':
            if self.time == self.endDay * 24:
                scheduledLoad, baseLoad, energy_difference, remaining_daily_hours_norm = 0., 0., 0., 0.
            else:
                scheduledLoad = self.scheduledLoad.loc[self.time, 'load'] / self.maxPower
                baseLoad = self.baseLoad.loc[self.time, 'load'] / self.maxPower
                energy_difference = (self.hourlyScheduledLoad - self.hourlyBid) / self.hourlyScheduledLoad
                remaining_daily_hours_norm = (self.time + 1 - self.day * 24) / 24
            return MCP, scheduledLoad, baseLoad, energy_difference, remaining_daily_hours_norm

        elif self.type == 'Wind Generation' or self.type == 'PV Generation':
            power = self.genSeries[self.time]
            return (not multiAgent) * (MCP, power) + multiAgent * (MCP, power, 0., 0., 0.)

        elif self.type == 'Battery Storage':
            SOC = self.SOC
            return (not multiAgent) * (MCP, SOC) + multiAgent * (MCP, SOC, 0., 0., 0.)

        elif self.type == 'Heat Pump':
            storageLevel = self.storageLevel / self.maxStorageLevel
            scheduledLoad = self.scheduledLoad.loc[self.time, 'load'] / self.maxPower
            return (not multiAgent) * (MCP, storageLevel, scheduledLoad) + \
                   multiAgent * (MCP, storageLevel, scheduledLoad, 0., 0.)

        elif self.type == 'E-vehicle':
            if self.time == self.endDay*24:
                SOC, absent, urgency = 0, 0, 0
            else:
                SOC = self.SOC
                absent = self.absenceTimes[self.time]

                # No more absence times in time intervall
                if self.nextAbsence == 0:
                    urgency = 0.

                # The car is abesent and bidding is forbidden
                elif self.nextAbsence <= self.time:
                    urgency = -1.

                # Check whether the SOC(t) > SOC(next consumption) + SOC_min, if yes no urgency to buy
                # if no, then calculate the urgency. The urgency should increase, the lower SOC(t) is and the closer
                # the next absence times is
                else:
                    needed_energy = (self.nextConsumption + self.minCapacity) / self.maxCapacity

                    if max(needed_energy, self.threshold_SOC) - self.SOC <= 0:
                        urgency = 0.
                    else:
                        urgency = min((max(needed_energy, self.threshold_SOC) - self.SOC) /
                                            ((self.nextAbsence - self.time) * self.deltaSOCmax), 1.)
                        if self.nextAbsence == (self.time + 1) and urgency != 0:
                            urgency = 1.

            return (not multiAgent)*(MCP, SOC, absent, urgency) + multiAgent*(MCP, SOC, absent, urgency, 0.)

    def getReward(self):
        spotReward = self.fullRewardTable.loc[self.time - 1, 'reward_spot']
        flexReward = self.fullRewardTable.loc[self.time - 1, 'reward_flex']
        return spotReward, flexReward
