import numpy as np
import pandas as pd
import os
import util
import time

class SpotMarket:
    def __init__(self, input_path = None, spotTimePeriod = 8760, spotHour = 1):
        self.participants = []
        self.bids = {}
        self.bidsDF = None
        self.spotTimePeriod = spotTimePeriod
        self.spotHour = spotHour
        self.spotHour = 1
        self.day = 0
        self.time = 0
        self.hours = None
        self.path = input_path
        # hourly market clearing price
        self.MCP = None
        self.reset()

    def reset(self, startDay=0):
        self.day = startDay
        self.time = self.day * 24
        self.hours = np.arange(self.time, self.time + 1)
        self.nextHours = np.arange(self.time + 1, self.time + 2)

        if self.MCP is None:
            # hourly market clearing price
            self.MCP = pd.DataFrame(data={'time': np.arange(self.spotTimePeriod),
                                          'price': np.zeros(self.spotTimePeriod, dtype=float)})
            self.importPrice()

    def addParticipants(self, participants):
        # participants is a list of FlexAgents
        self.participants.extend(participants)
        agentNameList = [agent.id for agent in self.participants]
        self.bidsDF = pd.DataFrame(np.full((self.spotHour, len(self.participants)), 0.0), columns=agentNameList,
                                   index=self.hours)

    def importPrice(self):
        datapath = os.path.join(self.path, "../inputs/Day-ahead Prices_201901010000-202001010000_neu.csv")
        self.MCP = pd.read_csv(datapath, sep=';', comment='#', header=0, skiprows=0,
                          usecols=['MTU (CET)', 'Day-ahead Price [EUR/MWh]'], error_bad_lines=False)
        self.MCP.columns = ['time', 'price']

    def collectBids(self):
        self.bidsDF.loc[:, :] = 0.0
        self.bidsDF.index=self.hours
        for participant in self.participants:
            participant.makeSpotBid()
            # assert len(participant.getSpotBid().index) == self.spotHour, 'Length of spot market and bid not equal'
            # SpotBid = participant.getSpotBid()
            self.bids[participant.getID()] = participant.fullSpotBid.loc[[self.time], :]
            self.bidsDF.loc[:, participant.id] = participant.fullSpotBid.loc[[self.time], 'qty_bid']
            participant.setMCP(self.MCP.loc[self.nextHours, 'price'].values)

    def sendDispatch(self):
        for participant in self.participants:
            # With the hourly Spot bid version the self.bids of the Spotmarket was a dictonary with the agent's ID and
            # the agent's dataframe (pointer to the dataframe), with slicing the full Spot bid the item inside self.bids
            # is no longer a pointer, it is a new dataframe thus we need to explicitly set dispatched to true
            self.bids[participant.getID()].loc[:, 'dispatched'] = True
            participant.fullSpotBid.loc[self.time, 'dispatched'] = True
            participant.fullSpotBid.loc[self.time, 'MCP'] = self.MCP.loc[participant.hours[0], 'price']
            participant.spotMarketEnd()

    def endDay(self):
        for participant in self.participants:
            participant.time += 1
            if participant.time % 24 == 0:
                participant.day += 1
                if participant.type == 'E-vehicle':
                    participant.nStart = -1  # reset start counter of EVs on a daily basis
            participant.hours = np.arange(participant.time, participant.time + 1)
        # update day for spot market object also
        self.time += 1
        if self.time % 24 == 0:
            self.day += 1
        self.hours = np.arange(self.time, self.time + 1)
        self.nextHours = np.arange(self.time + 1, self.time + 2)

