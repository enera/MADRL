from FlexMarket import FlexMarket
from Grid import Grid
import pandas as pd
import numpy as np
import re
import util
import time as zeit
import xarray as xr


class DSO:
    def __init__(self, grid, start, end, dailySpotTime=24):
        self.flexAgents = []
        self.flexMarket = FlexMarket()
        self.grid = grid
        self.start =start
        self.end = end
        self.dailySpotTime = dailySpotTime
        self.totalTimes = np.arange(self.start * self.dailySpotTime, self.end * self.dailySpotTime)
        self.numTopFivePercent = round(len(self.grid.data) * .05)
        self.numTopTwoAndHalfPercent = round(len(self.grid.data) * .025)
        self.congestionDetails = pd.DataFrame(data={'time': self.totalTimes,
                                                    'congested': np.full(len(self.totalTimes), 0),
                                                    'previous congested': np.full(len(self.totalTimes), 0),
                                                    'top 5 percent loading': np.full(len(self.totalTimes), 0.0),
                                                    'previous top 5 percent loading': np.full(len(self.totalTimes), 0.0),
                                                    'top 2.5 percent loading': np.full(len(self.totalTimes), 0.0),
                                                    'previous top 2.5 percent loading': np.full(len(self.totalTimes), 0.0)},
                                         index=self.totalTimes)

    def reset(self):
        self.flexMarket.reset(self.start)
        self.grid.reset(self.start)
        self.congestionDetails.loc[:, 'previous congested'] = self.congestionDetails.loc[:, 'congested']
        self.congestionDetails.loc[:, 'previous top 5 percent loading'] = self.congestionDetails.loc[:, 'top 5 percent loading']
        self.congestionDetails.loc[:, 'previous top 2.5 percent loading'] = self.congestionDetails.loc[:, 'top 2.5 percent loading']
        self.congestionDetails.loc[:, 'congested'] = 0
        self.congestionDetails.loc[:, 'top 5 percent loading'] = 0.0
        self.congestionDetails.loc[:, 'top 2.5 percent loading'] = 0.0

    def checkCongestion(self, spotBidDF):
        status = self.grid.isCongested(spotBidDF)
        return status

    def addflexAgents(self, agents):
        # agents is a list of FlexAgents
        self.flexAgents.extend(agents)
        self.grid.flexAgents.extend(agents)
        self.grid.importLoadsAndSensi()
        self.flexMarket.addParticipants(self.flexAgents)

    def askFlexibility(self):
        for agent in self.flexAgents:
            agent.spotState = False
        self.flexMarket.collectBids(self.grid.reqdFlexTimes)

    def optFlex(self):
        """choose the optimum flexibility wrt cost, qty and sensitivities"""
        nBids = len(self.flexMarket.bids)
        if len(self.grid.reqdFlexTimes) > 0:
            dispatchStatus = pd.DataFrame(np.full((len(self.grid.reqdFlexTimes), len(self.flexAgents)), False),
                                          columns=[agent.id for agent in self.flexAgents],
                                          index=self.grid.reqdFlexTimes)
            #TODO LG: Lass die for-Schleife durchlaufen und vergleiche vectorisierte und als Liste gespeicherte Variablen
            # Da darf sich nur was an der Anzahl der Leitungen ändern

            bids_vec, currentSensitivity_vec, sortedReqdFlexI_A_vec, congested_vec, impact_vec, sensitivityFlexAgents_vec  = self.getOptFlexData(nBids)
            sensitivityFlexAgents_vec, bids_vec, impact_vec =  self.buildSensiAndImpactDF(bids_vec, currentSensitivity_vec,
                                                                                          congested_vec, impact_vec, sensitivityFlexAgents_vec)

            for i, congestedLine in enumerate(impact_vec.coords['Line_Names'].values):
                """assuming if reqdFlexI_A is -3A(reduce 3A), everything less than -3A eg., -4A, -6A can be options
                    if its positive, everything more than reqdFlexI_A can be options"""
                # TODO LG: Fix if condition for vectorized approach

                posReqdFlex = sortedReqdFlexI_A_vec[sortedReqdFlexI_A_vec.loc[:, congestedLine] > 0]
                negReqdFlex = sortedReqdFlexI_A_vec[sortedReqdFlexI_A_vec.loc[:, congestedLine] < 0]
                options_vec = pd.DataFrame(np.full((len(self.grid.reqdFlexTimes), len(bids_vec.Agent_Names)), False), index = sortedReqdFlexI_A_vec.index, columns = bids_vec.Agent_Names)

                if not posReqdFlex.empty:
                    options_vec.loc[posReqdFlex.index.values, :] = \
                        (impact_vec["sensitivityFlexAgents"].loc[dict(Line_Names=congestedLine, Times = posReqdFlex.index.values)] > \
                                   np.tile(sortedReqdFlexI_A_vec.loc[posReqdFlex.index.values, congestedLine].values.reshape(-1, 1), (1, nBids))).values
                if not negReqdFlex.empty:
                    options_vec.loc[negReqdFlex.index.values, :] = \
                        (impact_vec["sensitivityFlexAgents"].loc[dict(Line_Names=congestedLine, Times = negReqdFlex.index.values)] < \
                                   np.tile(sortedReqdFlexI_A_vec.loc[negReqdFlex.index.values, congestedLine].values.reshape(-1, 1), (1, nBids))).values

                # TODO LG: Low programming skill du Idiot
                options_vec = options_vec.to_numpy()

                lCountNonZero = np.count_nonzero(options_vec, axis=1)
                if any(lCountNonZero >= 1):
                    helper = bids_vec["bids"].loc[dict(qty_price_accepted='price')].where(options_vec).to_pandas().dropna(
                        how='all')
                    dsMultipleOptions = bids_vec["bids"].loc[dict(qty_price_accepted='price')].where(options_vec).to_pandas().dropna(
                        how='all').min(axis = 1, skipna = True)
                    helper = helper.replace(dsMultipleOptions.to_numpy(), True) == True

                    # To replace only the false values in dispatchStatus with only the true values in positive Impacts
                    # we use: dispatchStatus = dispatchStatus | ~dispatchStatus & positiveImpacts_vec, this will be used throughout
                    # this loop since otherwise already dispatched agents could get undispatched for a different line
                    # Example:
                    # a =
                    # 0      1      2      3
                    # 0  True   True  False  False
                    # 1  True  False  False  False
                    #
                    # b =
                    #        0      1      2      3
                    # 0  False  False   True  False
                    # 1   True   True  False   True
                    #
                    # Results in a = a | ~a & b
                    #       0     1      2      3
                    # 0  True  True   True  False
                    # 1  True  True  False   True

                    dispatchStatus.loc[dsMultipleOptions.index, :] = dispatchStatus.loc[dsMultipleOptions.index, :] | \
                                                                     ~ dispatchStatus.loc[dsMultipleOptions.index, :] & helper

                remainingSortedReqdFlexI_A_vec = sortedReqdFlexI_A_vec.loc[self.grid.reqdFlexTimes[lCountNonZero == 0], :]
                if not remainingSortedReqdFlexI_A_vec.empty:
                    remainingImpact_vec = impact_vec["sensitivityFlexAgents"].loc[dict(Times = self.grid.reqdFlexTimes[lCountNonZero == 0])]
                    dispatchStatus = self.chooseMultipleFlexOptions(remainingSortedReqdFlexI_A_vec, remainingImpact_vec,
                                                                    congestedLine, dispatchStatus)

                # Now we only need to update if a new agent got a True!
                new_agent_bids = ~bids_vec['bids'].loc[dict(qty_price_accepted='accepted', Times=self.grid.reqdFlexTimes)].astype(bool) & dispatchStatus
                if new_agent_bids.any():
                    bids_vec['bids'].loc[dict(qty_price_accepted = 'accepted', Times = self.grid.reqdFlexTimes)] = \
                        bids_vec['bids'].loc[dict(qty_price_accepted='accepted', Times=self.grid.reqdFlexTimes)] + new_agent_bids
                    sortedReqdFlexI_A = self.updateRemainingLines(i, bids_vec, congested_vec, sensitivityFlexAgents_vec,
                                                                  sortedReqdFlexI_A_vec, new_agent_bids)
        self.flexMarket.sendDispatch(dispatchStatus)

    def getOptFlexData(self, nBids):
        # TODO LG: find a way to initialize the Dataset in a way that it the accepted column is a boolean instead of a number
        # For now we use 0 and 1 for True or False
        bids_vec = xr.Dataset(
            {
                'bids': (["Times", "Agent_Names", "qty_price_accepted"], np.full((len(self.grid.reqdFlexTimes), nBids, 3), 0.0))
            },
            coords={
                "Times": self.grid.reqdFlexTimes,
                "Agent_Names": self.grid.agentsList,
                "qty_price_accepted": ['qty_bid', 'price', 'accepted']
            }
        )
        # bids_vec['bids'].loc[dict(qty_price_accepted = 'accepted')] = np.full(len((self.grid.reqdFlexTimes), nBids), False)
        currentSensitivity_vec = self.grid.getSensitivity()
        """congested lines/ nodes at this particular time"""
        congested_vec = self.grid.congestionStatus[self.grid.congestionStatus.loc[self.grid.reqdFlexTimes, :]].loc[self.grid.reqdFlexTimes, :].dropna(axis = 1, how = 'all')

        # LG: commented out for the moment
        """
        self.congestionDetails.loc[time, 'congested'] = len(congested)
        loadingTopFivePercent = (self.grid.loading.loc[time, :].abs()*100/
                                 self.grid.data.loc[:, 'I_rated_A']).sort_values(ascending=False)[:self.numTopFivePercent].mean()
        loadingTopTwoAndHalfPercent = (self.grid.loading.loc[time, :].abs()*100/
                                 self.grid.data.loc[:, 'I_rated_A']).sort_values(ascending=False)[:self.numTopTwoAndHalfPercent].mean()
        self.congestionDetails.loc[time, 'top 5 percent loading'] = loadingTopFivePercent
        self.congestionDetails.loc[time, 'top 2.5 percent loading'] = loadingTopTwoAndHalfPercent
        """

        reqdFlexI_A_vec = self.grid.loading.loc[self.grid.reqdFlexTimes, congested_vec.columns].multiply(congested_vec.values).abs() - \
                          self.grid.data.loc[congested_vec.columns, 'I_rated_A'].values

        # Comment LG: sortedReqdFlexI_A_vec is sorted by the time which has the most congestion current. This is not entirely how
        # it is supposed to be because we would like to sort the lines with the highest loading for each required step time
        # but this not work in a vectorized fashion as of now :(

        sortedReqdFlexI_A_vec = reqdFlexI_A_vec[reqdFlexI_A_vec.abs().sum(axis = 0, skipna = True).sort_values(ascending = False).index]
        congested_vec = sortedReqdFlexI_A_vec.columns
        impact_vec = xr.Dataset(
            {
                'impact': (["Times", "Agent_Names", "Line_Names"], np.full((len(self.grid.reqdFlexTimes), nBids, len(congested_vec)), 0.0))
            },
            coords={
                "Times": self.grid.reqdFlexTimes,
                "Agent_Names": self.grid.agentsList,
                "Line_Names": sortedReqdFlexI_A_vec.columns
            }
        )
        """stores the sensi of the agents to the congested lines"""
        sensitivityFlexAgents_vec = xr.Dataset(
            {
                'sensitivityFlexAgents': (["Times", "Agent_Names", "Line_Names"], np.full((len(self.grid.reqdFlexTimes), nBids, len(congested_vec)), 0.0))
            },
            coords={
                "Times": self.grid.reqdFlexTimes,
                "Agent_Names": self.grid.agentsList,
                "Line_Names": sortedReqdFlexI_A_vec.columns
            }
        )
        return bids_vec, currentSensitivity_vec, sortedReqdFlexI_A_vec, congested_vec, impact_vec, sensitivityFlexAgents_vec

    def buildSensiAndImpactDF(self, bids_vec, currentSensitivity_vec, congested_vec, sensitivityFlexAgents_vec, impact_vec):
        for i, (agentID, flexbid) in enumerate(self.flexMarket.bids.items()):
            agentNode = 'Standort_' + re.search("k(\d+)[n,d,l]", agentID).group(1)
            """get the sensitivity of this agent on the congested lines"""
            # TODO: LG make sure that in case of parametersharing the correct nodes are picked
            sensitivity_vec = currentSensitivity_vec.loc[congested_vec, self.grid.nodeSensitivityDict[agentNode]].values
            # Sensitivity vec has on its columns the time and the rows are the congested lines
            sensitivity_vec = np.reshape(sensitivity_vec, (-1, len(self.grid.reqdFlexTimes)))
            sensitivityFlexAgents_vec['impact'].loc[dict(Agent_Names=agentID)] = sensitivity_vec.transpose()
            bids_vec['bids'].loc[dict(Agent_Names=agentID, qty_price_accepted=['qty_bid', 'price'])] = flexbid.loc[self.grid.reqdFlexTimes, ['qty_bid', 'price']]
            # TODO: LG Also this does not work unfortunately :(
            # bids_vec['bids'].loc[dict(Agent_Names=agentID, qty_price_accepted=['accepted'])] = np.full((23, 1), False)
            """determine the impact of flexbid on the lines"""
            impact_vec["sensitivityFlexAgents"].loc[dict(Agent_Names=agentID)] = \
                sensitivity_vec.transpose() * bids_vec['bids'].loc[dict(Agent_Names=agentID, qty_price_accepted='qty_bid')] \
                    .values.reshape(-1, 1)
        return sensitivityFlexAgents_vec, bids_vec, impact_vec

    def chooseMultipleFlexOptions(self, sortedReqdFlexI_A_vec, impact_vec, congestedLine, dispatchStatus):
        """No single agent can reduce congestion, select multiple agents"""

        # TODO LG: In the vectorized case it is possible that some hours require a positive current and some a negative current
        # Optimum case would be to do that all in one, but I have no idea how to do that yet

        # TODO LG convert to dataseries
        posReqdFlex = sortedReqdFlexI_A_vec[sortedReqdFlexI_A_vec.loc[:, congestedLine] > 0]
        negReqdFlex = sortedReqdFlexI_A_vec[sortedReqdFlexI_A_vec.loc[:, congestedLine] < 0]

        if not posReqdFlex.empty:
            """selecting agents having atleast 0.5A positive effect to prevent very small impacts to be selected"""
            positiveImpacts_vec = impact_vec.loc[dict(Line_Names=congestedLine, Times = posReqdFlex.index)].where( \
                impact_vec.loc[dict(Line_Names=congestedLine, Times = posReqdFlex.index)] > 0).to_pandas().dropna(
                axis=1, how='all')
            positiveImpacts_vec = positiveImpacts_vec.dropna(axis=0, how='all')

            if not len(positiveImpacts_vec) == 0:
                """Sum of all positively impacting flexbids is even not sufficient, so select all these bids"""
                hoursAllNotSufficient = positiveImpacts_vec.sum(axis = 1) < posReqdFlex.loc[positiveImpacts_vec.index, congestedLine] # Changed to negReqdFlex!
                if len(hoursAllNotSufficient) > 0:
                    dispatchStatus.loc[hoursAllNotSufficient[hoursAllNotSufficient].index, positiveImpacts_vec.columns.values] = \
                        dispatchStatus.loc[hoursAllNotSufficient[hoursAllNotSufficient].index, positiveImpacts_vec.columns.values] | \
                        ~ dispatchStatus.loc[hoursAllNotSufficient[hoursAllNotSufficient].index, positiveImpacts_vec.columns.values] & \
                        positiveImpacts_vec.loc[hoursAllNotSufficient[hoursAllNotSufficient].index, :].notnull()
                if any(hoursAllNotSufficient == False):

                    # TODO LG: Test this part
                    positiveImpacts_vec_greaterI = positiveImpacts_vec.loc[hoursAllNotSufficient[hoursAllNotSufficient == False].index, :]
                    # Sort so that the agent with the highest cumulative sum over all hours is first
                    positiveImpacts_vec_greaterI = \
                        positiveImpacts_vec_greaterI[positiveImpacts_vec_greaterI.sum(axis=0, skipna=True).sort_values(ascending=False).index] # Check whether ascending should be false for positive current
                    hourlyCurrent = positiveImpacts_vec_greaterI.fillna(0)
                    neededCurrent = posReqdFlex.loc[hoursAllNotSufficient[hoursAllNotSufficient == False].index, congestedLine]
                    cumCurrent = hourlyCurrent.cumsum(axis=1)
                    # congestion is not fully solved by that because the last agent missing which would overshoot the congestion
                    for column in cumCurrent.columns:
                        if not neededCurrent.empty:
                            if any(neededCurrent <= cumCurrent.loc[:, column]) == True:
                                dispatchStatus.loc[cumCurrent.index, column] = dispatchStatus.loc[cumCurrent.index, column] | \
                                                                               ~ dispatchStatus.loc[cumCurrent.index, column] & \
                                                                               positiveImpacts_vec_greaterI.loc[:, column].notnull()
                                helper = neededCurrent
                                neededCurrent = neededCurrent.drop(neededCurrent[neededCurrent >= cumCurrent.loc[:, column]].index)
                                # Also drop cummulated current
                                cumCurrent = cumCurrent.drop(helper[helper >= cumCurrent.loc[:, column]].index)



        if not negReqdFlex.empty:
            # TODO Lutz: Work without to_pandas()
            positiveImpacts_vec = impact_vec.loc[dict(Line_Names=congestedLine, Times = negReqdFlex.index)].where(\
                impact_vec.loc[dict(Line_Names = congestedLine, Times = negReqdFlex.index)] < 0).to_pandas().dropna(axis = 1, how = 'all')
            positiveImpacts_vec = positiveImpacts_vec.dropna(axis = 0, how = 'all')
            if not len(positiveImpacts_vec) == 0:
                """Sum of all positively impacting flexbids is even not sufficient, so select all these bids"""
                hoursAllNotSufficient = positiveImpacts_vec.sum(axis=1) > negReqdFlex.loc[positiveImpacts_vec.index, congestedLine]  # Changed to negReqdFlex!
                if len(hoursAllNotSufficient) > 0:
                    dispatchStatus.loc[hoursAllNotSufficient[hoursAllNotSufficient].index, positiveImpacts_vec.columns.values] = \
                        dispatchStatus.loc[hoursAllNotSufficient[hoursAllNotSufficient].index, positiveImpacts_vec.columns.values] | \
                        ~ dispatchStatus.loc[hoursAllNotSufficient[hoursAllNotSufficient].index, positiveImpacts_vec.columns.values] & \
                        positiveImpacts_vec.loc[hoursAllNotSufficient[hoursAllNotSufficient].index, :].notnull()
                if any(hoursAllNotSufficient == False):
                    positiveImpacts_vec_greaterI = positiveImpacts_vec.loc[hoursAllNotSufficient[hoursAllNotSufficient == False].index, :]
                    # Sort so that the agent with the highest cumulative sum over all hours is first
                    positiveImpacts_vec_greaterI = \
                        positiveImpacts_vec_greaterI[positiveImpacts_vec_greaterI.sum(axis=0, skipna=True).sort_values(ascending=True).index]
                    hourlyCurrent = positiveImpacts_vec_greaterI.fillna(0)
                    neededCurrent = negReqdFlex.loc[hoursAllNotSufficient[hoursAllNotSufficient == False].index, congestedLine]
                    cumCurrent = hourlyCurrent.cumsum(axis=1)
                    # congestion is not fully solved by that because the last agent missing which would overshoot the congestion
                    for column in cumCurrent.columns:
                        if not neededCurrent.empty:
                            if any(neededCurrent >= cumCurrent.loc[:, column]) == True:
                                dispatchStatus.loc[cumCurrent.index, column] = dispatchStatus.loc[cumCurrent.index, column] | \
                                                                               ~ dispatchStatus.loc[cumCurrent.index, column] & \
                                                                               positiveImpacts_vec_greaterI.loc[:, column].notnull()
                                helper = neededCurrent
                                neededCurrent = neededCurrent.drop(neededCurrent[neededCurrent >= cumCurrent.loc[:, column]].index)
                                cumCurrent = cumCurrent.drop(helper[helper >= cumCurrent.loc[:, column]].index)
        return dispatchStatus

    def updateRemainingLines(self, i, bids_vec, congested_vec, sensitivityFlexAgents_vec, sortedReqdFlexI_A_vec, new_agent_bids):
        """change the loading of the remaining lines according to the dispatched flexibilities"""
        if i < len(congested_vec) - 1:
            acceptedBids_vec_vec = (bids_vec["bids"].loc[dict(qty_price_accepted="qty_bid")] * new_agent_bids)
            acceptedBids_vec_vec = np.tile(acceptedBids_vec_vec.transpose(), (len(congested_vec[i + 1:]), 1, 1)).transpose()
            sensi_vec_vec = sensitivityFlexAgents_vec["impact"].loc[dict(Line_Names = congested_vec[i + 1:])]
            self.grid.loading.loc[self.grid.reqdFlexTimes, congested_vec[i + 1:]] += (acceptedBids_vec_vec * sensi_vec_vec).sum(dim="Agent_Names").to_pandas()

            # In SortedReqdFlex there are Nan values for a line which is not congested in this hour but in another in the whole reqdFlexTime
            # We do not want to change the nan there
            dontChangeNan = sortedReqdFlexI_A_vec.loc[:, congested_vec[i + 1:]]
            # Nan's will stay, the other values will be changed to one
            dontChangeNan[~dontChangeNan.isnull()] = 1
            sortedReqdFlexI_A_vec.loc[:, congested_vec[i + 1:]] = (np.abs(self.grid.loading.loc[self.grid.reqdFlexTimes, congested_vec[i + 1:]]) -
                self.grid.data.loc[congested_vec[i + 1:],'I_rated_A'])*dontChangeNan
        return sortedReqdFlexI_A_vec

    def endDay(self):
        for agent in self.flexAgents:
            agent.spotState = True
        self.flexMarket.endDay()
        self.grid.endDay()
