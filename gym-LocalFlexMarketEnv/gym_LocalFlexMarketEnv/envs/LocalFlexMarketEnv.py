import gym
from gym import spaces
import numpy as np
import pandas as pd
import time
import util


class LocalFlexMarketEnv(gym.Env):
    """A local flexibility market environment for OpenAI gym"""
    metadata = {'render.modes': ['human']}

    def __init__(self, SpotMarket, DSO, alg):
        super().__init__()
        self.SpotMarket = SpotMarket
        self.DSO = DSO
        self.alg = alg
        self.nAgents = len(self.SpotMarket.participants)
        self.agents = self.SpotMarket.participants
        self.time = 0
        self.startDay = 0
        self.endDay = self.SpotMarket.spotTimePeriod / self.SpotMarket.spotHour
        self.checkTime = False
        self.total_action_space = []
        self.total_observation_space = []

        max_obs_space = max([agent.obs_space_size for agent in self.agents])
        if self.alg == 'MADDPG':
            self.multiAgent = True
        else:
            self.multiAgent = False

        for agent in self.agents:
            total_action_space = []
            if self.alg == 'MADDPG':
                minLimit, maxLimit = agent.getActionLimits(self.alg)
                self.action_space = spaces.Box(low=minLimit, high=maxLimit, dtype=np.float32)
                self.observation_space = spaces.Box(low=-np.inf, high=+np.inf,
                                                    shape=(max_obs_space * self.SpotMarket.spotHour,),
                                                    dtype=np.float32)

            elif self.alg == 'DDPG':
                minLimit, maxLimit = agent.getActionLimits(self.alg)
                self.action_space = spaces.Box(low=minLimit, high=maxLimit, dtype=np.float32)
                self.observation_space = spaces.Box(low=-np.inf, high=+np.inf,
                                                    shape=(agent.obs_space_size * self.SpotMarket.spotHour,),
                                                    dtype=np.float32)

            elif self.alg == 'DQN':
                self.action_space = spaces.Discrete(len(agent.action_dict))
                self.observation_space = spaces.Box(low=-np.inf, high=+np.inf,
                                                    shape=(agent.obs_space_size * self.SpotMarket.spotHour,),
                                                    dtype=np.float32)

            total_action_space.append(self.action_space)
            if len(total_action_space) > 1:
                self.total_action_space.append(spaces.Tuple(total_action_space))
            else:
                self.total_action_space.append(total_action_space[0])
            self.total_observation_space.append(self.observation_space)

    def step(self, action):
        obs = []
        reward = []
        done = []
        info = {'x': []}
        # set action for each agent
        for i, agent in enumerate(self.agents):
            self._set_action(action[i], agent)

        self.spotStep()
        self.flexStep()

        # record observation for each agent
        i = 0
        for agent in self.agents:
            obs.append(self._get_obs(agent))
            reward.append(self._get_reward(agent))
            done.append(self._get_done())
            info['x'].append({})
            i += 1
        self.time += 1

        return obs, reward, done, info

    def reset(self):
        self.time = self.startDay * 24

        # reset the environment
        self.SpotMarket.reset(self.startDay)
        self.DSO.reset()
        for agent in self.agents:
            agent.reset(self.startDay)
            # Set correct MCP for start Day
            agent.MCP = [self.SpotMarket.MCP.loc[self.time, 'price']]
            # Set MCP or Marginal costs as flex Bid price depending on which is higher
            agent.fullFlexBid.loc[:, 'price'] = \
                (agent.marginalCost < self.SpotMarket.MCP.loc[agent.fullFlexBid.index, 'price']) * \
                self.SpotMarket.MCP.loc[agent.fullFlexBid.index, 'price'] + \
                (agent.marginalCost >= self.SpotMarket.MCP.loc[agent.fullFlexBid.index, 'price']) * agent.marginalCost
            agent.mean_MCP = np.mean(self.SpotMarket.MCP.loc[agent.totalHours, 'price'])
            agent.minMCP = self.SpotMarket.MCP.loc[agent.totalHours, 'price'].min()
            agent.maxMCP = self.SpotMarket.MCP.loc[agent.totalHours, 'price'].max()

        # record observations for each agent
        obs = []
        for agent in self.agents:
            obs.append(self._get_obs(agent))
        return obs

    # get info used for evaluation
    def _get_info(self):
        return {}

    # get observation for a particular agent
    def _get_obs(self, agent):
        obs_tuple = agent.getObservation(self.multiAgent)
        return np.hstack(obs_tuple)

    # get dones for a particular agent
    def _get_done(self):
        if self.SpotMarket.day + 1 > self.endDay:
            return True
        else:
            return False

    # get reward for a particular agent
    def _get_reward(self, agent):
        spotReward, flexReward = agent.getReward()
        if agent.type == 'Wind Generation':  # "normalizing" rewards since wind is a lot higher than all other rewards
            spotReward /= 100
            flexReward /= 100
        return spotReward + flexReward

    # Set correct action and observation space spec. The spot bid multiplier is defined on a different intervall for
    # different agents. Before the DDPG Agent is created the correct action and observation space is set
    def set_correct_space_spec(self, index):
        self.action_space = self.total_action_space[index]
        self.observation_space = self.total_observation_space[index]

    # Set env action for a particular agent
    def _set_action(self, action, agent):
        # Modification of discrete action to list of [sbm, fbm, pbm]
        if self.alg == 'DQN':
            agent.spotBidMultiplier = np.array([agent.action_dict[action][0]])
            agent.flexBidMultiplier = np.array([agent.action_dict[action][1]])
            agent.flexBidPriceMultiplier = np.array([agent.action_dict[action][2]])

        elif self.alg == 'MADDPG' or self.alg == "DDPG":
            agent.spotBidMultiplier = action[:self.SpotMarket.spotHour]
            agent.flexBidMultiplier = action[self.SpotMarket.spotHour:2 * self.SpotMarket.spotHour]
            agent.flexBidPriceMultiplier = action[2 * self.SpotMarket.spotHour:]

    def spotStep(self):
        lastTime = time.time()
        self.SpotMarket.collectBids()
        self.SpotMarket.sendDispatch()
        if self.checkTime:
            util.checkTime(lastTime, 'Spot step')

    def flexStep(self):
        lastTime = [time.time()]
        flex_step = True
        # this function checks whether the grid is congested and if yes, sets the reqdFlexTimes
        isCongested = self.DSO.checkCongestion(self.SpotMarket.bidsDF)
        dispatchStatus = pd.DataFrame(np.full((self.agents[0].spotHour, len(self.agents)), False),
                                      columns=[agent.id for agent in self.agents], index=self.agents[0].hours)
        if isCongested and flex_step:
            if self.checkTime:
                util.checkTime(lastTime[-1], 'Power flow approximation')
                lastTime.append(time.time())
            self.DSO.askFlexibility()
            if self.checkTime:
                util.checkTime(lastTime[-1], 'Getting flex bids')
                lastTime.append(time.time())
            dispatchStatus = self.DSO.optFlex(dispatchStatus)
            if self.checkTime:
                util.checkTime(lastTime[-1], 'choosing the flexibility')

        self.DSO.flexMarket.sendDispatch(dispatchStatus, isCongested, flex_step)
        self.DSO.endDay()
        self.SpotMarket.endDay()

        if self.checkTime:
            util.checkTime(lastTime[0], 'Flex step total')

    def render(self, mode='human'):
        if self.time % 20 == 0:
            for agent in self.agents:
                print("Agent ID: {} Reward: {}".format(agent.id, agent.getReward()))

