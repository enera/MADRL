import numpy as np
import time
import util

class FlexMarket:
    def __init__(self):
        self.participants = []
        self.bids = {}
        self.reqdFlexTimes = None
        self.flexHour = 1
        self.day = 0
        self.time = 0
        self.hours = None
        self.reset()

    def reset(self, startDay=0):
        self.day = startDay
        self.time = self.day * 24
        self.hours = np.arange(self.time, self.time + 1)

    def addParticipants(self, participants):
        # participants is a list of FlexAgents
        self.participants.extend(participants)

    def collectBids(self, reqdFlexTimes):
        self.reqdFlexTimes = reqdFlexTimes
        for participant in self.participants:
            participant.makeFlexBid(self.reqdFlexTimes)
            self.bids[participant.id] = participant.getFlexBid()

    def sendDispatch(self, flexDispatchStatus, isCongested, flex=True):
        for participant in self.participants:
            if flexDispatchStatus is not None and isCongested and flex:
                self.bids[participant.id].loc[:, 'dispatched'] = flexDispatchStatus.loc[:, participant.id].values
                # setting explicitly
                participant.fullFlexBid.loc[self.reqdFlexTimes, 'dispatched'] = \
                    flexDispatchStatus[participant.id].values
                participant.flexMarketEnd()

            elif not isCongested or not flex:
                if participant.type == "Wind Generation" or participant.type == "PV Generation":
                    pass
                else:
                    participant.flexMarketEnd()

    def endDay(self):
        # update day and dailyTimes after a day even if flex is not needed
        self.time += 1
        if self.time % 24 == 0:
            self.day += 1
        self.hours = np.arange(self.time, self.time + 1)
