import numpy as np
from stable_baselines3 import DDPG, TD3, DQN
from stable_baselines3.common.noise import NormalActionNoise, AdaptiveParamNoiseSpec, OrnsteinUhlenbeckActionNoise
import torch as th
from torch import nn
from typing import Any, Type, Dict, Tuple
import gym


class MADDPGAgent:
    """
    Class MADDPG Agent which sets the fixed Hyperparameters of the MADDPG agent according to the original paper

    """

    def __init__(self):
        # Hyperparameters
        self.learning_rate: float = 1e-3
        self.fc_layer_params_actor: Tuple = (400, 300)
        self.fc_layer_params_critic: Tuple = (400, 300)
        self.tf_agents_weights: bool = True
        self.TD3: bool = False
        self.prioritizedReplay: bool = False
        self.paramNoise: bool = False
        self.sigma: float = 0.1

    def hyperParameterInit(self, parameterDict: Dict[Any, str]) -> None:
        """
        Set Hyperparameter; only used with Optuna Hyperparameter seach

        :param parameterDict: Dictionary containing parameters of the specific trial
        """
        if parameterDict is not None:
            """hyperparameter optimization with optuna"""
            self.learning_rate = parameterDict['learning_rate']
            self.fc_layer_params_actor = parameterDict['fc_layer_params_actor']
            self.fc_layer_params_critic = parameterDict['fc_layer_params_critic']
            self.tf_agents_weights = parameterDict['tf_agents_weights']
            self.tau = parameterDict["tau"]
            self.TD3 = parameterDict["TD3"]
            self.paramNoise = parameterDict["ParamNoise"]
            self.sigma = parameterDict["sigma"]

    def initialize(self, env: gym.Env, index: int, total_steps: int, gradient_steps: int) -> None:
        """
        "True" initialize function of the MADDPGAgent Class. This function initilizes the ddpg object of SB3

        :param env: Gym environment
        :param index: integer containing the position of the to be initialized agent in the list of all agents
        :param total_steps: Number of total collection steps
        """

        policy_kwargs = dict(
            net_arch=dict(
                pi=list(self.fc_layer_params_actor),
                qf=list(self.fc_layer_params_critic),
                n_Agents=len(env.agents),
            )
        )

        # The noise objects for DDPG. It was observed that the OU Noise is quite bad to use because it correlates
        # and usually explores in one direction while collecting
        n_actions = env.total_action_space[index].shape[-1]
        # action_noise = OrnsteinUhlenbeckActionNoise(mean=np.zeros(n_actions), sigma= 0.2 * np.ones(n_actions))
        action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=self.sigma / 2 * np.ones(n_actions))
        param_noise = AdaptiveParamNoiseSpec(initial_stddev=self.sigma, desired_action_stddev=self.sigma)
        assert isinstance(param_noise, AdaptiveParamNoiseSpec), \
            "Param Noise has to be defined as an instance of AdaptiveParamNoise"
        param_noise_update_interval = int(gradient_steps / 2) + 1

        # Set the correct action and observation space for the env, because some agents have differently defined
        # intervals on which the spot and flex bid multiplier is defined. This has to be ensured for the definition
        # of actor and critic

        env.set_correct_space_spec(index)
        if self.paramNoise:
            action_noise = None
        else:
            param_noise = None

        from stable_baselines3.td3 import MlpPolicy

        if self.TD3:
            self.agent = TD3(policy=MlpPolicy, policy_kwargs=policy_kwargs, env=env, action_noise=action_noise, seed=0,
                              param_noise=param_noise, agent_index=index, gradient_steps=gradient_steps,
                              learning_starts=1, param_noise_adaption_interval=param_noise_update_interval)
        else:
            self.agent = DDPG(policy=MlpPolicy, policy_kwargs=policy_kwargs, env=env, action_noise=action_noise, seed=0,
                              param_noise=param_noise, agent_index=index, gradient_steps=gradient_steps,
                              learning_starts=1, param_noise_adaption_interval=param_noise_update_interval)


class DDPG_Agent:
    """
    Class Single Agent DDPG initialized with its original hyperparameters from the paper (which is given through SB3)

    """

    def __init__(self):
        # Hyperparameters
        self.learning_rate: float = 1e-3
        self.paramNoise: bool = False
        self.gaussianNoise: bool = True
        self.sigma: float = 0.2
        self.TD3: bool = False

    def hyperParameterInit(self, parameterDict: Dict[Any, str]) -> None:
        """
        Set Hyperparameter; only used with Optuna Hyperparameter seach

        :param parameterDict: Dictionary containing parameters of the specific trial
        """
        if parameterDict is not None:
            """hyperparameter optimization with optuna"""
            self.learning_rate = parameterDict['learning_rate']

    def initialize(self, env: gym.Env, index: int, gradient_steps: int) -> None:
        """
        "True" initialize function of the DDPG Class. This function initilizes the ddpg object of SB3

        :param gradient_steps: Number of gradient steps
        :param env: Gym environment
        :param index: integer containing the position of the to be initialized agent in the list of all agents
        :param total_steps: Number of total collection steps
        """

        from stable_baselines3.td3 import MlpPolicy

        # The noise objects for DDPG. It was observed that the OU Noise is quite bad to use because it correlates
        # and usually explores in one direction while collecting
        n_actions = env.total_action_space[index].shape[-1]
        if self.gaussianNoise:
            action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=self.sigma * np.ones(n_actions))
        else:
            action_noise = OrnsteinUhlenbeckActionNoise(mean=np.zeros(n_actions), sigma=self.sigm * np.ones(n_actions))

        param_noise = AdaptiveParamNoiseSpec(initial_stddev=self.sigma, desired_action_stddev=self.sigma)
        assert isinstance(param_noise, AdaptiveParamNoiseSpec), \
            "Param Noise has to be defined as an instance of AdaptiveParamNoise"
        param_noise_update_interval = int(gradient_steps / 2) + 1

        # Set the correct action and observation space for the env, because some agents have differently defined
        # intervals on which the spot and flex bid multiplier is defined. This has to be ensured for the definition
        # of actor and critic

        env.set_correct_space_spec(index)
        if self.paramNoise:
            action_noise = None
        else:
            param_noise = None

        if self.TD3:
            self.agent = TD3(policy=MlpPolicy, env=env, action_noise=action_noise, param_noise=param_noise, seed=0,
                              agent_index=0, gradient_steps=gradient_steps, learning_starts=1e5,
                              param_noise_adaption_interval=param_noise_update_interval)
        else:
            self.agent = DDPG(policy=MlpPolicy, env=env, action_noise=action_noise, param_noise=param_noise, seed=0,
                              agent_index=0, gradient_steps=gradient_steps, learning_starts=1e5,
                              param_noise_adaption_interval=param_noise_update_interval)


class DQN_Agent:
    """
    Class DQN Agent which sets the fixed Hyperparameters of the DQN agent

    """

    def __init__(self):
        self.learning_rate: float = 1e-4
        self.learning_starts: int = 50000

    def hyperParameterInit(self, parameterDict: Dict[Any, str]) -> None:
        """
        Set Hyperparameter; only used with Optuna Hyperparameter seach

        :param parameterDict: Dictionary containing parameters of the specific trial
        """
        if parameterDict is not None:
            """hyperparameter optimization with optuna"""
            self.learning_rate = parameterDict['learning_rate']
            self.learning_starts = parameterDict['learning_starts']

    def initialize(self, env: gym.Env, index: int, learning_starts: int = 0) -> None:
        """
        "True" initialize function of the MADDPGAgent Class. This function initializes the ddpg object of SB3

        :param env: Gym environment
        :param index: integer containing the position of the to be initialized agent in the list of all agents
        :param total_steps: Number of total collection steps
        """

        from stable_baselines3.dqn import MlpPolicy

        env.set_correct_space_spec(index)
        self.agent = DQN(MlpPolicy, env=env, seed=0, learning_starts=50000, target_update_interval=1e6,
                         exploration_final_eps=0.1)

