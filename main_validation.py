import time
import random
import torch as th
import gym
import yaml
from stable_baselines3.common.evaluation import evaluate_policy
from multiagent.environmentlg import MultiAgentEnvLG
import multiagent.scenarios as scenarios
from stable_baselines3.common import evaluation
from AgentNeuralNet import MADDPGAgent, DQN_Agent
import numpy as np
from stable_baselines3 import DDPG, TD3, DQN
from stable_baselines3.common.noise import NormalActionNoise, AdaptiveParamNoiseSpec


if __name__ == '__main__':

    alg = "DQN"

    if alg == "MADDPG":

        from stable_baselines3.td3 import MlpPolicy
        print("---------------------------------------Single Agent validation----------------------------------------")

        single_agent_env = []
        env1 = gym.make('Pendulum-v0')
        single_agent_env.append(env1)
        env2 = gym.make('LunarLanderContinuous-v2')
        single_agent_env.append(env2)

        """Lists to append and save data for analysis"""
        totalReturns, loss, lActor_loss, lCritic_loss, lActions = [], [], [], [], []

        """Training parameters"""
        num_iterations = [20000, 500000]
        batch_size = 100
        mean_reward_goal = [-170, 200]
        learning_starts = [200, 0]

        for env_number, env in enumerate(single_agent_env):

            random.seed(0)
            np.random.seed(0)
            th.manual_seed(0)

            RLAgents = []
            done_n = [False]

            try:
                env.agents
            except:
                env.agents = [1]

            n_actions = env.action_space.shape[-1]
            sigma = 0.1
            action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=sigma * np.ones(n_actions))

            for i in range(len(env.agents)):
                Agent = MADDPGAgent()
                Agent.agent = DDPG(policy=MlpPolicy, env=env, action_noise=action_noise, agent_index=0, seed=0,
                                   learning_starts=learning_starts[env_number])
                RLAgents.append(Agent.agent)

            num_iter = 0
            episode_rewards = [0.0]  # sum of rewards for all agents
            agent_rewards = [[0.0] for _ in range(len(env.agents))]
            lActor_loss = [[0.0] for _ in range(len(env.agents))]
            lCritic_loss = [[0.0] for _ in range(len(env.agents))]
            episode_number = 0
            episode_steps = 0
            episode_length = np.inf
            t_start = time.time()
            startTrain = False

            env = RLAgents[0].env

            obs_n = env.reset()
            for i, agent in enumerate(RLAgents):
                agent._last_obs = obs_n[i]

            while num_iter < num_iterations[env_number]:
                episode_number += 1
                episode_steps = 0
                while not any(done_n):
                    """Collect a few steps using collect_policy and save to the replay buffer."""
                    episode_steps += 1
                    # Get actions from agents
                    action_n, buffer_action_n = [], []
                    for agent in RLAgents:
                        if agent.learning_starts > num_iter:
                            action, buffer_action = agent._sample_action(learning_starts=agent.learning_starts,
                                                                         action_noise=agent.action_noise)
                            action = action[0]
                            buffer_action = buffer_action[0]
                        else:
                            action, buffer_action = agent._sample_action(learning_starts=0,
                                                                         action_noise=agent.action_noise)
                        action_n.append(action)
                        buffer_action_n.append(buffer_action)

                    # Step through the environment
                    # environment step
                    new_obs_n, rew_n, done_n, info_n = env.step(action_n)

                    # Store experience in Replay Buffer
                    for i, agent in enumerate(RLAgents):
                        agent._last_obs = new_obs_n[i]
                        agent.replay_buffer.add(obs_n[i], new_obs_n[i], buffer_action_n[i], rew_n[i], done_n[i])
                    obs_n = new_obs_n

                    for i, rew in enumerate(rew_n):
                        episode_rewards[-1] += rew
                        agent_rewards[i][-1] += rew
                    num_iter += 1

                    if num_iter % episode_length == 0 or any(done_n):
                        done_n = []
                        [done_n.append(True) for agent in RLAgents]
                        episode_rewards.append(0)
                        for a in agent_rewards:
                            a.append(0)
                        for a in lActor_loss:
                            a.append(0)
                        for a in lCritic_loss:
                            a.append(0)

                printFreq = 10
                if episode_number % printFreq == 0:
                    print("steps: {}, episodes: {}, mean episode reward: {}, time: {}".format(
                        num_iter, len(episode_rewards) - 1, np.mean(episode_rewards[-printFreq:]),
                        round(time.time() - t_start, 3)))
                    startTrain = True
                    t_start = time.time()

                done_n = []
                [done_n.append(False) for agent in RLAgents]
                [agent.action_noise.reset() for agent in RLAgents]
                if env.envs[
                    0].spec.id == 'LunarLanderContinuous-v2':  # two different training schemes for Pendulum and Lunar Lander
                    episode_steps = 200
                for _ in range(episode_steps):
                    for i, agent in enumerate(RLAgents):
                        # Adapt param noise, if necessary.
                        if agent.param_noise is not None and agent._n_updates % agent.param_noise_adaption_interval == 0:
                            distance = agent._adapt_param_noise(batch_size)
                        actor_loss, critic_loss, act = agent.trainMADDPG(RLAgents, batch_size)
                        lActor_loss[i][-1] += actor_loss
                        lCritic_loss[i][-1] += critic_loss

            mean_rew, std_rew = evaluation.evaluate_policy(RLAgents[0], env, 100)
            print("Mean Reward in env {} is {}. This should be better than {}".format(
                env.envs[0].spec.id, mean_rew, mean_reward_goal[env_number]))

        print(
            "------------------------------------------Multi Agent validation------------------------------------------")

        random.seed(0)
        np.random.seed(0)
        th.manual_seed(0)

        # load scenario from script
        scenario = scenarios.load("simple_spread" + ".py").Scenario()
        # create world
        world = scenario.make_world()
        # create multiagent environment
        env = MultiAgentEnvLG(world, scenario.reset_world, scenario.reward, scenario.observation)

        """Lists to append and save data for analysis"""
        totalReturns, loss, lActor_loss, lCritic_loss, lActions = [], [], [], [], []

        """Training parameters"""
        num_iterations = 1e6
        batch_size = 1024
        mean_reward_goal = -460
        gradient_steps = 1

        RLAgents = []
        done = []

        """Hyperparameter definition for MA case"""
        learning_rate = 0.005
        fc_layer_params_actor = (64, 64)
        fc_layer_params_critic = (64, 64)
        discount_factor = 0.95
        tf_agents_weights = True
        tau = 0.01
        sigma = 0.1

        policy_kwargs = dict(
            net_arch=dict(
                pi=list(fc_layer_params_actor),
                qf=list(fc_layer_params_critic),
                n_Agents=len(env.total_action_space)
            )
        )

        for index in range(len(env.agents)):
            Agent = MADDPGAgent()
            n_actions = env.total_action_space[index].shape[-1]
            action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=sigma * np.ones(n_actions))
            Agent.agent = DDPG(policy=MlpPolicy, policy_kwargs=policy_kwargs, env=env, action_noise=action_noise,
                               learning_rate=learning_rate, learning_starts=0, gamma=discount_factor,
                               seed=0, tau=tau, agent_index=index)
            RLAgents.append(Agent.agent)

        num_iter = 0
        episode_rewards = [0.0]  # sum of rewards for all agents
        agent_rewards = [[0.0] for _ in range(len(env.agents))]
        lActor_loss = [[0.0] for _ in range(len(env.agents))]
        lCritic_loss = [[0.0] for _ in range(len(env.agents))]
        episode_number = 0
        episode_length = 25
        t_start = time.time()
        startTrain = False

        obs_n = env.reset()
        for i, agent in enumerate(RLAgents):
            agent._last_obs = obs_n[i]

        while num_iter < num_iterations:
            episode_number += 1
            while not any(done):
                """Collect a few steps using collect_policy and save to the replay buffer."""

                # Get actions from agents
                action_n, buffer_action_n = [], []
                for agent in RLAgents:
                    action, buffer_action = agent._sample_action(learning_starts=agent.learning_starts,
                                                                 action_noise=agent.action_noise)
                    action_n.append(action)
                    buffer_action_n.append(buffer_action)

                # Step through the environment
                # environment step
                new_obs_n, rew_n, done_n, info_n = env.step(action_n)

                # Store experience in Replay Buffer
                for i, agent in enumerate(RLAgents):
                    agent._last_obs = new_obs_n[i]
                    agent.replay_buffer.add(obs_n[i], new_obs_n[i], buffer_action_n[i], rew_n[i], done_n[i])
                obs_n = new_obs_n

                for i, rew in enumerate(rew_n):
                    episode_rewards[-1] += rew
                    agent_rewards[i][-1] += rew
                num_iter += 1

                if num_iter % episode_length == 0:
                    done = []
                    [done.append(True) for agent in RLAgents]
                    obs_n = env.reset()
                    for i, agent in enumerate(RLAgents):
                        agent._last_obs = obs_n[i]
                    episode_rewards.append(0)
                    for a in agent_rewards:
                        a.append(0)
                    for a in lActor_loss:
                        a.append(0)
                    for a in lCritic_loss:
                        a.append(0)

            if episode_number % 1000 == 0:
                print("steps: {}, episodes: {}, mean episode reward: {}, time: {}".format(
                    num_iter, len(episode_rewards) - 1, np.mean(episode_rewards[-1000:]),
                    round(time.time() - t_start, 3)))
                startTrain = True
                t_start = time.time()

            done = []
            [done.append(False) for agent in RLAgents]
            [agent.action_noise.reset() for agent in RLAgents]
            if startTrain and num_iter % 100 == 0:
                for _ in range(gradient_steps):
                    for i, agent in enumerate(RLAgents):
                        # Adapt param noise, if necessary.
                        if agent.param_noise is not None and agent._n_updates % agent.param_noise_adaption_interval == 0:
                            distance = agent._adapt_param_noise(batch_size)
                        actor_loss, critic_loss, act = agent.trainMADDPG(RLAgents, batch_size)
                        lActor_loss[i][-1] += actor_loss
                        lCritic_loss[i][-1] += critic_loss

        obs_n = env.reset()
        counter = 0
        eval_rew = [0]
        while True:
            actions = []
            for i, agent in enumerate(RLAgents):
                action, _ = agent.predict(obs_n[i])
                actions.append(action)
            lActions.append(actions)
            obs_n, rew, _, _ = env.step(actions)
            for r in rew:
                eval_rew[-1] += r
            counter += 1
            if counter % 25 == 0:
                obs_n = env.reset()
                eval_rew.append(0)
            if counter % 2500 == 0:
                break
        mean_reward = np.mean(eval_rew[:-1])
        print("Mean Reward in env {} is {}. This should be better than {}".format(
            "Simple Spread", mean_reward, mean_reward_goal))

    elif alg == "DQN":
        print("--------------------------------------Single Agent validation------------------------------------------")

        from stable_baselines3.dqn import MlpPolicy
        import util

        single_agent_env = []
        env1 = gym.make('CartPole-v0')
        single_agent_env.append(env1)
        env2 = gym.make('LunarLander-v2')
        single_agent_env.append(env2)

        """Lists to append and save data for analysis"""
        totalReturns, loss, lActor_loss, lCritic_loss, lActions = [], [], [], [], []

        """Training parameters"""
        num_iterations = [200000, 100000]
        batch_size = 100
        mean_reward_goal = [195., 200.]

        for env_number, env in enumerate(single_agent_env):

            random.seed(0)
            np.random.seed(0)
            th.manual_seed(0)

            RLAgents = []
            done_n = [False]

            try:
                env.agents
            except:
                env.agents = [1]

            for i in range(len(env.agents)):
                Agent = DQN_Agent()
                Agent.agent = DQN(policy=MlpPolicy, env=env, seed=0)
                RLAgents.append(Agent.agent)

            num_iter = 0
            episode_rewards = [0.0]  # sum of rewards for all agents
            agent_rewards = [[0.0] for _ in range(len(env.agents))]
            episode_number = 0
            episode_steps = 0
            episode_length = np.inf
            t_start = time.time()
            startTrain = False

            env = RLAgents[0].env

            obs_n = env.reset()
            for i, agent in enumerate(RLAgents):
                agent._last_obs = obs_n[i]
                agent._total_timesteps = num_iterations[env_number]

            while num_iter < num_iterations[env_number]:
                episode_number += 1
                episode_steps = 0
                while not any(done_n):
                    """Collect a few steps using collect_policy and save to the replay buffer."""
                    num_iter += 1
                    episode_steps += 1
                    obs_n, rew_n, done_n = util.collect_step(env, RLAgents, obs_n, alg)

                    if episode_steps > 200:
                        done_n = [True]

                    for i, rew in enumerate(rew_n):
                        episode_rewards[-1] += rew
                        agent_rewards[i][-1] += rew

                    if any(done_n):
                        done_n = []
                        [done_n.append(True) for agent in RLAgents]
                        episode_rewards.append(0)
                        for a in agent_rewards:
                            a.append(0)

                printFreq = 10
                if episode_number % printFreq == 0:
                    print("steps: {}, episodes: {}, mean episode reward: {}, time: {}".format(
                        num_iter, len(episode_rewards) - 1, np.mean(episode_rewards[-printFreq:]),
                        round(time.time() - t_start, 3)))
                    startTrain = True
                    t_start = time.time()

                done_n = []
                [done_n.append(False) for agent in RLAgents]
                # Two different training schemes for Pendulum and Lunar Lander
                if env.envs[0].spec.id == 'LunarLander-v2':
                    episode_steps = 200
                if num_iter >= RLAgents[0].learning_starts:
                    for _ in range(episode_steps):
                        for i, agent in enumerate(RLAgents):
                            agent.train(batch_size=batch_size)

            mean_rew, std_rew = evaluation.evaluate_policy(RLAgents[0], env, 100)
            print("Mean Reward in env {} is {}. This should be better than {}".format(
                env.envs[0].spec.id, mean_rew, mean_reward_goal[env_number]))
