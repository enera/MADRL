import numpy as np
import pandas as pd
from FlexAgent import FlexAgent
from typing import List


class DSM(FlexAgent):
    """
        Demand Side Management Class

        :param id: ID of agent
        :param location: Location of the agent the DSM
        :param maxPower: Maximum power of the initialized DSM
        :param marginalCost: Marginal cost of production
        :param scheduledLoad: Pandas DataFrame which contains the yearly load of the DSM
        :param startDay: Starting Day of the simulation
        :param endDay: Ending Day of the simulation
        """

    def __init__(
            self,
            id: str,
            location: List = None,
            maxPower: float = 0.,
            marginalCost: float = 0.,
            scheduledLoad: pd.DataFrame = None,
            startDay: int = 0,
            endDay: int = 365
    ):
        super().__init__(
            id=id,
            location=location,
            maxPower=maxPower,
            marginalCost=marginalCost,
            startDay=startDay,
            endDay=endDay
        )

        self.type = "Demand Side Management"
        self.scheduledLoad = pd.DataFrame(data={'time': scheduledLoad.index,
                                                'load': scheduledLoad}, index=scheduledLoad.index)
        self.baseLoad = pd.DataFrame(data={'time': scheduledLoad.index,
                                           'load': 0.2 * scheduledLoad}, index=scheduledLoad.index)

        self.hourlyScheduledLoad = 0.
        self.hourlyBid = 0.

        self.penaltyViolation = -10
        self.penaltyTotalLoadViolation = -200
        self.penalizeTimes = []

        """spotBidMultiplier of DSM agent is used to redistribute the difference (P_total - P_base)
            of each hour : To incorporate the constraints
                                    if for each hour P_total > Pmax --> Negative rewards
                                    if total of 24 hr P_total > scheduledLoad --> Negative rewards
                                    if total of 24 hr P_total < scheduledLoad --> Negative rewards"""

        # REVERTED
        # LG modification of spot bid limits from [0, 2] to [-1, 1] afterwards we will add 1 at the required positions
        self.lowSpotBidLimit = 0
        self.highSpotBidLimit = 2
        """in flex , can "sell" qty ie, to reduce the qty from the spot dispatched amount 
                    can buy qty to increase the qty from the spot dispatched amount"""
        self.lowFlexBidLimit = -1
        self.highFlexBidLimit = 1

        self.obs_space_size = 5

        self.action_dict = \
            {
                0: [0, 0],
                1: [1, 0],
                2: [1, 1],
                3: [1, -1],
                4: [2, 0],
                5: [2, -1],
            }

        self.reset(startDay)

    def reset(self, startDay: int = 0) -> None:
        """
        Reset function which is called when the environment is reset

        :param startDay: Starting Day of the simulation
        """
        super().reset(startDay)
        """changing spot qty bid from maxPower to scheduled load"""
        self.fullSpotBid['qty_bid'] = self.scheduledLoad['load']
        # self.hourlySpotBid = self.fullSpotBid[np.isin(self.fullSpotBid['time'].values, self.hours)]
        self.fullFlexBid['qty_bid'] = self.fullSpotBid['qty_bid']
        # self.hourlyFlexBid = self.fullFlexBid[np.isin(self.fullFlexBid['time'].values, self.hours)]

        self.hourlyScheduledLoad = self.scheduledLoad.loc[np.arange(24 * startDay, 24 * (startDay + 1)), 'load'].sum()
        self.hourlyBid = 0.

    def printInfo(self) -> None:
        super().printInfo()
        print("Type: {}".format(self.type))

    def makeSpotBid(self) -> None:
        self.boundSpotBidMultiplier(low=self.lowSpotBidLimit, high=self.highSpotBidLimit)
        self.penalizeTimes = []
        self.spotBidMultiplier = self.getSpotBidMultiplier()

        hourlyTotalLoad = self.scheduledLoad.loc[self.time, 'load']
        hourlyBaseLoad = self.baseLoad.loc[self.time, 'load']

        qty_bid = hourlyBaseLoad + (hourlyTotalLoad - hourlyBaseLoad) * self.spotBidMultiplier

        # Penalize the agent if the spot bid is invalid
        if qty_bid[0] > self.maxPower:
            qty_bid = hourlyBaseLoad + (hourlyTotalLoad - hourlyBaseLoad)
            self.penalizeTimes.append(self.time)

        self.fullSpotBid.loc[self.time, 'qty_bid'] = qty_bid

    def spotMarketEnd(self):
        spotDispatchedTimes, spotDispatchedQty = super().spotMarketEnd()
        self.spotMarketReward(spotDispatchedTimes, spotDispatchedQty)

    def spotMarketReward(self, time, qty) -> None:
        """
        Calculation of the SpotMarket Reward for the DSM Agent

        :param time: Time step
        :param qty: Quantity which the agent bid on the spot market
        """

        # self.hourlyRewardTable = self.fullRewardTable[np.isin(self.fullRewardTable['time'].values, self.hours)]
        # Here negative of qty is used for reward because generation is negative qty
        self.fullRewardTable.loc[time, 'reward_spot'] = self.fullSpotBid.loc[time, 'MCP'] * -qty
        self.fullRewardTable.loc[self.penalizeTimes, 'reward_spot'] += self.penaltyViolation

        # Update the hourly bid
        self.hourlyBid += qty

    def makeFlexBid(self, reqdFlexTimes: np.ndarray) -> None:
        """
        Function that sets the corresponding flex bid multiplier and price multiplier for the required Flex time

        :param reqdFlexTimes: Required flex time
        """

        self.penalizeTimes = []
        self.boundFlexBidMultiplier(low=self.lowFlexBidLimit, high=self.highFlexBidLimit)

        hourlyBaseLoad = self.baseLoad.loc[self.time, 'load']

        # Initialize to the (current spot bid quantities - P_base) the qty that can be shifted downwards
        if self.flexBidMultiplier[0] < 0:
            self.fullFlexBid.loc[self.fullFlexBid['time'].isin(self.hours), 'qty_bid'] = \
                self.fullSpotBid.loc[self.time, 'qty_bid'] - hourlyBaseLoad

        # If fbm > 0: it is essentially the same bid as on the spot market --> it should not only be possible to buy
        # on the spot market, thus the flex bid is initialized to the shiftable range
        else:
            self.fullFlexBid.loc[self.time, 'qty_bid'] = \
                self.scheduledLoad.loc[self.time, 'load'] - hourlyBaseLoad

        super().makeFlexBid(reqdFlexTimes)

        """if the flexbid is greater than (maxPower-spot dispatched qty), its not valid """
        time = self.fullFlexBid.loc[self.time, 'time']
        qty = self.fullFlexBid.loc[self.time, 'qty_bid']

        highLimit = self.maxPower
        if self.fullSpotBid.loc[time, 'dispatched']:
            highLimit = self.maxPower - self.fullSpotBid.loc[time, 'qty_bid']

        if not qty <= highLimit:
            self.fullFlexBid.loc[time, 'qty_bid'] = 0
            self.penalizeTimes.append(self.time)

    def flexMarketEnd(self) -> None:
        flexDispatchedTimes, flexDispatchedQty, flexDispatchedPrice = super().flexMarketEnd()
        self.flexMarketReward(flexDispatchedTimes, flexDispatchedQty, flexDispatchedPrice)

    def flexMarketReward(self, time: List[int], qty: float, price: float) -> None:
        """
        Function to calculate the reward obtained by the agent on the flex market

        :param time: list of flexDispatchedTimes
        :param qty: list of flexDispatchedQty
        :param price: list of the prices for dispatched times
        """

        # Either way, if dispatched, the DSO pays the agents
        self.fullRewardTable.loc[time, 'reward_flex'] = price * qty.abs()
        self.fullRewardTable.loc[self.penalizeTimes, 'reward_flex'] += self.penaltyViolation

        # Update the dailyBid
        if not qty.values:
            pass
        else:
            self.hourlyBid += qty[self.time]

        # If the next hour is in the next day, then we calculate the difference between the dailyBid and the true daily
        # load
        if not int(self.time / 24) == int((self.time + 1) / 24):
            energy_deficit = self.hourlyBid - self.hourlyScheduledLoad
            energy_penalty = abs(energy_deficit) * self.penaltyTotalLoadViolation
            self.fullRewardTable.loc[self.time, 'reward_spot'] += energy_penalty

            # Reset the dailyBid to zero and update the next dailyScheduledLoad
            self.hourlyBid = 0.
            if not int((self.time + 1) / 24) == self.endDay:
                self.hourlyScheduledLoad = self.scheduledLoad.loc[
                    np.arange(24 * int((self.time + 1) / 24), 24 * (int((self.time + 1) / 24) + 1)), 'load'].sum()
            else:
                self.hourlyScheduledLoad = 0.
