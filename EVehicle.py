import numpy as np
import pandas as pd
import os
import ast
from typing import List, Any

from FlexAgent import FlexAgent


class EVehicle(FlexAgent):
    """
    Represents an electric vehicles
    It can provide:
        +ve flexibility: STOP charging
        -ve flexibility: START charging
    """

    def __init__(
            self,
            id,
            location=None,
            maxPower: float = 0.0036,
            marginalCost: float = 0,
            maxCapacity: float = 0,
            efficiency: float = 1.0,
            absenceTimes: pd.Series = None,
            consumption: pd.Series = None,
            chargingSeries: pd.Series = None,
            startDay: int = 0,
            endDay: int = 365
    ):

        super().__init__(
            id=id,
            location=location,
            maxPower=maxPower,
            marginalCost=marginalCost,
            startDay=startDay,
            endDay=endDay
        )

        self.type = "E-vehicle"
        self.maxCapacity = maxCapacity  # capacity in MWh
        self.minCapacity = 0.2 * self.maxCapacity
        self.SOC = 0
        self.maxPower = maxPower

        self.threshold_energetic = 0.5 * self.maxCapacity
        self.threshold_SOC = 0.5

        """in absence times, 1 indicates EV in use and 0 indicates EV in charging station"""
        self.dailyAbsenceTimes = None
        self.consumption = consumption

        # Mod LG because input data is a Series of length EndDay - StartDay containing on each position the values
        # for the 24 hours of its day
        absenceTimes_quarterly = []
        for time in range(self.startDay, self.endDay):
            absenceTimes_quarterly.append(ast.literal_eval(absenceTimes.loc[time]))

        absenceTimes_quarterly = np.array(absenceTimes_quarterly).reshape(24 * (endDay - startDay), -1)
        # An electric vehilce is defined as absent if for half an hour of the hour the vehicle is absent
        absenceTimes = absenceTimes_quarterly.sum(axis=1)
        absenceTimes[absenceTimes < 2] = 0
        absenceTimes[absenceTimes > 1] = 1
        self.absenceTimes = pd.Series(absenceTimes, index=range(24 * startDay, 24 * endDay))
        self.nStart = -1  # Mod LG: set as an attribute in order to count the starts on a full day

        self.chargingSeries = chargingSeries
        self.energyTable = None
        self.dailyConsumption = None
        self.remainingEnergy = None
        self.flexChangedEnergy = 0
        self.spotChangedEnergy = 0
        self.efficiency = efficiency
        """in flex , can "sell" qty ie, to reduce the qty from the spot dispatched amount (eg, to stop/reduce charging)
                    can buy qty to increase the qty from the spot dispatched amount"""
        self.lowFlexBidLimit = -1
        self.highFlexBidLimit = 1

        self.penalizeTimes = []

        self.obs_space_size = 4
        self.action_dict = \
            {
                0: [0, 0],
                1: [0, 1],
                2: [1, 0],
                3: [1, -1],
            }
        self.mean_MCP = 0

        self.nextAbsence = 0
        self.lastAbsenceTime = 0
        self.nextConsumption = 0.
        self.threshold_energetic = 0.5 * self.maxCapacity
        self.threshold_SOC = 0.5
        self.deltaSOCmax = self.maxPower / self.maxCapacity

        self.reset(startDay)

    def reset(self, startDay: int = 0) -> None:
        super().reset(startDay)
        self.remainingEnergy = self.maxCapacity / 2
        self.SOC = self.remainingEnergy / self.maxCapacity
        # self.hourlySpotBid = self.fullSpotBid.loc[self.fullSpotBid['time'].isin(self.hours)]
        self.flexChangedEnergy = 0.
        self.spotChangedEnergy = 0.
        self.energyTable = pd.DataFrame(data={'time': np.arange(self.spotHour + 1),
                                              'after_spot': np.full(self.spotHour + 1, self.remainingEnergy,
                                                                    dtype=float),
                                              'before_flex': np.full(self.spotHour + 1, self.remainingEnergy,
                                                                     dtype=float),
                                              'after_flex': np.full(self.spotHour + 1, self.remainingEnergy,
                                                                    dtype=float),
                                              'after_flex_remarks': np.full(self.spotHour + 1, 'dispatched')})

        self.nextAbsence = self.absenceTimes.ne(0).idxmax()
        self.lastAbsenceTime = self.startDay * 24
        self.nextConsumption = ast.literal_eval(self.consumption.loc[int(self.nextAbsence / 24)])[0]

    def printInfo(self) -> None:
        super().printInfo()
        print("Type: {}\n".format(self.type))

    def canCharge(self, chargePower: float, chargeTime: int, status: str) -> [bool, float]:
        """
        check whether charging is possible to charge. If False, returns by how much amount

        :param chargePower: Power which is to be charged
        :param chargeTime: chargeTime in hrs and chargePower in MW (+ve)
        :param status: whether after spot bids, or when making flex bids, or after flex bids
        :return: Bool whether it is possible to charge, if false returns by how much
        """
        if chargePower > self.maxPower:
            chargePower = self.maxPower

        energy = chargePower * chargeTime * self.efficiency
        if status == 'after_spot':
            if self.remainingEnergy + energy > self.maxCapacity:
                return False, self.remainingEnergy + energy - self.maxCapacity
            else:
                return True, None
        elif status == 'before_flex':
            """checks if in the current hour spot dispatched qty + remaining energy after flex bidding 
            leads to constraints violation or not"""
            if self.remainingEnergy + self.spotChangedEnergy + energy > self.maxCapacity:
                return False, self.remainingEnergy + self.spotChangedEnergy + energy - self.maxCapacity
            else:
                return True, None
        elif status == 'after_flex':
            if self.remainingEnergy + self.flexChangedEnergy > self.maxCapacity:
                return False, self.remainingEnergy + self.flexChangedEnergy - self.maxCapacity
            else:
                return True, None

    def canDischarge(self, dischargePower: float, dischargeTime: int, status: str) -> [bool, float]:
        """
        check whether charging is possible to charge. If False, returns by how much amount

        :param dischargePower: Power which is to be discharged
        :param dischargeTime: dischargeTime in hrs and chargePower in MW (+ve)
        :param status: whether after spot bids, or when making flex bids, or after flex bids
        :return: Bool whether it is possible to charge, if false returns by how much
        """
        if not status == 'before_departure' and dischargePower < -self.maxPower:
            dischargePower = -self.maxPower

        # energy will be negative as dischargePower is negative
        energy = dischargePower * dischargeTime * self.efficiency
        if status == 'after_spot' or status == 'before_departure':
            if self.remainingEnergy + energy < self.minCapacity:
                return False, self.minCapacity - (self.remainingEnergy + energy)
            else:
                return True, None
        elif status == 'before_flex':
            """checks if in the current hour spot dispatched qty + remaining energy after flex bidding 
            leads to constraints violation or not"""
            if self.remainingEnergy + self.spotChangedEnergy + energy < self.minCapacity:
                return False, self.minCapacity - (self.remainingEnergy + self.spotChangedEnergy + energy)
            else:
                return True, None
        elif status == 'after_flex':
            if self.remainingEnergy + self.flexChangedEnergy < self.minCapacity:
                return False, self.minCapacity - (self.remainingEnergy + self.flexChangedEnergy)
            else:
                return True, None

    def changeSOC(self, qty: float, time: int, status: str, index: int) -> None:
        """
        Changes the SOC of the battery

        :param qty: Quantity which is to be changed
        :param time: time of the day
        :param status: if after spot bid, or before flex bid or after flex bid
        :param index: the hour in the day
        """
        energy = qty * time * self.efficiency
        if status == 'after_spot':
            self.remainingEnergy += energy
        elif status == 'before_flex':
            self.remainingEnergy = self.remainingEnergy + self.spotChangedEnergy + energy
        elif status == 'after_flex':
            self.remainingEnergy = self.remainingEnergy + self.flexChangedEnergy
        if not index == self.startDay * 24 + self.spotTimePeriod:
            # Remaining energy can not be lower than zero
            self.remainingEnergy = max(0., self.remainingEnergy)
            self.energyTable.loc[self.energyTable['time'] == index, status] = self.remainingEnergy
        self.SOC = self.remainingEnergy / self.maxCapacity

    def checkSOC(self) -> None:
        if self.remainingEnergy >= self.threshold_energetic:
            return True
        else:
            return False

    def makeSpotBid(self) -> None:
        """
        Function to make the spot bid
        """
        status = 'after_spot'
        self.spotChangedEnergy = 0.
        self.flexChangedEnergy = 0.
        self.boundSpotBidMultiplier(low=self.lowSpotBidLimit, high=self.highSpotBidLimit)
        self.energyTable['time'] = np.concatenate([self.hours, np.array([self.hours[-1] + 1])])

        self.dailyAbsenceTimes = self.absenceTimes[self.time]
        self.dailyConsumption = ast.literal_eval(self.consumption.loc[self.day])

        # Check whether the car is there in the next time
        if not self.time == self.totalHours[-1]:
            if self.nextAbsence <= self.time and self.absenceTimes.loc[self.time + 1] == 0 and self.dailyAbsenceTimes == 1:
                self.lastAbsenceTime = self.time
                self.nextAbsence = self.absenceTimes[np.arange(self.time + 1, self.totalHours[-1])].ne(0).idxmax()
                if int(self.nextAbsence / 24) > int(self.time / 24):
                    index = 0
                elif int(self.nextAbsence / 24) == int(self.time / 24):
                    index = self.nStart + 1
                try:
                    load = ast.literal_eval(self.consumption.loc[int(self.nextAbsence / 24)])[index]
                except:
                    load = ast.literal_eval(self.consumption.loc[int(self.nextAbsence / 24)])[index - 1]
                self.nextConsumption = load

                if self.nextAbsence == self.time:
                    self.nextAbsence = 0
        else:
            self.nextAbsence = 0
            self.nextConsumption = 0

        super().makeSpotBid()
        self.fullSpotBid.loc[[self.time], :] = self.makeBid(self.fullSpotBid.loc[[self.time], :], status)

    def spotMarketEnd(self) -> None:
        """
        Calculates spot market reward
        """

        spotDispatchedTimes, spotDispatchedQty = super().spotMarketEnd()
        self.spotMarketReward(spotDispatchedTimes, spotDispatchedQty)
        """change remaining energy to that of the starting time for this day to use in flex market"""
        self.remainingEnergy = \
            self.energyTable.loc[self.energyTable['time'] == self.hours[0], 'after_spot'].values[0]

    def spotMarketReward(self, time: np.array, qty: float) -> None:
        """
        Calculation of spotmarket reward

        :param time: list of spotDispatchedTimes
        :param qty: list of spotDispatchedQty
        """
        # self.hourlyRewardTable = self.fullRewardTable[np.isin(self.fullRewardTable['time'].values, self.hours)]
        qty = qty if not self.dailyAbsenceTimes else 0.

        self.fullRewardTable.loc[time, 'reward_spot'] = self.fullSpotBid.loc[time, 'MCP'] * -qty
        """Penalize to not charge during absent times or charge goes below 0.2 when absent"""
        self.fullRewardTable.loc[self.penalizeTimes, 'reward_spot'] += self.penaltyViolation
        # self.fullRewardTable.loc[self.fullRewardTable['time'].isin(self.hours), 'reward_spot'] \
        #     = self.hourlyRewardTable['reward_spot']

        self.penalizeTimes = []

    def makeFlexBid(self, reqdFlexTimes: np.ndarray) -> None:
        """
        Function to make the flex bid

        :param reqdFlexTimes: List of reqdFlexTimes
        """
        status = 'before_flex'
        self.boundFlexBidMultiplier(low=self.lowFlexBidLimit, high=self.highFlexBidLimit)
        # Copy spot bids so that the flexbid multiplier is applied on this instead of maxPower but only in case
        # the already bought energy should be reduced. If the spot bid was to buy but the reservoir is not full, then
        # the agent can also just buy on the flex market, and there it can buy with maximum power

        if self.flexBidMultiplier[0] < 0:
            self.fullFlexBid.loc[self.fullFlexBid['time'].isin(self.hours), 'qty_bid'] = \
                self.fullSpotBid.loc[self.time, 'qty_bid']
        super().makeFlexBid(reqdFlexTimes)
        self.fullFlexBid.loc[[self.time], :] = self.makeBid(self.fullFlexBid.loc[[self.time], :], status)

    def flexMarketEnd(self) -> None:
        """
         Function to compute the flex market reward for the computed flex dispatched time, quantity and price
        """
        flexDispatchedTimes, flexDispatchedQty, flexDispatchedPrice = super().flexMarketEnd()
        """change remaining energy to that of the starting time for this day to update after_flex energy table"""
        self.remainingEnergy = \
            self.energyTable.loc[self.energyTable['time'] == self.hours[0], 'after_spot'].values[0]

        time = self.time
        qty = self.fullFlexBid.loc[self.time, 'qty_bid']
        dispatched = self.fullFlexBid.loc[self.time, 'dispatched']

        """amount of energy changed in the spot and flex dispatch used to update the energy table for after_flex 
                    times """
        self.flexChangedEnergy = \
            self.energyTable.loc[self.energyTable['time'] == time + 1, 'before_flex'].values[0] - \
            self.energyTable.loc[self.energyTable['time'] == time, 'before_flex'].values[0]
        self.spotChangedEnergy = \
            self.energyTable.loc[self.energyTable['time'] == time + 1, 'after_spot'].values[0] - \
            self.energyTable.loc[self.energyTable['time'] == time, 'after_spot'].values[0]

        """Mod LG: added self.startDay*24 to self.spotTimePeriod, with this the if constraint is not true in case
                       time + 1 is equal the last time + 1 (because python starts counting at zero)"""
        if not time + 1 == self.startDay * 24 + self.spotTimePeriod:
            if dispatched:
                if qty >= 0:
                    possible, _ = self.canCharge(chargePower=qty, chargeTime=self.spotTimeInterval,
                                                 status='after_flex')
                    if possible and self.flexChangedEnergy <= self.maxPower:
                        self.changeSOC(qty, self.spotTimeInterval, 'after_flex', time + 1)
                    else:
                        """in changeSOC, the remaining energy is added with flexChangedEnergy"""
                        self.flexChangedEnergy = 0
                        self.changeSOC(0, self.spotTimeInterval, 'after_flex', time + 1)
                        self.penalizeTimes.append(time)
                        if not time + 1 == self.startDay * 24 + self.spotTimePeriod:
                            self.energyTable.loc[
                                self.energyTable['time'] == time + 1, 'after_flex_remarks'] = 'cant charge'
                else:
                    possible, _ = self.canDischarge(dischargePower=qty, dischargeTime=self.spotTimeInterval,
                                                    status='after_flex')
                    if possible and self.flexChangedEnergy >= -self.maxPower:
                        self.changeSOC(qty, self.spotTimeInterval, 'after_flex', time + 1)
                    else:
                        self.flexChangedEnergy = 0
                        self.changeSOC(0, self.spotTimeInterval, 'after_flex', time + 1)
                        self.penalizeTimes.append(time)
                        if not time + 1 == self.startDay * 24 + self.spotTimePeriod:
                            self.energyTable.loc[
                                self.energyTable['time'] == time + 1, 'after_flex_remarks'] = 'cant discharge'
            else:
                self.remainingEnergy = self.remainingEnergy + self.spotChangedEnergy
                if not time + 1 == self.startDay * 24 + self.spotTimePeriod:
                    self.energyTable.loc[self.energyTable['time'] == time + 1, 'after_flex'] = self.remainingEnergy
                    self.energyTable.loc[
                        self.energyTable['time'] == time + 1, 'after_flex_remarks'] = 'not dispatched'

        """After flex market ends for a day, the final energy must be updated for all columns in energy table as it is 
                the final realised energy after the day"""
        nextTime = (self.time + 1) * self.spotHour
        self.energyTable.loc[self.energyTable['time'] == nextTime, ['after_spot', 'before_flex']] = \
            self.energyTable.loc[self.energyTable['time'] == nextTime, 'after_flex']
        self.remainingEnergy = self.energyTable.loc[self.energyTable['time'] == nextTime, 'after_flex'].values[0]
        self.SOC = self.remainingEnergy / self.maxCapacity
        """keep the energy at the end of last day as the starting energy"""
        self.energyTable.loc[0, ['after_spot', 'before_flex', 'after_flex']] = self.remainingEnergy
        self.flexMarketReward(flexDispatchedTimes, flexDispatchedQty, flexDispatchedPrice)

    def flexMarketReward(self, time: pd.Series, qty: pd.Series, price: pd.Series) -> None:
        """
        Computes the flex market rewards
        :param time: list of flexDispatchedTimes
        :param qty: Spot Bid
        :param price: list of the prices for dispatched times
        """
        # Either way, if dispatched, the DSO pays the agents
        self.fullRewardTable.loc[time, 'reward_flex'] = price * qty.abs()
        self.fullRewardTable.loc[self.penalizeTimes, 'reward_flex'] += self.penaltyViolation

    def starting(self, qty: float, status: str, time: int):
        """
        Checks whether the EV starts at this time stamp. If yes, then the SOC is checked and a penalty is
        given in case the SOC is below a certain threshold. In all other cases it is checked whether the bid
        quantity is zero thus the agent does not bid when the car is away

        :param qty: DispatchedQty
        :param status: whether after spot bids, or when making flex bids, or after flex bids
        :param time: DispatchedTime
        :return: penalizeTimes: list of times to penalize the agent, startingPenalty: the penalty for a single
        violation of constraint
        """
        if not time + 1 == self.spotTimePeriod:
            self.energyTable.loc[self.energyTable['time'] == time + 1, 'after_flex_remarks'] = 'Absent'
        """to check if the EV is starting"""
        # To be able to check that, the car has to be not absent at the timestep or a new day starting
        if (not self.absenceTimes[self.time - 1] or self.time % 24 == 0) and status == 'after_spot':
            """EV is starting now"""
            self.nStart += 1
            """check for sufficient charge in EV and if the discharge leads to a SOC < 20% which is also not wanted"""
            if not self.checkSOC() or \
                    not self.canDischarge(dischargePower=-self.dailyConsumption[self.nStart],
                                          dischargeTime=self.spotTimeInterval, status='before_departure')[0]:
                self.penalizeTimes.append(self.time)
            """the consumption during entire trip"""
            if self.nStart < len(self.dailyConsumption):
                """data has sometimes less consumption values than the number of starts"""
                self.changeSOC(-self.dailyConsumption[self.nStart], self.spotTimeInterval, status, time + 1)
                if not time + 1 == self.spotTimePeriod:
                    self.energyTable.loc[self.energyTable['time'] == time + 1, 'after_flex_remarks'] = 'consumption'
            # else:
            #    self.changeSOC(0, self.spotTimeInterval, status, time + 1)
        # else:
        # self.changeSOC(0, self.spotTimeInterval, status, time + 1)
        """Ev not connected to charging station, penalize to not charge during this time"""
        if not qty == 0:
            # TODO check if this or just making the bid as 0 works well
            if not self.penalizeTimes:
                self.penalizeTimes.append(self.time)

    def makeBid(self, hourlyBid: pd.DataFrame, status: str) -> pd.DataFrame:
        """
        Function to make the bid, the dailyBid DataFrame is checked whether it is possible to make the bids

        :param hourlyBid: Pandas Dataframe which is about to be checked
        :param status: whether it is after spot or before/after flex
        :return dailyBid: Pandas Dataframe with the daily bid which was checked whether a bid is possible
        """
        self.penalizeTimes = []

        time = hourlyBid['time'].values[0]
        qty = hourlyBid['qty_bid'].values[0]

        if status == 'before_flex':
            # amount of energy changed in the spot dispatch used to update the energy table for before_flex times
            self.spotChangedEnergy = \
                self.energyTable.loc[self.energyTable['time'] == time + 1, 'after_spot'].values[0] - \
                self.energyTable.loc[self.energyTable['time'] == time, 'after_spot'].values[0]

        # Check whether the car is absent, if yes and this is the first absent time and SOC < threshold -> Penalty
        # If it is not the first absent time but the agent bids -> penalty as well
        # The penalties are given by append the time to self.penalizeTimes which is then used in the reward tables
        if self.dailyAbsenceTimes:
            self.starting(qty, status, self.time)
            hourlyBid.loc[time, 'qty_bid'] = 0
        else:
            if qty <= 0:
                # power output to grid - generator
                # constraintAmount is positive
                possible, _ = self.canDischarge(dischargePower=qty, dischargeTime=self.spotTimeInterval,
                                                status=status)
                if possible and self.spotChangedEnergy + qty >= -self.maxPower:
                    self.changeSOC(qty, self.spotTimeInterval, status, time + 1)
                else:
                    # If discharging by a certain amount is not possible, change the bid to 0 and penalize the agent
                    hourlyBid.loc[time, 'qty_bid'] = 0
                    self.changeSOC(0, self.spotTimeInterval, status, time + 1)
                    if not self.penalizeTimes:
                        self.penalizeTimes.append(time)

            elif qty > 0:
                # power intake from grid - consumer
                # constraintAmount is positive
                possible, _ = self.canCharge(chargePower=qty, chargeTime=self.spotTimeInterval, status=status)
                if possible and self.spotChangedEnergy + qty <= self.maxPower:
                    self.changeSOC(qty, self.spotTimeInterval, status, time + 1)
                else:
                    # If charging by a certain amount is not possible, change the bid to 0 and penalize the agent
                    hourlyBid.loc[time, 'qty_bid'] = 0
                    self.changeSOC(0, self.spotTimeInterval, status, time + 1)
                    if not self.penalizeTimes:
                        self.penalizeTimes.append(time)

        assert -self.maxPower <= hourlyBid.loc[time, 'qty_bid'] <= self.maxPower, 'Qty bid cannot be more than maxPower'
        return hourlyBid