import numpy as np
import pandas as pd
from FlexAgent import FlexAgent


class REAgent(FlexAgent):
    """
    Renewable energy agent Class

    :param id: ID of agent
    :param location: Location of the battery agent
    :param voltageLevel: Voltage level of the agent - unused
    :param maxPower: Maximum power of the initialized REA
    :param minPower: Minimum power of the initialized REA
    :param marginalCost: Marginal cost
    :param feedInPremium: Feed in premium for renewables
    :param genSeries: Generator series containing a time series for the generation
    :param startDay: Starting Day of the simulation
    :param endDay: Ending Day of the simulation
    """

    def __init__(
            self,
            id,
            location=None,
            voltageLevel=None,
            minPower: float = 0.,
            maxPower: float = 0.,
            marginalCost: float = 0.,
            feedInPremium: float = 0.,
            genSeries: pd.Series = None,
            startDay: int = 0,
            endDay: int = 365
    ):
        super().__init__(
            id=id,
            location=location,
            minPower=minPower,
            maxPower=maxPower,
            marginalCost=marginalCost,
            startDay=startDay,
            endDay=endDay
        )

        self.feedInPremium = feedInPremium

        # Assumption: the timeseries data contains -ve qty (generation)
        self.genSeries = genSeries

        self.maxPower = max(self.maxPower, genSeries.abs().max())
        self.penalizeTimes = []

        if self.maxPower > 1:
            self.penaltyViolation *= 50

        self.lowFlexBidLimit = -1
        self.highFlexBidLimit = 1

        self.obs_space_size = 2

        self.action_dict = \
            {
                0: [0, 0, 1],
                1: [0, 0, 2],
                2: [0, 1, 1],
                3: [0, 1, 2],
                4: [1, 0, 1],
                5: [1, 0, 2],
                6: [1, -1, 1],
                7: [1, -1, 2],
            }

        self.reset(startDay)

    def reset(self, startDay=0):
        """
        Reset function

        :param startDay: Starting day of the simulation
        """

        super().reset(startDay)
        self.fullSpotBid['qty_bid'] = self.genSeries
        self.fullFlexBid['qty_bid'] = self.genSeries
        self.fullFlexBid['price'] = np.full(self.flexTimePeriod, self.feedInPremium, dtype=float)

        self.penalizeTimes = []

    def printInfo(self) -> None:
        """
        Print info function -> unused
        """
        super().printInfo()
        print("Type: {}".format(self.type))

    def makeSpotBid(self) -> None:
        """
        Function to make the spot bid based on the generation time series
        """
        self.boundSpotBidMultiplier(low=self.lowSpotBidLimit, high=self.highSpotBidLimit)
        super().makeSpotBid()

    def spotMarketEnd(self) -> None:
        """
        Function to end the spot market and to call the spot market reward function
        """
        super().spotMarketEnd()
        self.spotMarketReward()

    def spotMarketReward(self) -> None:
        """
        Renewables receive Feed-in Premium from the market
        Fixed  FIP is implemented here
        """
        self.penalizeTimes = []
        # Here negative of qty is used for reward because generation is negative qty
        self.fullRewardTable.loc[self.time, 'reward_spot'] = \
            (self.fullSpotBid.loc[self.time, 'MCP'] + self.feedInPremium) * -self.fullSpotBid.loc[self.time, 'qty_bid']

    def makeFlexBid(self, reqdFlexTimes: np.ndarray) -> None:
        """
        Function to make the flex bid based on the spot bid as generating units can only reduce what they bid

        :param reqdFlexTimes: Numpy array with one integer in it when flex is required
        """
        self.penalizeTimes = []
        self.boundFlexBidMultiplier(low=self.lowFlexBidLimit, high=self.highFlexBidLimit)

        # In case the flexbidmultiplier is negative, the agent wants to reduce its already dispatched spot quantity
        # If the fbm is positive, the agent wants to increase its output, thus the fbm is used with the
        # generationSeries. This could be the case when the agent wants to provoke a load-driven congestion by
        # not bidding on the spot market and then selling its generation to a higher price on the flex market.

        if self.flexBidMultiplier[0] <= 0:
            self.fullFlexBid.loc[self.time, 'qty_bid'] = \
                self.fullSpotBid.loc[self.time, 'qty_bid']
        else:
            self.fullFlexBid.loc[self.time, 'qty_bid'] = self.genSeries[self.time]

        super().makeFlexBid(reqdFlexTimes)

        # Check if the bid is valid. It is invalid if sbm + fbm > 1, and quantity > 0
        if self.fullSpotBid.loc[self.time, 'qty_bid'] + self.fullFlexBid.loc[self.time, 'qty_bid'] < \
                self.genSeries[self.time]:
            self.penalizeTimes.append(self.time)
            self.fullFlexBid.loc[self.time, 'qty_bid'] = 0.

    def flexMarketEnd(self) -> None:
        """
        Function to end the flex market and then call the reward function
        """
        flexDispatchedTimes, flexDispatchedQty, flexDispatchedPrice = super().flexMarketEnd()
        self.flexMarketReward(flexDispatchedTimes, flexDispatchedQty, flexDispatchedPrice)

    def flexMarketReward(self, time: np.ndarray, qty: np.ndarray, price: np.ndarray) -> None:
        """
        Function to compute the flex market reward

        :param time: list of flexDispatchedTimes
        :param qty: list of flexDispatchedQty
        :param price: list of the prices for dispatched times
        """

        self.fullRewardTable.loc[time, 'reward_flex'] = price * abs(qty)
        self.fullRewardTable.loc[self.penalizeTimes, 'reward_flex'] += self.penaltyViolation
