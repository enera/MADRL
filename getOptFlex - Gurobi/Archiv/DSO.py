from FlexMarket import FlexMarket
from Grid import Grid
import pandas as pd
import numpy as np
import re
import util
import time as zeit
import xarray as xr
import gurobipy as gp
from gurobipy import GRB

class DSO:
    def __init__(self, grid, start, end, dailySpotTime=24):
        self.flexAgents = []
        self.flexMarket = FlexMarket()
        self.grid = grid
        self.start =start
        self.end = end
        self.dailySpotTime = dailySpotTime
        self.totalTimes = np.arange(self.start * self.dailySpotTime, self.end * self.dailySpotTime)
        self.numTopFivePercent = round(len(self.grid.data) * .05)
        self.numTopTwoAndHalfPercent = round(len(self.grid.data) * .025)
        self.congestionDetails = pd.DataFrame(data={'time': self.totalTimes,
                                                    'congested': np.full(len(self.totalTimes), 0),
                                                    'previous congested': np.full(len(self.totalTimes), 0),
                                                    'top 5 percent loading': np.full(len(self.totalTimes), 0.0),
                                                    'previous top 5 percent loading': np.full(len(self.totalTimes), 0.0),
                                                    'top 2.5 percent loading': np.full(len(self.totalTimes), 0.0),
                                                    'previous top 2.5 percent loading': np.full(len(self.totalTimes), 0.0)},
                                         index=self.totalTimes)

    def reset(self):
        self.flexMarket.reset(self.start)
        self.grid.reset(self.start)
        self.congestionDetails.loc[:, 'previous congested'] = self.congestionDetails.loc[:, 'congested']
        self.congestionDetails.loc[:, 'previous top 5 percent loading'] = self.congestionDetails.loc[:, 'top 5 percent loading']
        self.congestionDetails.loc[:, 'previous top 2.5 percent loading'] = self.congestionDetails.loc[:, 'top 2.5 percent loading']
        self.congestionDetails.loc[:, 'congested'] = 0
        self.congestionDetails.loc[:, 'top 5 percent loading'] = 0.0
        self.congestionDetails.loc[:, 'top 2.5 percent loading'] = 0.0

    def checkCongestion(self, spotBidDF):
        status = self.grid.isCongested(spotBidDF)
        return status

    def addflexAgents(self, agents):
        # agents is a list of FlexAgents
        self.flexAgents.extend(agents)
        self.grid.flexAgents.extend(agents)
        self.grid.importLoadsAndSensi()
        self.flexMarket.addParticipants(self.flexAgents)

    def askFlexibility(self):
        for agent in self.flexAgents:
            agent.spotState = False
        self.flexMarket.collectBids(self.grid.reqdFlexTimes)

    def GurobiModel(self, reqdFlexTimes, bids_vec, reqdFlexCurrent, sensitivityFlexAgents_vec):

        slack_penalty = np.ones(reqdFlexCurrent.shape) * (bids_vec.loc[dict(qty_price_accepted = "price")].sum(axis=1).sum(axis=0) * 100).values
        model = gp.Model()

        # Mute the optimization function for increased speed
        model.setParam('OutputFlag', 0)

        # Set Matrix Variables
        agent_bids = model.addMVar((len(self.flexAgents), len(reqdFlexTimes)), vtype=GRB.CONTINUOUS, name="agent_bids")
        slacks = model.addMVar((len(reqdFlexCurrent.columns), len(reqdFlexTimes)), vtype=GRB.CONTINUOUS, name="slacks")

        # Set Constraints
        model.addConstrs((agent_bids[:, i] <= bids_vec.loc[dict(qty_price_accepted = "qty_bid")].values[i, :]
                          for i in range(len(reqdFlexTimes))), name='MaxBidConstraint')

        model.addConstrs((sensitivityFlexAgents_vec.loc[dict(Times=reqdFlexTimes[i])].transpose().values @ agent_bids[:, i] + slacks[:, i] >=
                          reqdFlexCurrent.loc[reqdFlexTimes[i], :].values for i in range(len(reqdFlexTimes))), name='Bid')
        model.addConstrs((slacks[:, i] >= 0 for i in range(len(reqdFlexTimes))), name='Slack')

        # Set the objective function
        model.setObjective(sum(agent_bids[:, i] @ bids_vec.loc[dict(qty_price_accepted = "price")].loc[reqdFlexTimes[i], :].values +
                               slacks[:, i] @ slack_penalty[i, :] for i in range(len(reqdFlexTimes))), GRB.MINIMIZE)

        return model

    def optFlex(self):
        """choose the optimum flexibility wrt cost, qty and sensitivities"""
        nBids = len(self.flexMarket.bids)
        if len(self.grid.reqdFlexTimes) > 0:

            bids_vec, currentSensitivity_vec, reqdFlexCurrent, congested_vec, sensitivityFlexAgents_vec  = \
                self.getOptFlexData(nBids)
            sensitivityFlexAgents_vec, bids_vec, = \
                self.buildSensiandBid(bids_vec, currentSensitivity_vec, congested_vec, sensitivityFlexAgents_vec)

        # Correction of the sensi matrix: Sensitivities can not be zero, hence we can take the absolute of the matrix
        sensitivityFlexAgents_vec = np.absolute(sensitivityFlexAgents_vec)

        # For the optimization problem the qty bid should be positive, hence we swap the sign of the sensitivity in
        # case a the bid quantity is negative. Therefore it is guaranteed that for a positive reqd current the
        # agent will be picked and the optimization problem remains feasible
        # Use np.broadcast_to since the read only view guarnatees fast computation

        bid_sign = bids_vec.loc[dict(qty_price_accepted="qty_bid")].values < 0
        sensi_bid_sign = np.broadcast_to(bid_sign[..., None], bid_sign.shape + (len(reqdFlexCurrent.columns),))

        # Furthermore we need to make sure that the optimization problem is feasible even if the reqd current
        # is negative or has negative entries. For this we also change the sign of the sensitivities for
        # the specific line(s) in the specific hour and take the absolute value of the current before it enters the
        # optimization model function

        current_sign = reqdFlexCurrent.values < 0
        sensi_current_sign = np.broadcast_to(current_sign[:, None, :], (current_sign.shape[0], nBids, current_sign.shape[-1]))

        # Take both boolean multidimensional arrays bid sign and current sign and apply them to the sensitivity DataArray
        # Herefore we use the XOR operation which makes sure that the sign is only changed on the position where either
        # the current sign or the bid sign is negaitve.

        sensi_sign = sensi_bid_sign != sensi_current_sign
        sensitivityFlexAgents_vec.values[sensi_sign] = -sensitivityFlexAgents_vec.values[sensi_sign]

        bids_vec.loc[dict(qty_price_accepted="qty_bid")] = np.absolute(bids_vec.loc[dict(qty_price_accepted="qty_bid")])
        optModel = self.GurobiModel(self.grid.reqdFlexTimes, bids_vec, reqdFlexCurrent, sensitivityFlexAgents_vec)

        # Solve model, display the optimization problem and print out the dispatch for the agents
        optModel.optimize()
        #optModel.display()

        # Get the accepted bids and their quantity from Gurobi
        lAcceptedQty = []
        for i, v in enumerate(optModel.getVars()):
            if i == len(self.flexAgents)*len(self.grid.reqdFlexTimes):
                break
            lAcceptedQty.append(v.x)

        # Correct the sign in the end, by multiplying with -1 everywhere where save_sign is true and 1 where it was false
        acceptedQty = \
            pd.DataFrame(np.array(lAcceptedQty).reshape(len(self.flexAgents), -1).transpose()*(-1*bid_sign + 1*(~bid_sign)),
                                   index = self.grid.reqdFlexTimes, columns = [agent.id for agent in self.flexAgents])

        self.flexMarket.sendDispatch(acceptedQty)

    def getOptFlexData(self, nBids):
        bids_vec = xr.DataArray(
            np.full((len(self.grid.reqdFlexTimes), nBids, 3), 0.0),
            coords={
                "Times": self.grid.reqdFlexTimes,
                "Agent_Names": self.grid.agentsList,
                "qty_price_accepted": ['qty_bid', 'price', 'accepted']
            },
            dims = ["Times", "Agent_Names", "qty_price_accepted"]
        )

        currentSensitivity_vec = self.grid.getSensitivity()
        """congested lines/ nodes at this particular time"""
        congested_vec = self.grid.congestionStatus[self.grid.congestionStatus.loc[self.grid.reqdFlexTimes, :]].loc[self.grid.reqdFlexTimes, :].dropna(axis = 1, how = 'all')

        # LG: commented out for the moment
        """
        self.congestionDetails.loc[time, 'congested'] = len(congested)
        loadingTopFivePercent = (self.grid.loading.loc[time, :].abs()*100/
                                 self.grid.data.loc[:, 'I_rated_A']).sort_values(ascending=False)[:self.numTopFivePercent].mean()
        loadingTopTwoAndHalfPercent = (self.grid.loading.loc[time, :].abs()*100/
                                 self.grid.data.loc[:, 'I_rated_A']).sort_values(ascending=False)[:self.numTopTwoAndHalfPercent].mean()
        self.congestionDetails.loc[time, 'top 5 percent loading'] = loadingTopFivePercent
        self.congestionDetails.loc[time, 'top 2.5 percent loading'] = loadingTopTwoAndHalfPercent
        """

        reqdFlexI_A_vec = self.grid.loading.loc[self.grid.reqdFlexTimes, congested_vec.columns].multiply(congested_vec.values).abs() - \
                          self.grid.data.loc[congested_vec.columns, 'I_rated_A'].values

        # Comment LG: In the vector reqdFlex Current you'll find nan values at the places where a line is congested
        # within the timeframe of reqdFlexTimes but not in the specific hour. We can just set the reqdFlexCurrent
        # to zero at this time

        reqdFlexI_A_vec = reqdFlexI_A_vec.fillna(value = 0)
        congested_vec = reqdFlexI_A_vec.columns

        """stores the sensi of the agents to the congested lines"""
        sensitivityFlexAgents_vec = xr.DataArray(
            np.full((len(self.grid.reqdFlexTimes), nBids, len(congested_vec)), 0.0),
            coords={
                "Times": self.grid.reqdFlexTimes,
                "Agent_Names": self.grid.agentsList,
                "Line_Names": reqdFlexI_A_vec.columns
            },
            dims = ["Times", "Agent_Names", "Line_Names"]
        )
        return bids_vec, currentSensitivity_vec, reqdFlexI_A_vec, congested_vec, sensitivityFlexAgents_vec

    def buildSensiandBid(self, bids_vec, currentSensitivity_vec, congested_vec, sensitivityFlexAgents_vec):
        for i, (agentID, flexbid) in enumerate(self.flexMarket.bids.items()):
            agentNode = 'Standort_' + re.search("k(\d+)[n,d,l]", agentID).group(1)
            """get the sensitivity of this agent on the congested lines"""
            sensitivity_vec = currentSensitivity_vec.loc[congested_vec, self.grid.nodeSensitivityDict[agentNode]].values
            sensitivity_vec = np.reshape(sensitivity_vec, (-1, len(self.grid.reqdFlexTimes)))
            # Sensitivity vec has on its columns the time and the rows are the congested lines
            sensitivityFlexAgents_vec.loc[dict(Agent_Names=agentID)] = sensitivity_vec.transpose()
            bids_vec.loc[dict(Agent_Names=agentID, qty_price_accepted=['qty_bid', 'price'])] = flexbid.loc[self.grid.reqdFlexTimes, ['qty_bid', 'price']]
        return sensitivityFlexAgents_vec, bids_vec

    def endDay(self):
        for agent in self.flexAgents:
            agent.spotState = True
        self.flexMarket.endDay()
        self.grid.endDay()
