from FlexMarket import FlexMarket
from Grid import Grid
import pandas as pd
import numpy as np
import re
import util
import time as zeit
import xarray as xr
import gurobipy as gp
from gurobipy import GRB

class DSO:
    def __init__(self, grid, start, end, dailySpotTime=24):
        self.flexAgents = []
        self.flexMarket = FlexMarket()
        self.grid = grid
        self.start =start
        self.end = end
        self.dailySpotTime = dailySpotTime
        self.totalTimes = np.arange(self.start * self.dailySpotTime, self.end * self.dailySpotTime)
        self.numTopFivePercent = round(len(self.grid.data) * .05)
        self.numTopTwoAndHalfPercent = round(len(self.grid.data) * .025)
        self.congestionDetails = pd.DataFrame(data={'time': self.totalTimes,
                                                    'congested': np.full(len(self.totalTimes), 0),
                                                    'previous congested': np.full(len(self.totalTimes), 0),
                                                    'top 5 percent loading': np.full(len(self.totalTimes), 0.0),
                                                    'previous top 5 percent loading': np.full(len(self.totalTimes), 0.0),
                                                    'top 2.5 percent loading': np.full(len(self.totalTimes), 0.0),
                                                    'previous top 2.5 percent loading': np.full(len(self.totalTimes), 0.0)},
                                         index=self.totalTimes)

    def reset(self):
        self.flexMarket.reset(self.start)
        self.grid.reset(self.start)
        self.congestionDetails.loc[:, 'previous congested'] = self.congestionDetails.loc[:, 'congested']
        self.congestionDetails.loc[:, 'previous top 5 percent loading'] = self.congestionDetails.loc[:, 'top 5 percent loading']
        self.congestionDetails.loc[:, 'previous top 2.5 percent loading'] = self.congestionDetails.loc[:, 'top 2.5 percent loading']
        self.congestionDetails.loc[:, 'congested'] = 0
        self.congestionDetails.loc[:, 'top 5 percent loading'] = 0.0
        self.congestionDetails.loc[:, 'top 2.5 percent loading'] = 0.0

    def checkCongestion(self, spotBidDF):
        status = self.grid.isCongested(spotBidDF)
        return status

    def addflexAgents(self, agents):
        # agents is a list of FlexAgents
        self.flexAgents.extend(agents)
        self.grid.flexAgents.extend(agents)
        self.grid.importLoadsAndSensi()
        self.flexMarket.addParticipants(self.flexAgents)

    def askFlexibility(self):
        for agent in self.flexAgents:
            agent.spotState = False
        self.flexMarket.collectBids(self.grid.reqdFlexTimes)

    def GurobiModel(self, reqdFlexTimes, bids_vec, reqdFlexCurrent, sensitivityFlexAgents_vec):

        # Get the minimum and maximum bids while distinguishing between positive and negative values
        qty_bid = bids_vec.loc[dict(qty_price_accepted = "qty_bid")].values
        bids_greater_zero = qty_bid > 0
        max_bid = qty_bid*bids_greater_zero
        min_bid = qty_bid*~bids_greater_zero

        slack_penalty = np.ones(reqdFlexCurrent.shape) * (bids_vec.loc[dict(qty_price_accepted = "price")].sum(axis=1).sum(axis=0) * 100).values
        model = gp.Model()

        # Mute the optimization function for increased speed
        # model.setParam('OutputFlag', 0)

        # Set Matrix Variables
        agent_bids = model.addMVar((len(self.flexAgents), len(reqdFlexTimes)), vtype=GRB.CONTINUOUS, name="agent_bids")
        slacks = model.addMVar((len(reqdFlexCurrent.columns), len(reqdFlexTimes)), vtype=GRB.CONTINUOUS, name="slacks")

        # Set Constraints
        model.addConstrs((agent_bids[:, i] <= max_bid[i, :] for i in range(len(reqdFlexTimes))), name='MaxBidConstraint')
        model.addConstrs((min_bid[i, :] <= agent_bids[:, i]  for i in range(len(reqdFlexTimes))), name='MinBidConstraint')

        model.addConstrs((sensitivityFlexAgents_vec.loc[dict(Times=reqdFlexTimes[i])].transpose().values @ agent_bids[:, i] - slacks[:, i] <=
                          - reqdFlexCurrent.loc[reqdFlexTimes[i], :].values for i in range(len(reqdFlexTimes))), name='Bid')

        model.addConstrs((slacks[:, i] >= 0 for i in range(len(reqdFlexTimes))), name='Slack')

        # Set the objective function
        model.setObjective(sum(agent_bids[:, i] @ bids_vec.loc[dict(qty_price_accepted = "price")].loc[reqdFlexTimes[i], :].values +
                               slacks[:, i] @ slack_penalty[i, :] for i in range(len(reqdFlexTimes))), GRB.MINIMIZE)

        return model

    def optFlex(self):
        """choose the optimum flexibility wrt cost, qty and sensitivities"""
        nBids = len(self.flexMarket.bids)
        minSensi = 1e-03
        acceptedQty = pd.DataFrame(np.full((len(self.grid.reqdFlexTimes), len(self.flexAgents)), 0.0),
                                   index = self.grid.reqdFlexTimes, columns = [agent.id for agent in self.flexAgents])
        self.grid.reqdFlexTimes = np.array([self.grid.reqdFlexTimes[0]])
        if len(self.grid.reqdFlexTimes) > 0:
            sensitivityFlexAgents_vec, bids_vec, reqdFlexCurrent  = self.getOptFlexData(nBids, minSensi)

            optModel = self.GurobiModel(self.grid.reqdFlexTimes, bids_vec, reqdFlexCurrent, sensitivityFlexAgents_vec)

            # Solve model, display the optimization problem and print out the dispatch for the agents
            optModel.optimize()
            optModel.display()

            # Get the accepted bids and their quantity from Gurobi
            lAcceptedQty = []
            for i, v in enumerate(optModel.getVars()):
                if i == len(self.flexAgents)*len(self.grid.reqdFlexTimes):
                    break
                lAcceptedQty.append(v.x)

            # Correct the sign in the end, by multiplying with -1 everywhere where save_sign is true and 1 where it was false
            acceptedQty = pd.DataFrame(np.array(lAcceptedQty).reshape(len(self.flexAgents), -1).transpose())

        self.flexMarket.sendDispatch(acceptedQty)

    def getOptFlexData(self, nBids, minSensi):

        # Get all lines and transformers
        lines_and_trafos = self.grid.loading.columns

        # Look at all lines which are overloaded. These will have a different sign than the lines which are not yet congested
        higher_max_loading = self.grid.loading.loc[self.grid.reqdFlexTimes, :].abs() > (self.grid.data.loc[:, 'I_rated_A'].values)

        # Get the required Flex current for all lines. Here we distinguish between positive and negative current in a way
        # that a positive flex current suggests a congestion and a negative current shows the remaining current a line
        # can handle

        reqdFlexI_A_vec = self.grid.loading.loc[self.grid.reqdFlexTimes, :].abs() - (self.grid.data.loc[:, 'I_rated_A'].values)

        bids_vec = xr.DataArray(
            np.full((len(self.grid.reqdFlexTimes), nBids, 3), 0.0),
            coords={
                "Times": self.grid.reqdFlexTimes,
                "Agent_Names": self.grid.agentsList,
                "qty_price_accepted": ['qty_bid', 'price', 'accepted']
            },
            dims = ["Times", "Agent_Names", "qty_price_accepted"]
        )

        currentSensitivity_vec = self.grid.getSensitivity()

        # stores the sensi of the agents to all lines
        sensitivityFlexAgents_vec = xr.DataArray(
            np.full((len(self.grid.reqdFlexTimes), nBids, len(lines_and_trafos)), 0.0),
            coords={
                "Times": self.grid.reqdFlexTimes,
                "Agent_Names": self.grid.agentsList,
                "Line_Names": lines_and_trafos
            },
            dims = ["Times", "Agent_Names", "Line_Names"]
        )

        for i, (agentID, flexbid) in enumerate(self.flexMarket.bids.items()):
            agentNode = 'Standort_' + re.search("k(\d+)[n,d,l]", agentID).group(1)

            # get the sensitivity of this agent on all lines
            sensitivity_vec = currentSensitivity_vec.loc[lines_and_trafos, self.grid.nodeSensitivityDict[agentNode]].values

            # Set all values in the sensitivity matrix to zero which are smaller than the input margin
            sensitivity_vec[np.absolute(sensitivity_vec) < minSensi] = 0
            sensitivity_vec = np.reshape(sensitivity_vec, (-1, len(self.grid.reqdFlexTimes)))

            # Sensitivity vec has on its columns the time and the rows are all lines
            sensitivityFlexAgents_vec.loc[dict(Agent_Names=agentID)] = sensitivity_vec.transpose()
            bids_vec.loc[dict(Agent_Names=agentID, qty_price_accepted=['qty_bid', 'price'])] = flexbid.loc[self.grid.reqdFlexTimes, ['qty_bid', 'price']]

        return sensitivityFlexAgents_vec, bids_vec, reqdFlexI_A_vec

    def endDay(self):
        for agent in self.flexAgents:
            agent.spotState = True
        self.flexMarket.endDay()
        self.grid.endDay()
