import pandas as pd
import numpy as np
import gurobipy as gp
from gurobipy import GRB
import xarray as xr
import random

def GurobiModel(agents, times, qty_bid, price_bid, reqdFlexI, sensi_1h, slack_penalty):
    model = gp.Model()

    # Set Matrix Variables
    agent_bids = model.addMVar((len(agents), len(times)), vtype = GRB.CONTINUOUS, name = "agent_bids")
    slacks = model.addMVar((len(reqdFlexI.columns), len(times)), vtype = GRB.CONTINUOUS, name = "slacks")

    # Set Constraints
    model.addConstrs((agent_bids[:, i] <= qty_bid.to_numpy()[i, :] for i in range(len(times))), name='MaxBidConstraint')
    model.addConstrs((sensi_1h.loc[dict(Times = times[i])].transpose().values @ agent_bids[:, i] + slacks[:, i] >=
                      reqdFlexI.loc[times[i], :].values for i in range(len(times))), name = 'Bid')
    model.addConstrs((slacks[:, i] >= 0 for i in range(len(times))), name = 'Slack')

    # Set the objective function
    model.setObjective(sum(agent_bids[:, i] @ price_bid.loc[times[i], :].values + slacks[:, i] @ slack_penalty[i, :]
                           for i in range(len(times))), GRB.MINIMIZE)

    return model

if __name__ == '__main__':
    #random.seed(0)
    #np.random.seed(0)
    times = [0, 1, 2, 3]
    lines = ["L1", "L2", "L3"]
    agents = ["A1", "A2"]
    all_branches = ["T1", "L1", "L2", "L3", "L4", "L5", "L6", "L7", "L8", "L9"]
    all_injections = ["K1", "K2", "K3", "K4", "K5", "K6", "A1", "A2"]

    price_bid = pd.DataFrame(np.random.rand(len(times), len(agents)), columns = agents, index = times)
    qty_bid = pd.DataFrame(np.random.rand(len(times), len(agents)), columns = agents, index = times)
    reqdFlexI = pd.DataFrame(np.random.rand(len(times), len(lines)), columns = lines, index = times)
    sensi = xr.DataArray(
        np.random.rand(len(times), len(agents), len(lines)),
        dims={
            "Times": times,
            "Generatoren": agents,
            "Betriebsmittel": lines
        }
    )

    slack_penalty = np.ones(reqdFlexI.shape)*price_bid.sum(axis = 1).sum(axis = 0)*100

    model = GurobiModel(agents, times, qty_bid, price_bid, reqdFlexI, sensi, slack_penalty)

    # Solve model, display the optimization problem and print out the dispatch for the agents
    model.optimize()
    model.display()

    iCounter = 0
    jCounter = 0

    agent_dispatched = qty_bid.copy()

    for v in model.getVars():
        if iCounter == 0:
            agent = "A1"
        else:
            agent = "A2"
        agent_dispatched.loc[jCounter, agent] = v.x
        jCounter += 1
        if jCounter % 4 == 0:
            iCounter += 1
            if iCounter == 2:
                break
            jCounter = 0
    print(agent_dispatched)
    print(qty_bid)