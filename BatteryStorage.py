import numpy as np
import pandas as pd
from FlexAgent import FlexAgent
from typing import List


class BatStorage(FlexAgent):
    """
        Battery Storage Class

        :param id: ID of agent
        :param location: Location of the battery agent
        :param maxPower: Maximum power of the initialized battery
        :param minPower: Minimum power of the initialized battery
        :param marginalCost: Marginal cost
        :param maxCapacity: Maximum capacity of the battery agent
        :param efficiency: Charging and discharging efficiency of the battery
        :param SOC: starting SOC
        :param minSOC: Minimum SOC, which is an operational constraint
        :param startDay: Starting Day of the simulation
        :param endDay: Ending Day of the simulation
        """

    def __init__(
            self,
            id,
            location=None,
            minPower: float = 0.,
            maxPower: float = 0.,
            voltageLevel: float = 0.,
            marginalCost: float = 0.,
            maxCapacity: float = 0.,
            efficiency: float = 1.0,
            SOC: float = 1.0,
            minSOC: float = 0.2,
            startDay: int = 0,
            endDay: int = 365,
    ):

        super().__init__(
            id=id,
            location=location,
            minPower=minPower,
            maxPower=maxPower,
            marginalCost=marginalCost,
            startDay=startDay,
            endDay=endDay
        )

        self.type = "Battery Storage"
        self.maxCapacity = maxCapacity  # capacity in MWh
        """
        How much energy is left in the battery after the spot market bids
        and after the spot market dispatch
        used to update the energyTable
        """
        self.remainingEnergy = 0
        self.voltageLevel = voltageLevel
        self.efficiency = efficiency
        self.SOC = SOC
        self.energyTable = None
        self.minSOC = minSOC
        self.minCapacity = self.minSOC * self.maxCapacity

        self.lowSpotBidLimit = -1
        self.highSpotBidLimit = 1

        self.lowFlexBidLimit = -1
        self.highFlexBidLimit = 1

        self.lowPriceLimit = 1
        self.highPriceLimit = 2

        self.penalizeTimes = []
        self.flexChangedEnergy = 0.
        self.spotChangedEnergy = 0.
        if self.voltageLevel == 10:
            self.penaltyViolation = -100

        self.obs_space_size = 2
        self.action_dict = \
            {
                0: [0, 0, 2],
                1: [0, -1, 1],
                2: [0, -1, 2],
                3: [0, 1, 1],
                4: [0, 1, 2],

                5: [1, 0, 2],
                6: [1, -1, 1],
                7: [1, -1, 2],

                8: [-1, 0, 2],
                9: [-1, 1, 1],
                10: [-1, 1, 2],
            }
        self.mean_MCP = 0

        self.reset(startDay)

    def reset(self, startDay: int = 0) -> None:
        """
        Resets the Battery Agent

        :param startDay: Starting Day of the environment
        """

        super().reset(startDay)
        self.remainingEnergy = self.maxCapacity / 2
        self.SOC = 0.5
        self.flexChangedEnergy = 0
        self.spotChangedEnergy = 0
        """extra row to store the values needed for next iteration"""
        self.energyTable = pd.DataFrame(data={'time': np.arange(self.spotHour + 1),
                                              'after_spot': np.full(self.spotHour + 1, self.remainingEnergy,
                                                                    dtype=float),
                                              'before_flex': np.full(self.spotHour + 1, self.remainingEnergy,
                                                                     dtype=float),
                                              'after_flex': np.full(self.spotHour + 1, self.remainingEnergy,
                                                                    dtype=float),
                                              'after_flex_remarks': np.full(self.spotHour + 1, 'dispatched')})

    def printInfo(self):
        super().printInfo()
        print("Type: {}".format(self.type))

    def canCharge(self, chargePower: float, chargeTime: int, status: str) -> [bool, float]:
        """
        Check whether charging is possible
        If False, returns by how much amount

        :param chargePower: Power which is to be charged
        :param chargeTime: chargeTime in hrs and chargePower in MW (+ve)
        :param status: whether after spot bids, or when making flex bids, or after flex bids
        :param index: the hour in the day
        :return: Bool whether it is possible to charge, if false returns by how much
        """
        if chargePower > self.maxPower:
            chargePower = self.maxPower

        energy = chargePower * chargeTime * self.efficiency
        if status == 'after_spot':
            if self.remainingEnergy + energy > self.maxCapacity:
                return False, self.remainingEnergy + energy - self.maxCapacity
            else:
                return True, None
        elif status == 'before_flex':
            """checks if in the current hour spot dispatched qty + remaining energy after flex bidding 
            leads to constraints violation or not"""
            if self.remainingEnergy + self.spotChangedEnergy + energy > self.maxCapacity:
                return False, self.remainingEnergy + self.spotChangedEnergy + energy - self.maxCapacity
            else:
                return True, None
        elif status == 'after_flex':
            if self.remainingEnergy + self.flexChangedEnergy > self.maxCapacity:
                return False, self.remainingEnergy + self.flexChangedEnergy - self.maxCapacity
            else:
                return True, None

    def canDischarge(self, dischargePower: float, dischargeTime: int, status: str) -> [bool, float]:
        """
        Check whether discharging is possible
        If False, returns by how much amount

        :param dischargePower: Power which is to be discharged
        :param dischargeTime: dischargeTime in hrs and chargePower in MW (+ve)
        :param status: whether after spot bids, or when making flex bids, or after flex bids
        :param index: the hour in the day
        :return: Bool whether it is possible to charge, if false returns by how much
        """
        if dischargePower < -self.maxPower:
            dischargePower = -self.maxPower

        # energy will be negative as dischargePower is negative
        energy = dischargePower * dischargeTime * self.efficiency
        if status == 'after_spot':
            if self.remainingEnergy + energy < self.minCapacity:
                return False, self.minCapacity - (self.remainingEnergy + energy)
            else:
                return True, None
        elif status == 'before_flex':
            """checks if in the current hour spot dispatched qty + remaining energy after flex bidding 
            leads to constraints violation or not"""
            if self.remainingEnergy + self.spotChangedEnergy + energy < self.minCapacity:
                return False, self.minCapacity - (self.remainingEnergy + self.spotChangedEnergy + energy)
            else:
                return True, None
        elif status == 'after_flex':
            if self.remainingEnergy + self.flexChangedEnergy < self.minCapacity:
                return False, self.minCapacity - (self.remainingEnergy + self.flexChangedEnergy)
            else:
                return True, None

    def changeSOC(self, qty: float, time: int, status: str, index: int) -> None:
        """
        Changes the SOC of the battery

        :param qty: Quantity which is to be changed
        :param time: time of the day
        :param status: if after spot bid, or before flex bid or after flex bid
        :param index: the hour in the day
        """
        energy = qty * time * self.efficiency
        if status == 'after_spot':
            self.remainingEnergy += energy
        elif status == 'before_flex':
            self.remainingEnergy = self.remainingEnergy + self.spotChangedEnergy + energy
        elif status == 'after_flex':
            self.remainingEnergy = self.remainingEnergy + self.flexChangedEnergy
        if not index == self.spotTimePeriod:
            self.energyTable.loc[self.energyTable['time'] == index, status] = self.remainingEnergy
        self.SOC = self.remainingEnergy / self.maxCapacity

    def makeSpotBid(self) -> None:
        """
        Function to make the spot bid
        """
        status = 'after_spot'
        self.spotChangedEnergy = 0.
        self.flexChangedEnergy = 0.
        self.boundSpotBidMultiplier(low=self.lowSpotBidLimit, high=self.highSpotBidLimit)
        self.energyTable['time'] = np.concatenate([self.hours, np.array([self.hours[-1] + 1])])
        super().makeSpotBid()
        self.fullSpotBid.loc[[self.time], :] = self.makeBid(self.fullSpotBid.loc[[self.time], :], status)

    def spotMarketEnd(self) -> None:
        """
        Function to compute the reward and the remaining energy after the spot market ends
        """
        spotDispatchedTimes, spotDispatchedQty = super().spotMarketEnd()
        self.spotMarketReward(spotDispatchedTimes, spotDispatchedQty)
        """change remaining energy to that of the starting time for this day to use in flex market"""
        self.remainingEnergy = \
            self.energyTable.loc[self.energyTable['time'] == self.hours[0], 'after_spot'].values[0]

    def spotMarketReward(self, time: int, qty: float) -> None:
        """
        Computes the spot market reward

        :param time: list of spotDispatchedTimes
        :param qty: list of spotDispatchedQty
        """
        # self.hourlyRewardTable = self.fullRewardTable[np.isin(self.fullRewardTable['time'].values, self.hours)]
        # Here negative of qty is used for reward because generation is negative qty
        self.fullRewardTable.loc[time, 'reward_spot'] = self.fullSpotBid.loc[time, 'MCP'] * -qty
        self.fullRewardTable.loc[self.penalizeTimes, 'reward_spot'] += self.penaltyViolation
        # self.fullRewardTable.loc[self.fullRewardTable['time'].isin(self.hours), 'reward_spot'] \
        #     = self.hourlyRewardTable['reward_spot']
        self.penalizeTimes = []

    def makeFlexBid(self, reqdFlexTimes: np.ndarray) -> None:
        """
        Function to make the flex bid

        :param reqdFlexTimes: List of reqdFlexTimes
        """

        status = 'before_flex'
        self.boundFlexBidMultiplier(low=self.lowFlexBidLimit, high=self.highFlexBidLimit)
        super().makeFlexBid(reqdFlexTimes)
        self.fullFlexBid.loc[[self.time], :] = self.makeBid(self.fullFlexBid.loc[[self.time], :], status)

    def flexMarketEnd(self) -> None:
        """
         Function to compute the flex market reward for the computed flex dispatched time, quantity and price
        """
        flexDispatchedTimes, flexDispatchedQty, flexDispatchedPrice = super().flexMarketEnd()
        """change remaining energy to that of the starting time for this day to update after_flex energy table"""
        self.remainingEnergy = \
            self.energyTable.loc[self.energyTable['time'] == self.hours[0], 'after_spot'].values[0]

        time = self.time
        qty = self.fullFlexBid.loc[self.time, 'qty_bid']
        dispatched = self.fullFlexBid.loc[self.time, 'dispatched']

        """Changed Energy Amount in the spot and flex dispatch used to update the Energy Table for after_flex times"""
        self.flexChangedEnergy = self.energyTable.loc[self.energyTable['time'] == time + 1, 'before_flex'].values[0] - \
                                 self.energyTable.loc[self.energyTable['time'] == time, 'before_flex'].values[0]
        self.spotChangedEnergy = self.energyTable.loc[self.energyTable['time'] == time + 1, 'after_spot'].values[0] - \
                                 self.energyTable.loc[self.energyTable['time'] == time, 'after_spot'].values[0]

        """
        Mod LG: added self.startDay*24 to self.spotTimePeriod, with this the if constraint is not true in case
                time + 1 is equal the last time + 1 (because python starts counting at zero)
        """
        if not time + 1 == self.startDay * 24 + self.spotTimePeriod:
            if dispatched:
                if qty >= 0:
                    possible, _ = self.canCharge(chargePower=qty, chargeTime=self.spotTimeInterval,
                                                 status='after_flex')
                    if possible and self.flexChangedEnergy <= self.maxPower:
                        self.changeSOC(qty, self.spotTimeInterval, 'after_flex', time + 1)
                    else:
                        """in changeSOC, the remaining energy is added with flexChangedEnergy"""
                        self.flexChangedEnergy = 0
                        self.changeSOC(0, self.spotTimeInterval, 'after_flex', time + 1)
                        self.penalizeTimes.append(time)
                        if not time + 1 == self.startDay * 24 + self.spotTimePeriod:
                            self.energyTable.loc[
                                self.energyTable['time'] == time + 1, 'after_flex_remarks'] = 'cant charge'
                else:
                    possible, _ = self.canDischarge(dischargePower=qty, dischargeTime=self.spotTimeInterval,
                                                    status='after_flex')
                    if possible:
                        self.changeSOC(qty, self.spotTimeInterval, 'after_flex', time + 1)
                    else:
                        self.flexChangedEnergy = 0
                        self.changeSOC(0, self.spotTimeInterval, 'after_flex', time + 1)
                        self.penalizeTimes.append(time)
                        if not time + 1 == self.spotTimePeriod:
                            self.energyTable.loc[
                                self.energyTable['time'] == time + 1, 'after_flex_remarks'] = 'cant discharge'
            else:
                self.remainingEnergy = self.remainingEnergy + self.spotChangedEnergy
                if not time + 1 == self.startDay * 24 + self.spotTimePeriod:
                    self.energyTable.loc[self.energyTable['time'] == time + 1, 'after_flex'] = self.remainingEnergy
                    self.energyTable.loc[
                        self.energyTable['time'] == time + 1, 'after_flex_remarks'] = 'not dispatched'

        """After flex market ends for a day, the final energy must be updated for all columns in energy table as it is 
        the final realised energy after the day"""
        nextTime = (self.time + 1) * self.spotHour
        self.energyTable.loc[self.energyTable['time'] == nextTime, ['after_spot', 'before_flex']] = \
            self.energyTable.loc[self.energyTable['time'] == nextTime, 'after_flex']
        self.remainingEnergy = self.energyTable.loc[self.energyTable['time'] == nextTime, 'after_flex'].values[0]
        self.SOC = self.remainingEnergy / self.maxCapacity
        """keep the energy at the end of last day as the starting energy"""
        self.energyTable.loc[0, ['after_spot', 'before_flex', 'after_flex']] = self.remainingEnergy
        self.flexMarketReward(flexDispatchedTimes, flexDispatchedQty, flexDispatchedPrice)

    def flexMarketReward(self, time: pd.Series, qty: pd.Series, price: pd.Series) -> None:
        """
        Computes the flex market rewards
        :param time: list of flexDispatchedTimes
        :param qty: list of flexDispatchedQty
        :param price: list of the prices for dispatched times
        """
        # Either way, if dispatched, the DSO pays the agents
        self.fullRewardTable.loc[time, 'reward_flex'] = price * qty.abs()
        self.fullRewardTable.loc[self.penalizeTimes, 'reward_flex'] += self.penaltyViolation

    def makeBid(self, hourlyBid: pd.DataFrame, status: str) -> pd.DataFrame:
        """
        Function to make the bid, the dailyBid DataFrame is checked whether it is possible to make the bids

        :param hourlyBid: Pandas Dataframe which is about to be checked
        :param status: whether it is after spot or before/after flex
        :return hourlyBid: Pandas Dataframe with the hourly bid which was checked whether a bid is possible
        """
        self.penalizeTimes = []

        time = hourlyBid['time'].values[0]
        qty = hourlyBid['qty_bid'].values[0]

        if status == 'before_flex':
            # amount of energy changed in the spot dispatch used to update the energy table for before_flex times
            self.spotChangedEnergy = self.energyTable.loc[
                                         self.energyTable['time'] == time + 1, 'after_spot'].values[0] - \
                                     self.energyTable.loc[self.energyTable['time'] == time, 'after_spot'].values[0]

        if qty <= 0:
            # power output to grid - generator
            # constraintAmount is positive
            possible, _ = self.canDischarge(dischargePower=qty, dischargeTime=self.spotTimeInterval, status=status)
            if possible and self.spotChangedEnergy + qty >= -self.maxPower:
                self.changeSOC(qty, self.spotTimeInterval, status, time + 1)
            else:
                """If discharging by a certain amount is not possible, change the bid to 0 and penalize the agent"""
                hourlyBid.loc[time, 'qty_bid'] = 0
                self.changeSOC(0, self.spotTimeInterval, status, time + 1)
                if not self.penalizeTimes:
                    self.penalizeTimes.append(time)

        elif qty > 0:
            # power intake from grid - consumer
            # constraintAmount is positive
            possible, _ = self.canCharge(chargePower=qty, chargeTime=self.spotTimeInterval, status=status)
            if possible and self.spotChangedEnergy + qty <= self.maxPower:
                self.changeSOC(qty, self.spotTimeInterval, status, time + 1)
            else:
                """If charging by a certain amount is not possible, change the bid to 0 and penalize the agent"""
                hourlyBid.loc[time, 'qty_bid'] = 0
                self.changeSOC(0, self.spotTimeInterval, status, time + 1)
                if not self.penalizeTimes:
                    self.penalizeTimes.append(time)

        assert -self.maxPower <= hourlyBid.loc[time, 'qty_bid'] <= self.maxPower,\
            'Qty bid cannot be more than maxPower'
        return hourlyBid
