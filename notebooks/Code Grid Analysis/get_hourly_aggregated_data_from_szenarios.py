import pandas as pd
import numpy as np
import tkinter as tk
from tkinter import filedialog
from tkinter import simpledialog
from tqdm import tqdm
import os
import re


def browse_button():
   # Allow user to select a directory
   root = tk.Tk()
   directory_path = filedialog.askdirectory(title="Bitte FGH Datenordner auswaehlen!")
   root.withdraw()
   return directory_path

# get the expansion path of the scenario, excluded 9 since this is already included for all scenarios
def ausbaupfad(i):
   switch = {
      10: [10],
      11: [11],
      12: [12],
      13: [13],
      14: [10, 14],
      15: [11, 15],
      16: [12, 16],
      17: [13, 17],
      18: [14, 18],
      19: [15, 19],
      20: [16, 20]
   }
   return switch.get(i, 'Ungültige Szenario ID!')


# hier alle Zeitreihen eintragen die für Szenarien gemerged werden sollen:
lZeitreihen_name = [
   'ang_Kunden_GHD_nnf_1h.csv',
   'ang_Kunden_HH1_nnf_corrected_1h.csv',
   'EMob_Zeitreihe_nnf_1h_fixed.csv',
   'PV_Zeitreihe_nnf_1h.csv',
   'WEA_Zeitreihe_nnf_1h.csv',
   'WP_Zeitreihe_nnf_fixed.csv'
]

lNetz_id = ['186', '245', '1899', '2464', '2767', '3005', '3544', '3756']
sName_szenario = ['9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']
sNames = ["HH_Load", "GHD", "EV", "WP", "PV", "WEA"]
iTimes = range(1, 8761, 1)
dic_szenario_to_idx = {'9':0, '10':1, '11':2, '12':3, '13':4, '14':5, '15':6, '16':7, '17':8, '18':8, '19':10, '20':11}
dic_netzid_to_idx = {'186':0, '245':1, '1899':2, '2464':3, '2767':4, '3005':5, '3544':6, '3756':7}

# Datenordner auswählen und Szenario_ID abfragen:
#path = browse_button()
path = r"E:\Projekte\enera\Netze AP13"
save_path = r"E:\Projekte\Gajewski\Netze MA_2021\Stuendliche Auswertung Netze und Betriebsmittel\Daten/"

# Data Frames for storing energetic features like peak power/load of each grid
dfHH_1h_186 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfHH_1h_186 = dfHH_1h_186.set_axis(sName_szenario, axis = 'index')
dfHH_1h_245 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfHH_1h_245 = dfHH_1h_245.set_axis(sName_szenario, axis = 'index')
dfHH_1h_1899 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfHH_1h_1899 = dfHH_1h_1899.set_axis(sName_szenario, axis = 'index')
dfHH_1h_2464 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfHH_1h_2464 = dfHH_1h_2464.set_axis(sName_szenario, axis = 'index')
dfHH_1h_2767 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfHH_1h_2767 = dfHH_1h_2767.set_axis(sName_szenario, axis = 'index')
dfHH_1h_3005 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfHH_1h_3005 = dfHH_1h_3005.set_axis(sName_szenario, axis = 'index')
dfHH_1h_3544 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfHH_1h_3544 = dfHH_1h_3544.set_axis(sName_szenario, axis = 'index')
dfHH_1h_3756 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfHH_1h_3756 = dfHH_1h_3756.set_axis(sName_szenario, axis = 'index')

dfHH_1h = [dfHH_1h_186, dfHH_1h_245, dfHH_1h_1899, dfHH_1h_2464, dfHH_1h_2767, dfHH_1h_3005, dfHH_1h_3544, dfHH_1h_3756]

dfGHD_1h_186 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfGHD_1h_186 = dfGHD_1h_186.set_axis(sName_szenario, axis = 'index')
dfGHD_1h_245 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfGHD_1h_245 = dfGHD_1h_245.set_axis(sName_szenario, axis = 'index')
dfGHD_1h_1899 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfGHD_1h_1899 = dfGHD_1h_1899.set_axis(sName_szenario, axis = 'index')
dfGHD_1h_2464 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfGHD_1h_2464 = dfGHD_1h_2464.set_axis(sName_szenario, axis = 'index')
dfGHD_1h_2767 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfGHD_1h_2767 = dfGHD_1h_2767.set_axis(sName_szenario, axis = 'index')
dfGHD_1h_3005 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfGHD_1h_3005 = dfGHD_1h_3005.set_axis(sName_szenario, axis = 'index')
dfGHD_1h_3544 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfGHD_1h_3544 = dfGHD_1h_3544.set_axis(sName_szenario, axis = 'index')
dfGHD_1h_3756 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfGHD_1h_3756 = dfGHD_1h_3756.set_axis(sName_szenario, axis = 'index')

dfGHD_1h = [dfGHD_1h_186, dfGHD_1h_245, dfGHD_1h_1899, dfGHD_1h_2464, dfGHD_1h_2767, dfGHD_1h_3005, dfGHD_1h_3544, dfGHD_1h_3756]

dfEMob_1h_186 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfEMob_1h_186 = dfEMob_1h_186.set_axis(sName_szenario, axis = 'index')
dfEMob_1h_245 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfEMob_1h_245 = dfEMob_1h_245.set_axis(sName_szenario, axis = 'index')
dfEMob_1h_1899 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfEMob_1h_1899 = dfEMob_1h_1899.set_axis(sName_szenario, axis = 'index')
dfEMob_1h_2464 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfEMob_1h_2464 = dfEMob_1h_2464.set_axis(sName_szenario, axis = 'index')
dfEMob_1h_2767 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfEMob_1h_2767 = dfEMob_1h_2767.set_axis(sName_szenario, axis = 'index')
dfEMob_1h_3005 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfEMob_1h_3005 = dfEMob_1h_3005.set_axis(sName_szenario, axis = 'index')
dfEMob_1h_3544 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfEMob_1h_3544 = dfEMob_1h_3544.set_axis(sName_szenario, axis = 'index')
dfEMob_1h_3756 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfEMob_1h_3756 = dfEMob_1h_3756.set_axis(sName_szenario, axis = 'index')

dfEMob_1h = [dfEMob_1h_186, dfEMob_1h_245, dfEMob_1h_1899, dfEMob_1h_2464, dfEMob_1h_2767, dfEMob_1h_3005, dfEMob_1h_3544, dfEMob_1h_3756]

dfPV_1h_186 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfPV_1h_186 = dfPV_1h_186.set_axis(sName_szenario, axis = 'index')
dfPV_1h_245 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfPV_1h_245 = dfPV_1h_245.set_axis(sName_szenario, axis = 'index')
dfPV_1h_1899 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfPV_1h_1899 = dfPV_1h_1899.set_axis(sName_szenario, axis = 'index')
dfPV_1h_2464 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfPV_1h_2464 = dfPV_1h_2464.set_axis(sName_szenario, axis = 'index')
dfPV_1h_2767 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfPV_1h_2767 = dfPV_1h_2767.set_axis(sName_szenario, axis = 'index')
dfPV_1h_3005 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfPV_1h_3005 = dfPV_1h_3005.set_axis(sName_szenario, axis = 'index')
dfPV_1h_3544 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfPV_1h_3544 = dfPV_1h_3544.set_axis(sName_szenario, axis = 'index')
dfPV_1h_3756 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfPV_1h_3756 = dfPV_1h_3756.set_axis(sName_szenario, axis = 'index')

dfPV_1h = [dfPV_1h_186, dfPV_1h_245, dfPV_1h_1899, dfPV_1h_2464, dfPV_1h_2767, dfPV_1h_3005, dfPV_1h_3544, dfPV_1h_3756]

dfWP_1h_186 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWP_1h_186 = dfWP_1h_186.set_axis(sName_szenario, axis = 'index')
dfWP_1h_245 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWP_1h_245 = dfWP_1h_245.set_axis(sName_szenario, axis = 'index')
dfWP_1h_1899 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWP_1h_1899 = dfWP_1h_1899.set_axis(sName_szenario, axis = 'index')
dfWP_1h_2464 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWP_1h_2464 = dfWP_1h_2464.set_axis(sName_szenario, axis = 'index')
dfWP_1h_2767 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWP_1h_2767 = dfWP_1h_2767.set_axis(sName_szenario, axis = 'index')
dfWP_1h_3005 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWP_1h_3005 = dfWP_1h_3005.set_axis(sName_szenario, axis = 'index')
dfWP_1h_3544 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWP_1h_3544 = dfWP_1h_3544.set_axis(sName_szenario, axis = 'index')
dfWP_1h_3756 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWP_1h_3756 = dfWP_1h_3756.set_axis(sName_szenario, axis = 'index')

dfWP_1h = [dfWP_1h_186, dfWP_1h_245, dfWP_1h_1899, dfWP_1h_2464, dfWP_1h_2767, dfWP_1h_3005, dfWP_1h_3544, dfWP_1h_3756]

dfWEA_1h_186 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWEA_1h_186 = dfWEA_1h_186.set_axis(sName_szenario, axis = 'index')
dfWEA_1h_245 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWEA_1h_245 = dfWEA_1h_245.set_axis(sName_szenario, axis = 'index')
dfWEA_1h_1899 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWEA_1h_1899 = dfWEA_1h_1899.set_axis(sName_szenario, axis = 'index')
dfWEA_1h_2464 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWEA_1h_2464 = dfWEA_1h_2464.set_axis(sName_szenario, axis = 'index')
dfWEA_1h_2767 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWEA_1h_2767 = dfWEA_1h_2767.set_axis(sName_szenario, axis = 'index')
dfWEA_1h_3005 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWEA_1h_3005 = dfWEA_1h_3005.set_axis(sName_szenario, axis = 'index')
dfWEA_1h_3544 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWEA_1h_3544 = dfWEA_1h_3544.set_axis(sName_szenario, axis = 'index')
dfWEA_1h_3756 = pd.DataFrame(np.zeros((len(sName_szenario), len(iTimes))), columns = iTimes)
dfWEA_1h_3756 = dfWEA_1h_3756.set_axis(sName_szenario, axis = 'index')

dfWEA_1h = [dfWEA_1h_186, dfWEA_1h_245, dfWEA_1h_1899, dfWEA_1h_2464, dfWEA_1h_2767, dfWEA_1h_3005, dfWEA_1h_3544, dfWEA_1h_3756]

lHH_filenames = []
lGHD_filenames = []

for sNetz_id in lNetz_id:
   sPath_load = path + '/' + sNetz_id
   for r, dirs, files in os.walk(sPath_load):
      if len(re.findall(r"Sensis", r)) > 0 or len(re.findall(r"mm1h", r)) > 0:
         continue
      else:
         for file in files:
            if (len(re.findall(r"ang_Kunden_HH.{1,4}_nnf_corrected_1h.csv", file)) > 0) and sPath_load == r \
                    and'ang_Kunden_HH1_nnf_corrected_1h.csv' in lZeitreihen_name:
               lHH_filenames.append(os.path.join(r, file))
            elif file == 'ang_Kunden_GHD_nnf_1h.csv' and sPath_load == r and 'ang_Kunden_HH1_nnf_corrected_1h.csv' in lZeitreihen_name:
               lGHD_filenames.append(os.path.join(r, file))
            # Read the corresponding EMob time series and write them in the dfEMob dataframe
            elif file == 'EMob_Zeitreihe_nnf_1h_fixed.csv' and file in lZeitreihen_name and \
                    len(re.findall("\d\d\d_\d$",r) + re.findall("\d\d\d_\d\d$",r) + re.findall("\d\d\d\d_\d$",r) + re.findall("\d\d\d\d_\d\d$",r))> 0:
               sSzenario = (re.findall("\d\d\d_\d$", r) + re.findall("\d\d\d_\d\d$", r) + re.findall("\d\d\d\d_\d$",r) + re.findall("d\d\d\d_\d\d$", r))[0].split("_", 1)[1]
               if sSzenario in sName_szenario:
                  dfCSV = pd.read_csv(os.path.join(r, file), sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                                        dtype=np.float32)
                  seriesHourly_power = dfCSV.sum(axis=1)
                  fHourlyPower = seriesHourly_power.to_numpy()[:8760]
                  list_idx = dic_netzid_to_idx[sNetz_id]
                  if sSzenario == '9':
                     dfEMob_1h[list_idx] = dfEMob_1h[list_idx] + fHourlyPower
                  else:
                     list_szenario = ausbaupfad(int(sSzenario))
                     dfEMob_1h[list_idx].loc[sSzenario, :] =  dfEMob_1h[list_idx].loc[sSzenario, :] + fHourlyPower
                     for i in list_szenario[0:-1]:
                        dfEMob_1h[list_idx].loc[sSzenario, :] = dfEMob_1h[list_idx].loc[sSzenario, :] + dfEMob_1h[list_idx].loc[str(i), :]

            elif file == 'PV_Zeitreihe_nnf_1h.csv' and file in lZeitreihen_name and \
                    len(re.findall("\d\d\d_\d$",r) + re.findall("\d\d\d_\d\d$",r) + re.findall("\d\d\d\d_\d$",r) + re.findall("\d\d\d\d_\d\d$",r))> 0:
               sSzenario = (re.findall("\d\d\d_\d$", r) + re.findall("\d\d\d_\d\d$", r) + re.findall("\d\d\d\d_\d$",r) + re.findall("d\d\d\d_\d\d$", r))[0].split("_", 1)[1]
               if sSzenario in sName_szenario:
                  dfCSV = pd.read_csv(os.path.join(r, file), sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                                        dtype=np.float32)
                  seriesHourly_power = dfCSV.sum(axis=1)
                  fHourlyPower = seriesHourly_power.to_numpy()[:8760]
                  list_idx = dic_netzid_to_idx[sNetz_id]
                  if sSzenario == '9':
                     dfPV_1h[list_idx] = dfPV_1h[list_idx] + fHourlyPower
                  else:
                     list_szenario = ausbaupfad(int(sSzenario))
                     dfPV_1h[list_idx].loc[sSzenario, :] =  dfPV_1h[list_idx].loc[sSzenario, :] + fHourlyPower
                     for i in list_szenario[0:-1]:
                        dfPV_1h[list_idx].loc[sSzenario, :] = dfPV_1h[list_idx].loc[sSzenario, :] + dfPV_1h[list_idx].loc[str(i), :]

            elif file == 'WEA_Zeitreihe_nnf_1h.csv' and file in lZeitreihen_name and \
                    len(re.findall("\d\d\d_\d$",r) + re.findall("\d\d\d_\d\d$",r) + re.findall("\d\d\d\d_\d$",r) + re.findall("\d\d\d\d_\d\d$",r))> 0:
               sSzenario = (re.findall("\d\d\d_\d$", r) + re.findall("\d\d\d_\d\d$", r) + re.findall("\d\d\d\d_\d$",r) + re.findall("d\d\d\d_\d\d$", r))[0].split("_", 1)[1]
               if sSzenario in sName_szenario:
                  dfCSV = pd.read_csv(os.path.join(r, file), sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                                        dtype=np.float32)
                  seriesHourly_power = dfCSV.sum(axis=1)
                  fHourlyPower = seriesHourly_power.to_numpy()[:8760]
                  list_idx = dic_netzid_to_idx[sNetz_id]
                  if sSzenario == '9':
                     dfWEA_1h[list_idx] = dfWEA_1h[list_idx] + fHourlyPower
                  else:
                     list_szenario = ausbaupfad(int(sSzenario))
                     dfWEA_1h[list_idx].loc[sSzenario, :] =  dfWEA_1h[list_idx].loc[sSzenario, :] + fHourlyPower
                     for i in list_szenario[0:-1]:
                        dfWEA_1h[list_idx].loc[sSzenario, :] = dfWEA_1h[list_idx].loc[sSzenario, :] + dfWEA_1h[list_idx].loc[str(i), :]


            elif (file == 'WP_Zeitreihe_nnf_fixed.csv' and file in lZeitreihen_name or file == 'WP_Zeitreihe_nnf.csv' and sNetz_id == '3544') and \
                    len(re.findall("\d\d\d_\d$",r) + re.findall("\d\d\d_\d\d$",r) + re.findall("\d\d\d\d_\d$",r) + re.findall("\d\d\d\d_\d\d$",r))> 0:
               sSzenario = (re.findall("\d\d\d_\d$", r) + re.findall("\d\d\d_\d\d$", r) + re.findall("\d\d\d\d_\d$",r) + re.findall("d\d\d\d_\d\d$", r))[0].split("_", 1)[1]
               if sSzenario in sName_szenario:
                  dfCSV = pd.read_csv(os.path.join(r, file), sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                                        dtype=np.float32)
                  seriesHourly_power = dfCSV.sum(axis=1)
                  fHourlyPower = seriesHourly_power.to_numpy()[:8760]
                  list_idx = dic_netzid_to_idx[sNetz_id]
                  if sSzenario == '9':
                     dfWP_1h[list_idx] = dfWP_1h[list_idx] + fHourlyPower
                  else:
                     list_szenario = ausbaupfad(int(sSzenario))
                     dfWP_1h[list_idx].loc[sSzenario, :] =  dfWP_1h[list_idx].loc[sSzenario, :] + fHourlyPower
                     for i in list_szenario[0:-1]:
                        dfWP_1h[list_idx].loc[sSzenario, :] = dfWP_1h[list_idx].loc[sSzenario, :] + dfWP_1h[list_idx].loc[str(i), :]

   for sFilenames in lHH_filenames:
      # Get all CSVs with for GHD and HH
      dfCSV = pd.read_csv(sFilenames, sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                            dtype=np.float32)
      seriesHourly_power = dfCSV.sum(axis=1)
      fHourlyPower = seriesHourly_power.to_numpy()[:8760]
      list_idx = dic_netzid_to_idx[sNetz_id]
      dfHH_1h[list_idx] = dfHH_1h[list_idx] + fHourlyPower

   for sFilenames in lGHD_filenames:
      # Get all CSVs with for GHD and HH
      dfCSV = pd.read_csv(sFilenames, sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                            dtype=np.float32)
      seriesHourly_power = dfCSV.sum(axis=1)
      fHourlyPower = seriesHourly_power.to_numpy()[:8760]
      list_idx = dic_netzid_to_idx[sNetz_id]
      dfGHD_1h[list_idx] = dfGHD_1h[list_idx] + fHourlyPower

   lGHD_filenames = []
   lHH_filenames = []


if 'EMob_Zeitreihe_nnf_1h_fixed.csv'in lZeitreihen_name:
   i = 0
   for dfEMob in dfEMob_1h:
      dfEMob.to_csv(
         save_path + "EMob" + "/" + "EMob_1h" + lNetz_id[i] + '.csv',
         sep=';',
         header=True,
         index=True,
         encoding='latin-1',
      )
      i = i+1

if 'ang_Kunden_HH1_nnf_corrected_1h.csv'in lZeitreihen_name:
   i = 0
   for dfHH in dfHH_1h:
      dfHH.to_csv(
         save_path + "HH" + "/" + "HH_1h" + lNetz_id[i] + '.csv',
         sep=';',
         header=True,
         index=True,
         encoding='latin-1',
      )
      i = i+1

if 'PV_Zeitreihe_nnf_1h.csv'in lZeitreihen_name:
   i = 0
   for dfPV in dfPV_1h:
      dfPV.to_csv(
         save_path + "PV" + "/" + "PV_1h" + lNetz_id[i] + '.csv',
         sep=';',
         header=True,
         index=True,
         encoding='latin-1',
      )
      i = i+1

if 'ang_Kunden_GHD_nnf_1h.csv'in lZeitreihen_name:
   i = 0
   for dfGHD in dfGHD_1h:
      dfGHD.to_csv(
         save_path + "GHD" + "/" + "GHD_1h" + lNetz_id[i] + '.csv',
         sep=';',
         header=True,
         index=True,
         encoding='latin-1',
      )
      i = i+1

if 'WEA_Zeitreihe_nnf_1h.csv'in lZeitreihen_name:
   i = 0
   for dfWEA in dfWEA_1h:
      dfWEA.to_csv(
         save_path + "WEA" + "/" + "WEA_1h" + lNetz_id[i] + '.csv',
         sep=';',
         header=True,
         index=True,
         encoding='latin-1',
      )
      i = i+1

if 'WP_Zeitreihe_nnf_fixed.csv'in lZeitreihen_name:
   i = 0
   for dfWP in dfWP_1h:
      dfWP.to_csv(
         save_path + "WP" + "/" + "WP_1h" + lNetz_id[i] + '.csv',
         sep=';',
         header=True,
         index=True,
         encoding='latin-1',
      )
      i = i+1