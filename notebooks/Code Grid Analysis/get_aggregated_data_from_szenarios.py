import pandas as pd
import numpy as np
import tkinter as tk
from tkinter import filedialog
from tkinter import simpledialog
from tqdm import tqdm
import os
import re

def browse_button():
   # Allow user to select a directory
   root = tk.Tk()
   directory_path = filedialog.askdirectory(title="Bitte FGH Datenordner auswaehlen!")
   root.withdraw()
   return directory_path

# get the expansion path of the scenario, excluded 9 since this is already included for all scenarios
def ausbaupfad(i):
   switch = {
      10: [10],
      11: [11],
      12: [12],
      13: [13],
      14: [10, 14],
      15: [11, 15],
      16: [12, 16],
      17: [13, 17],
      18: [14, 18],
      19: [15, 19],
      20: [16, 20]
   }
   return switch.get(i, 'Ungültige Szenario ID!')


# hier alle Zeitreihen eintragen die für Szenarien gemerged werden sollen:
lZeitreihen_name = [
   'ang_Kunden_GHD_nnf_1h.csv',
   'ang_Kunden_HH1_nnf_corrected_1h.csv',
   'EMob_Zeitreihe_nnf_1h_fixed.csv',
   'PV_Zeitreihe_nnf_1h.csv',
   'WEA_Zeitreihe_nnf_1h.csv',
   'WP_Zeitreihe_nnf_fixed.csv'
]
lNetz_id = ['186', '245', '1899', '2464', '2767', '3005', '3544', '3756']
sName_szenario = ['9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']
sNames = ["HH_Load", "GHD", "EV", "WP", "PV", "WEA"]

# Datenordner auswählen und Szenario_ID abfragen:
#path = browse_button()
path = r"E:\Projekte\enera\Netze AP13"
save_path = r"E:\Projekte\Gajewski\Netze MA_2021/"

dfHH = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfHH = dfHH.set_axis(sName_szenario, axis = 'index')
dfGHD = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfGHD = dfGHD.set_axis(sName_szenario, axis = 'index')
dfEMob = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfEMob = dfEMob.set_axis(sName_szenario, axis = 'index')
dfPV = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfPV = dfPV.set_axis(sName_szenario, axis = 'index')
dfWEA = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfWEA = dfWEA.set_axis(sName_szenario, axis = 'index')
dfWP = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfWP = dfWP.set_axis(sName_szenario, axis = 'index')

dfCountHH = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfCountHH = dfCountHH.set_axis(sName_szenario, axis = 'index')
dfCountGHD = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfCountGHD = dfCountGHD.set_axis(sName_szenario, axis = 'index')
dfCountEMob = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfCountEMob = dfCountEMob.set_axis(sName_szenario, axis = 'index')
dfCountPV = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfCountPV = dfCountPV.set_axis(sName_szenario, axis = 'index')
dfCountWEA = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfCountWEA = dfCountWEA.set_axis(sName_szenario, axis = 'index')
dfCountWP = pd.DataFrame(np.zeros((len(sName_szenario), len(lNetz_id))), columns = lNetz_id)
dfCountWP = dfCountWP.set_axis(sName_szenario, axis = 'index')


# Shuffle the data from one single utility mapped to all grid numbers to all utilities mapped to one grid_number
dfGrid_186 = pd.DataFrame(np.zeros((len(sName_szenario), len(sNames))), columns = sNames)
dfGrid_186 = dfGrid_186.set_axis(sName_szenario, axis = 'index')
dfGrid_245 = pd.DataFrame(np.zeros((len(sName_szenario), len(sNames))), columns = sNames)
dfGrid_245 = dfGrid_245.set_axis(sName_szenario, axis = 'index')
dfGrid_1899 = pd.DataFrame(np.zeros((len(sName_szenario), len(sNames))), columns = sNames)
dfGrid_1899 = dfGrid_1899.set_axis(sName_szenario, axis = 'index')
dfGrid_2464 = pd.DataFrame(np.zeros((len(sName_szenario), len(sNames))), columns = sNames)
dfGrid_2464 = dfGrid_2464.set_axis(sName_szenario, axis = 'index')
dfGrid_2767 = pd.DataFrame(np.zeros((len(sName_szenario), len(sNames))), columns = sNames)
dfGrid_2767 = dfGrid_2767.set_axis(sName_szenario, axis = 'index')
dfGrid_3005 = pd.DataFrame(np.zeros((len(sName_szenario), len(sNames))), columns = sNames)
dfGrid_3005 = dfGrid_3005.set_axis(sName_szenario, axis = 'index')
dfGrid_3544 = pd.DataFrame(np.zeros((len(sName_szenario), len(sNames))), columns = sNames)
dfGrid_3544 = dfGrid_3544.set_axis(sName_szenario, axis = 'index')
dfGrid_3756 = pd.DataFrame(np.zeros((len(sName_szenario), len(sNames))), columns = sNames)
dfGrid_3756 = dfGrid_3756.set_axis(sName_szenario, axis = 'index')

lHH_filenames = []
lGHD_filenames = []

for sNetz_id in lNetz_id:
   sPath_load = path + '/' + sNetz_id
   for r, dirs, files in os.walk(sPath_load):
      if len(re.findall(r"Sensis", r)) > 0 or len(re.findall(r"mm1h", r)) > 0:
         continue
      else:
         for file in files:
            if (len(re.findall(r"ang_Kunden_HH.{1,4}_nnf_corrected_1h.csv", file)) > 0) and sPath_load == r \
                    and'ang_Kunden_HH1_nnf_corrected_1h.csv' in lZeitreihen_name:
               lHH_filenames.append(os.path.join(r, file))
            elif file == 'ang_Kunden_GHD_nnf_1h.csv' and sPath_load == r and 'ang_Kunden_HH1_nnf_corrected_1h.csv' in lZeitreihen_name:
               lGHD_filenames.append(os.path.join(r, file))
            # Read the corresponding EMob time series and write them in the dfEMob dataframe
            elif file == 'EMob_Zeitreihe_nnf_1h_fixed.csv' and file in lZeitreihen_name and \
                    len(re.findall("\d\d\d_\d$",r) + re.findall("\d\d\d_\d\d$",r) + re.findall("\d\d\d\d_\d$",r) + re.findall("\d\d\d\d_\d\d$",r))> 0:
               sSzenario = (re.findall("\d\d\d_\d$", r) + re.findall("\d\d\d_\d\d$", r) + re.findall("\d\d\d\d_\d$",r) + re.findall("d\d\d\d_\d\d$", r))[0].split("_", 1)[1]
               if sSzenario in sName_szenario:
                  #list_EMOB_filenames.append(os.path.join(r, file))
                  dfCSV = pd.read_csv(os.path.join(r, file), sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                                        dtype=np.float32)
                  fHourly_power = dfCSV.sum(axis=1)
                  fYearly_power = fHourly_power.sum()
                  iCount_EMob = len(dfCSV.columns)
                  if sSzenario == '9':
                     dfEMob.loc[:, sNetz_id] = dfEMob.loc[:, sNetz_id] + fYearly_power
                     dfCountEMob.loc[:, sNetz_id] = dfCountEMob.loc[:, sNetz_id] + iCount_EMob
                  else:
                     list_szenario = ausbaupfad(int(sSzenario))
                     dfEMob.loc[sSzenario, sNetz_id] =  dfEMob.loc[sSzenario, sNetz_id] + fYearly_power
                     dfCountEMob.loc[sSzenario, sNetz_id] =  dfCountEMob.loc[sSzenario, sNetz_id] + iCount_EMob
                     for i in list_szenario[0:-1]:
                        dfEMob.loc[sSzenario, sNetz_id] = dfEMob.loc[sSzenario, sNetz_id] + dfEMob.loc[str(i), sNetz_id]
                        dfCountEMob.loc[sSzenario, sNetz_id] = dfCountEMob.loc[sSzenario, sNetz_id] + dfCountEMob.loc[str(i), sNetz_id]

            elif file == 'PV_Zeitreihe_nnf_1h.csv' and file in lZeitreihen_name and \
                    len(re.findall("\d\d\d_\d$",r) + re.findall("\d\d\d_\d\d$",r) + re.findall("\d\d\d\d_\d$",r) + re.findall("\d\d\d\d_\d\d$",r))> 0:
               sSzenario = (re.findall("\d\d\d_\d$", r) + re.findall("\d\d\d_\d\d$", r) + re.findall("\d\d\d\d_\d$",r) + re.findall("d\d\d\d_\d\d$", r))[0].split("_", 1)[1]
               if sSzenario in sName_szenario:
                  dfCSV = pd.read_csv(os.path.join(r, file), sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                                        dtype=np.float32)
                  fHourly_power = dfCSV.sum(axis=1)
                  fYearly_power = fHourly_power.sum()
                  iCount_PV = len(dfCSV.columns)
                  if sSzenario == '9':
                     dfPV.loc[:, sNetz_id] = dfPV.loc[:, sNetz_id] + fYearly_power
                     dfCountPV.loc[:, sNetz_id] = dfCountPV.loc[:, sNetz_id] + iCount_PV
                  else:
                     list_szenario = ausbaupfad(int(sSzenario))
                     dfPV.loc[sSzenario, sNetz_id] =  dfPV.loc[sSzenario, sNetz_id] + fYearly_power
                     dfCountPV.loc[sSzenario, sNetz_id] = dfCountPV.loc[sSzenario, sNetz_id] + iCount_PV
                     for i in list_szenario[0:-1]:
                        dfPV.loc[sSzenario, sNetz_id] = dfPV.loc[sSzenario, sNetz_id] + dfPV.loc[str(i), sNetz_id]
                        dfCountPV.loc[sSzenario, sNetz_id] = dfCountPV.loc[sSzenario, sNetz_id] + dfCountPV.loc[str(i), sNetz_id]

            elif file == 'WEA_Zeitreihe_nnf_1h.csv' and file in lZeitreihen_name and \
                    len(re.findall("\d\d\d_\d$",r) + re.findall("\d\d\d_\d\d$",r) + re.findall("\d\d\d\d_\d$",r) + re.findall("\d\d\d\d_\d\d$",r))> 0:
               sSzenario = (re.findall("\d\d\d_\d$", r) + re.findall("\d\d\d_\d\d$", r) + re.findall("\d\d\d\d_\d$",r) + re.findall("d\d\d\d_\d\d$", r))[0].split("_", 1)[1]
               if sSzenario in sName_szenario:
                  dfCSV = pd.read_csv(os.path.join(r, file), sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                                        dtype=np.float32)
                  fHourly_power = dfCSV.sum(axis=1)
                  fYearly_power = fHourly_power.sum()
                  iCount_WEA = len(dfCSV.columns)
                  if sSzenario == '9':
                     dfWEA.loc[:, sNetz_id] = dfWEA.loc[:, sNetz_id] + fYearly_power
                     dfCountWEA.loc[:, sNetz_id] = dfCountWEA.loc[:, sNetz_id] + iCount_WEA
                  else:
                     list_szenario = ausbaupfad(int(sSzenario))
                     dfWEA.loc[sSzenario, sNetz_id] =  dfWEA.loc[sSzenario, sNetz_id] + iCount_WEA
                     dfCountWEA.loc[sSzenario, sNetz_id] = dfCountWEA.loc[sSzenario, sNetz_id] + iCount_WEA
                     for i in list_szenario[0:-1]:
                        dfWEA.loc[sSzenario, sNetz_id] = dfWEA.loc[sSzenario, sNetz_id] + dfWEA.loc[str(i), sNetz_id]
                        dfCountWEA.loc[sSzenario, sNetz_id] = dfCountWEA.loc[sSzenario, sNetz_id] + dfCountWEA.loc[str(i), sNetz_id]

            elif (file == 'WP_Zeitreihe_nnf_fixed.csv' and file in lZeitreihen_name or file == 'WP_Zeitreihe_nnf.csv' and sNetz_id == '3544') and \
                    len(re.findall("\d\d\d_\d$",r) + re.findall("\d\d\d_\d\d$",r) + re.findall("\d\d\d\d_\d$",r) + re.findall("\d\d\d\d_\d\d$",r))> 0:
               sSzenario = (re.findall("\d\d\d_\d$", r) + re.findall("\d\d\d_\d\d$", r) + re.findall("\d\d\d\d_\d$",r) + re.findall("d\d\d\d_\d\d$", r))[0].split("_", 1)[1]
               if sSzenario in sName_szenario:
                  dfCSV = pd.read_csv(os.path.join(r, file), sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                                        dtype=np.float32)
                  fHourly_power = dfCSV.sum(axis=1)
                  fYearly_power = fHourly_power.sum()
                  iCount_WP = len(dfCSV.columns)
                  if sSzenario == '9':
                     dfWP.loc[:, sNetz_id] = dfWP.loc[:, sNetz_id] + fYearly_power
                     dfCountWP.loc[:, sNetz_id] = dfCountWP.loc[:, sNetz_id] + iCount_WP
                  else:
                     list_szenario = ausbaupfad(int(sSzenario))
                     dfWP.loc[sSzenario, sNetz_id] =  dfWP.loc[sSzenario, sNetz_id] + fYearly_power
                     dfCountWP.loc[sSzenario, sNetz_id] = dfCountWP.loc[sSzenario, sNetz_id] + iCount_WP
                     for i in list_szenario[0:-1]:
                        dfWP.loc[sSzenario, sNetz_id] = dfWP.loc[sSzenario, sNetz_id] + dfWP.loc[str(i), sNetz_id]
                        dfCountWP.loc[sSzenario, sNetz_id] = dfCountWP.loc[sSzenario, sNetz_id] + dfCountWP.loc[str(i), sNetz_id]

   for sFilenames in lHH_filenames:
      # Get all CSVs with for GHD and HH
      dfCSV = pd.read_csv(sFilenames, sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                            dtype=np.float32)
      hourly_load = dfCSV.sum(axis=1)
      yearly_load = hourly_load.sum()
      iCount_HH = len(dfCSV.columns)
      dfHH.loc[:, sNetz_id] = dfHH.loc[:, sNetz_id] + yearly_load
      dfCountHH.loc[:, sNetz_id] = dfCountHH.loc[:, sNetz_id] + iCount_HH

   for sFilenames in lGHD_filenames:
      # Get all CSVs with for GHD and HH
      dfCSV = pd.read_csv(sFilenames, sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=2,
                            dtype=np.float32)
      hourly_load = dfCSV.sum(axis=1)
      yearly_load = hourly_load.sum()
      iCount_GHD = len(dfCSV.columns)
      dfGHD.loc[:, sNetz_id] = dfGHD.loc[:, sNetz_id] + yearly_load
      dfCountGHD.loc[:, sNetz_id] = dfCountHH.loc[:, sNetz_id] + iCount_GHD

   lGHD_filenames = []
   lHH_filenames = []

for sSzenario in sName_szenario: # and len(lZeitreihen_name) == 6:
    dfGrid_186.loc[sSzenario, "HH_Load"] = dfHH.loc[sSzenario, '186']
    dfGrid_186.loc[sSzenario, "GHD"] = dfGHD.loc[sSzenario, '186']
    dfGrid_186.loc[sSzenario, "EV"] = dfEMob.loc[sSzenario, '186']
    dfGrid_186.loc[sSzenario, "WP"] = dfWP.loc[sSzenario, '186']
    dfGrid_186.loc[sSzenario, "PV"] = dfPV.loc[sSzenario, '186']
    dfGrid_186.loc[sSzenario, "WEA"] = dfWEA.loc[sSzenario, '186']
    dfGrid_245.loc[sSzenario, "HH_Load"] = dfHH.loc[sSzenario, '245']
    dfGrid_245.loc[sSzenario, "GHD"] = dfGHD.loc[sSzenario, '245']
    dfGrid_245.loc[sSzenario, "EV"] = dfEMob.loc[sSzenario, '245']
    dfGrid_245.loc[sSzenario, "WP"] = dfWP.loc[sSzenario, '245']
    dfGrid_245.loc[sSzenario, "PV"] = dfPV.loc[sSzenario, '245']
    dfGrid_245.loc[sSzenario, "WEA"] = dfWEA.loc[sSzenario, '245']
    dfGrid_1899.loc[sSzenario, "HH_Load"] = dfHH.loc[sSzenario, '1899']
    dfGrid_1899.loc[sSzenario, "GHD"] = dfGHD.loc[sSzenario, '1899']
    dfGrid_1899.loc[sSzenario, "EV"] = dfEMob.loc[sSzenario, '1899']
    dfGrid_1899.loc[sSzenario, "WP"] = dfWP.loc[sSzenario, '1899']
    dfGrid_1899.loc[sSzenario, "PV"] = dfPV.loc[sSzenario, '1899']
    dfGrid_1899.loc[sSzenario, "WEA"] = dfWEA.loc[sSzenario, '1899']
    dfGrid_2464.loc[sSzenario, "HH_Load"] = dfHH.loc[sSzenario, '2464']
    dfGrid_2464.loc[sSzenario, "GHD"] = dfGHD.loc[sSzenario, '2464']
    dfGrid_2464.loc[sSzenario, "EV"] = dfEMob.loc[sSzenario, '2464']
    dfGrid_2464.loc[sSzenario, "WP"] = dfWP.loc[sSzenario, '2464']
    dfGrid_2464.loc[sSzenario, "PV"] = dfPV.loc[sSzenario, '2464']
    dfGrid_2767.loc[sSzenario, "WEA"] = dfWEA.loc[sSzenario, '2464']
    dfGrid_2767.loc[sSzenario, "HH_Load"] = dfHH.loc[sSzenario, '2767']
    dfGrid_2767.loc[sSzenario, "GHD"] = dfGHD.loc[sSzenario, '2767']
    dfGrid_2767.loc[sSzenario, "EV"] = dfEMob.loc[sSzenario, '2767']
    dfGrid_2767.loc[sSzenario, "WP"] = dfWP.loc[sSzenario, '2767']
    dfGrid_2767.loc[sSzenario, "PV"] = dfPV.loc[sSzenario, '2767']
    dfGrid_2767.loc[sSzenario, "WEA"] = dfWEA.loc[sSzenario, '2767']
    dfGrid_3005.loc[sSzenario, "HH_Load"] = dfHH.loc[sSzenario, '3005']
    dfGrid_3005.loc[sSzenario, "GHD"] = dfGHD.loc[sSzenario, '3005']
    dfGrid_3005.loc[sSzenario, "EV"] = dfEMob.loc[sSzenario, '3005']
    dfGrid_3005.loc[sSzenario, "WP"] = dfWP.loc[sSzenario, '3005']
    dfGrid_3005.loc[sSzenario, "PV"] = dfPV.loc[sSzenario, '3005']
    dfGrid_3005.loc[sSzenario, "WEA"] = dfWEA.loc[sSzenario, '3005']
    dfGrid_3544.loc[sSzenario, "HH_Load"] = dfHH.loc[sSzenario, '3544']
    dfGrid_3544.loc[sSzenario, "GHD"] = dfGHD.loc[sSzenario, '3544']
    dfGrid_3544.loc[sSzenario, "EV"] = dfEMob.loc[sSzenario, '3544']
    dfGrid_3544.loc[sSzenario, "WP"] = dfWP.loc[sSzenario, '3544']
    dfGrid_3544.loc[sSzenario, "PV"] = dfPV.loc[sSzenario, '3544']
    dfGrid_3544.loc[sSzenario, "WEA"] = dfWEA.loc[sSzenario, '3544']
    dfGrid_3756.loc[sSzenario, "HH_Load"] = dfHH.loc[sSzenario, '3756']
    dfGrid_3756.loc[sSzenario, "GHD"] = dfGHD.loc[sSzenario, '3756']
    dfGrid_3756.loc[sSzenario, "EV"] = dfEMob.loc[sSzenario, '3756']
    dfGrid_3756.loc[sSzenario, "WP"] = dfWP.loc[sSzenario, '3756']
    dfGrid_3756.loc[sSzenario, "PV"] = dfPV.loc[sSzenario, '3756']
    dfGrid_3756.loc[sSzenario, "WEA"] = dfWEA.loc[sSzenario, '3756']

if len(lZeitreihen_name) == 6:
    dfGrid_186.to_csv(
      save_path + '/' + 'Netzscharfe Auswertung/' + "Grid186" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
    )

    dfGrid_245.to_csv(
      save_path + '/' + 'Netzscharfe Auswertung/' + "Grid245" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
    )

    dfGrid_1899.to_csv(
      save_path + '/' + 'Netzscharfe Auswertung/' + "Grid1899" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
    )

    dfGrid_2464.to_csv(
      save_path + '/' + 'Netzscharfe Auswertung/' + "Grid2464" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
    )

    dfGrid_2767.to_csv(
      save_path + '/' + 'Netzscharfe Auswertung/' + "Grid2767" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
    )

    dfGrid_3005.to_csv(
      save_path + '/' + 'Netzscharfe Auswertung/' + "Grid3005" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
    )

    dfGrid_3544.to_csv(
      save_path + '/' + 'Netzscharfe Auswertung/' + "Grid3544" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
    )

    dfGrid_3756.to_csv(
      save_path + '/' + 'Netzscharfe Auswertung/' + "Grid3756" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
    )


if 'ang_Kunden_HH1_nnf_corrected_1h.csv'in lZeitreihen_name:
   dfHH.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "HH" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )
   dfCountHH.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "Anzahl_HH" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )

if 'ang_Kunden_GHD_nnf_1h.csv'in lZeitreihen_name:
   dfGHD.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "GHD" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )
   dfCountGHD.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "Anzahl_GHD" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )

if 'EMob_Zeitreihe_nnf_1h_fixed.csv' in lZeitreihen_name:
   dfEMob.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "EMob" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )
   dfCountEMob.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "Anzahl_EMob" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )

if 'WEA_Zeitreihe_nnf_1h.csv'in lZeitreihen_name:
   dfWEA.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "WEA" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )
   dfCountWEA.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "Anzahl_WEA" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )

if 'WP_Zeitreihe_nnf_fixed.csv'in lZeitreihen_name:
   dfWP.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "WP" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )
   dfCountWP.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "Anzahl_WP" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )

if 'PV_Zeitreihe_nnf_1h.csv'in lZeitreihen_name:
   dfPV.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "PV" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )
   dfCountPV.to_csv(
      save_path + '/' + 'Betriebsmittelscharfe Auswertung/' + "Anzahl_PV" + '.csv',
      sep=';',
      header=True,
      index=True,
      encoding='latin-1',
   )


# import pandas as pd
# a = np.array(lActions)
# b = pd.DataFrame(a)
# b.to_csv(
#     r"E:\Quellen\Studenten\Gajewski\Auswertung Actions DDPG\Vergleich Param Noise und Action Noise/" + "PendulumAN" + '.csv',
#     sep=';',
#     header=True,
#     index=True,
#     encoding='latin-1',
# )