import matplotlib.pyplot as plt
import os
import pandas as pd
import numpy as np

data_path = r"E:\Projekte\Gajewski\Netze MA_2021\Stuendliche Auswertung Netze und Betriebsmittel\Daten/"
save_path = r"E:\Projekte\Gajewski\Netze MA_2021\Stuendliche Auswertung Netze und Betriebsmittel\Auswertung/"

lNetz_id = [186.0, 245.0, 1899.0, 2464.0, 2767.0, 3005.0, 3544.0, 3756.0]
sName_szenario = ['9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']
lIdx_name_szenario_df = [9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0]
sNames = ["HH_Load", "GHD", "EV", "WP", "PV", "WEA"]
iTimes = range(1, 8761, 1)
dic_szenario_to_idx = {'9':0, '10':1, '11':2, '12':3, '13':4, '14':5, '15':6, '16':7, '17':8, '18':8, '19':10, '20':11}
dic_netzid_to_idx = {'186':0, '245':1, '1899':2, '2464':3, '2767':4, '3005':5, '3544':6, '3756':7}

# Data Frames for storing energetic features like peak power/load of each grid
dfHH_1h_186 = pd.read_csv(data_path + 'HH/' + 'HH_1h186.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=0,
                          dtype=np.float32)
dfHH_1h_245 = pd.read_csv(data_path + 'HH/' + 'HH_1h245.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=0,
                          dtype=np.float32)
dfHH_1h_1899 = pd.read_csv(data_path + 'HH/' + 'HH_1h1899.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfHH_1h_2464 = pd.read_csv(data_path + 'HH/' + 'HH_1h2464.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfHH_1h_2767 = pd.read_csv(data_path + 'HH/' + 'HH_1h2767.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfHH_1h_3005 = pd.read_csv(data_path + 'HH/' + 'HH_1h3005.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfHH_1h_3544 = pd.read_csv(data_path + 'HH/' + 'HH_1h3544.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfHH_1h_3756 = pd.read_csv(data_path + 'HH/' + 'HH_1h3756.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)

dfHH_1h = [dfHH_1h_186, dfHH_1h_245, dfHH_1h_1899, dfHH_1h_2464, dfHH_1h_2767, dfHH_1h_3005, dfHH_1h_3544, dfHH_1h_3756]

dfGHD_1h_186 = pd.read_csv(data_path + 'GHD/' + 'GHD_1h186.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfGHD_1h_245 = pd.read_csv(data_path + 'GHD/' + 'GHD_1h245.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfGHD_1h_1899 = pd.read_csv(data_path + 'GHD/' + 'GHD_1h1899.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfGHD_1h_2464 = pd.read_csv(data_path + 'GHD/' + 'GHD_1h2464.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfGHD_1h_2767 = pd.read_csv(data_path + 'GHD/' + 'GHD_1h2767.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfGHD_1h_3005 = pd.read_csv(data_path + 'GHD/' + 'GHD_1h3005.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfGHD_1h_3544 = pd.read_csv(data_path + 'GHD/' + 'GHD_1h3544.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfGHD_1h_3756 = pd.read_csv(data_path + 'GHD/' + 'GHD_1h3756.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)

dfGHD_1h = [dfGHD_1h_186, dfGHD_1h_245, dfGHD_1h_1899, dfGHD_1h_2464, dfGHD_1h_2767, dfGHD_1h_3005, dfGHD_1h_3544,
            dfGHD_1h_3756]

dfEMob_1h_186 = pd.read_csv(data_path + 'EMob/' + 'EMob_1h186.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfEMob_1h_245 = pd.read_csv(data_path + 'EMob/' + 'EMob_1h245.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfEMob_1h_1899 = pd.read_csv(data_path + 'EMob/' + 'EMob_1h1899.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                             skiprows=0,
                             dtype=np.float32)
dfEMob_1h_2464 = pd.read_csv(data_path + 'EMob/' + 'EMob_1h2464.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                             skiprows=0,
                             dtype=np.float32)
dfEMob_1h_2767 = pd.read_csv(data_path + 'EMob/' + 'EMob_1h2767.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                             skiprows=0,
                             dtype=np.float32)
dfEMob_1h_3005 = pd.read_csv(data_path + 'EMob/' + 'EMob_1h3005.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                             skiprows=0,
                             dtype=np.float32)
dfEMob_1h_3544 = pd.read_csv(data_path + 'EMob/' + 'EMob_1h3544.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                             skiprows=0,
                             dtype=np.float32)
dfEMob_1h_3756 = pd.read_csv(data_path + 'EMob/' + 'EMob_1h3756.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                             skiprows=0,
                             dtype=np.float32)

dfEMob_1h = [dfEMob_1h_186, dfEMob_1h_245, dfEMob_1h_1899, dfEMob_1h_2464, dfEMob_1h_2767, dfEMob_1h_3005,
             dfEMob_1h_3544, dfEMob_1h_3756]

dfPV_1h_186 = pd.read_csv(data_path + 'PV/' + 'PV_1h186.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=0,
                          dtype=np.float32)
dfPV_1h_245 = pd.read_csv(data_path + 'PV/' + 'PV_1h245.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=0,
                          dtype=np.float32)
dfPV_1h_1899 = pd.read_csv(data_path + 'PV/' + 'PV_1h1899.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfPV_1h_2464 = pd.read_csv(data_path + 'PV/' + 'PV_1h2464.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfPV_1h_2767 = pd.read_csv(data_path + 'PV/' + 'PV_1h2767.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfPV_1h_3005 = pd.read_csv(data_path + 'PV/' + 'PV_1h3005.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfPV_1h_3544 = pd.read_csv(data_path + 'PV/' + 'PV_1h3544.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfPV_1h_3756 = pd.read_csv(data_path + 'PV/' + 'PV_1h3756.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)

dfPV_1h = [dfPV_1h_186, dfPV_1h_245, dfPV_1h_1899, dfPV_1h_2464, dfPV_1h_2767, dfPV_1h_3005, dfPV_1h_3544, dfPV_1h_3756]

dfWP_1h_186 = pd.read_csv(data_path + 'WP/' + 'WP_1h186.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=0,
                          dtype=np.float32)
dfWP_1h_245 =  pd.read_csv(data_path + 'WP/' + 'WP_1h245.csv', sep=';', encoding='latin-1', low_memory=False,
                                        index_col=0, skiprows=0,
                                        dtype=np.float32)
dfWP_1h_1899 =  pd.read_csv(data_path + 'WP/' + 'WP_1h1899.csv', sep=';', encoding='latin-1', low_memory=False,
                                         index_col=0, skiprows=0,
                                         dtype=np.float32)
dfWP_1h_2464 = pd.read_csv(data_path + 'WP/' + 'WP_1h2464.csv', sep=';', encoding='latin-1', low_memory=False,
                                         index_col=0, skiprows=0,
                                         dtype=np.float32)
dfWP_1h_2767 = pd.read_csv(data_path + 'WP/' + 'WP_1h2767.csv', sep=';', encoding='latin-1', low_memory=False,
                                         index_col=0, skiprows=0,
                                         dtype=np.float32)
dfWP_1h_3005 = pd.read_csv(data_path + 'WP/' + 'WP_1h3005.csv', sep=';', encoding='latin-1', low_memory=False,
                                         index_col=0, skiprows=0,
                                         dtype=np.float32)
dfWP_1h_3544 = pd.read_csv(data_path + 'WP/' + 'WP_1h3544.csv', sep=';', encoding='latin-1', low_memory=False,
                                         index_col=0, skiprows=0,
                                         dtype=np.float32)
dfWP_1h_3756 = pd.read_csv(data_path + 'WP/' + 'WP_1h3756.csv', sep=';', encoding='latin-1', low_memory=False,
                                         index_col=0, skiprows=0,
                                         dtype=np.float32)

dfWP_1h = [dfWP_1h_186, dfWP_1h_245, dfWP_1h_1899, dfWP_1h_2464, dfWP_1h_2767, dfWP_1h_3005, dfWP_1h_3544, dfWP_1h_3756]

dfWEA_1h_186 = pd.read_csv(data_path + 'WEA/' + 'WEA_1h186.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfWEA_1h_245 = pd.read_csv(data_path + 'WEA/' + 'WEA_1h245.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                           skiprows=0,
                           dtype=np.float32)
dfWEA_1h_1899 = pd.read_csv(data_path + 'WEA/' + 'WEA_1h1899.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfWEA_1h_2464 = pd.read_csv(data_path + 'WEA/' + 'WEA_1h2464.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfWEA_1h_2767 = pd.read_csv(data_path + 'WEA/' + 'WEA_1h2767.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfWEA_1h_3005 = pd.read_csv(data_path + 'WEA/' + 'WEA_1h3005.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfWEA_1h_3544 = pd.read_csv(data_path + 'WEA/' + 'WEA_1h3544.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)
dfWEA_1h_3756 = pd.read_csv(data_path + 'WEA/' + 'WEA_1h3756.csv', sep=';', encoding='latin-1', low_memory=False, index_col=0,
                            skiprows=0,
                            dtype=np.float32)

dfWEA_1h = [dfWEA_1h_186, dfWEA_1h_245, dfWEA_1h_1899, dfWEA_1h_2464, dfWEA_1h_2767, dfWEA_1h_3005, dfWEA_1h_3544,
            dfWEA_1h_3756]

column = ['Pmax_load', 'Pmax_load_Stunde', 'Pmax_sektor', 'Pmax_sektor_Stunde', 'Pmax_gen', 'Pmax_gen_Stunde',
          'Pmax_ResiLoad', 'Pmax_ResiLoad_Stunde', 'Pmax_Erzeugungsueberschuss', 'Pmax_Erzeugungsueberschuss_Stunde', 'Durchschnittliche_Residuallast']

df_hourly_informations_186 = pd.DataFrame(np.zeros((len(sName_szenario), len(column))), columns=column)
df_hourly_informations_186 = df_hourly_informations_186.set_axis(lIdx_name_szenario_df, axis='index')
df_hourly_informations_245 = pd.DataFrame(np.zeros((len(sName_szenario), len(column))), columns=column)
df_hourly_informations_245 = df_hourly_informations_245.set_axis(lIdx_name_szenario_df, axis='index')
df_hourly_informations_1899 = pd.DataFrame(np.zeros((len(sName_szenario), len(column))), columns=column)
df_hourly_informations_1899 = df_hourly_informations_1899.set_axis(lIdx_name_szenario_df, axis='index')
df_hourly_informations_2464 = pd.DataFrame(np.zeros((len(sName_szenario), len(column))), columns=column)
df_hourly_informations_2464 = df_hourly_informations_2464.set_axis(lIdx_name_szenario_df, axis='index')
df_hourly_informations_2767 = pd.DataFrame(np.zeros((len(sName_szenario), len(column))), columns=column)
df_hourly_informations_2767 = df_hourly_informations_2767.set_axis(lIdx_name_szenario_df, axis='index')
df_hourly_informations_3005 = pd.DataFrame(np.zeros((len(sName_szenario), len(column))), columns=column)
df_hourly_informations_3005 = df_hourly_informations_3005.set_axis(lIdx_name_szenario_df, axis='index')
df_hourly_informations_3544 = pd.DataFrame(np.zeros((len(sName_szenario), len(column))), columns=column)
df_hourly_informations_3544 = df_hourly_informations_3544.set_axis(lIdx_name_szenario_df, axis='index')
df_hourly_informations_3756 = pd.DataFrame(np.zeros((len(sName_szenario), len(column))), columns=column)
df_hourly_informations_3756 = df_hourly_informations_3756.set_axis(lIdx_name_szenario_df, axis='index')

df_hourly_informations = [df_hourly_informations_186, df_hourly_informations_245, df_hourly_informations_1899, df_hourly_informations_2464, df_hourly_informations_2767, \
                          df_hourly_informations_3005, df_hourly_informations_3544, df_hourly_informations_3756]

for iCounter in range(len(lNetz_id)):
   dfLoad = dfHH_1h[iCounter] + dfGHD_1h[iCounter] + dfEMob_1h[iCounter] + dfWP_1h[iCounter]
   dfSektor = dfEMob_1h[iCounter] + dfWP_1h[iCounter]
   dfEE = dfPV_1h[iCounter] + dfWEA_1h[iCounter]

   dfMaxResiLoad = dfLoad.subtract(dfEE)
   dfMaxResiGen = dfEE.subtract(dfLoad)

   for fSzenario in lIdx_name_szenario_df:
      row_load = dfLoad.loc[fSzenario, :]
      row_Sektor = dfSektor.loc[fSzenario, :]
      row_EE = dfEE.loc[fSzenario, :]
      row_resi_load = dfMaxResiLoad.loc[fSzenario, :]
      row_resi_gen = dfMaxResiGen.loc[fSzenario, :]

      df_hourly_informations[iCounter].loc[fSzenario, 'Pmax_load'] = row_load.max()
      df_hourly_informations[iCounter].loc[fSzenario, 'Pmax_load_Stunde'] = row_load.idxmax()
      df_hourly_informations[iCounter].loc[fSzenario, 'Pmax_sektor'] = row_Sektor.max()
      df_hourly_informations[iCounter].loc[fSzenario, 'Pmax_sektor_Stunde'] = row_Sektor.idxmax()
      df_hourly_informations[iCounter].loc[fSzenario, 'Pmax_gen'] = row_EE.max()
      df_hourly_informations[iCounter].loc[fSzenario, 'Pmax_gen_Stunde'] = row_EE.idxmax()
      df_hourly_informations[iCounter].loc[fSzenario, 'Pmax_ResiLoad'] = row_resi_load.max()
      df_hourly_informations[iCounter].loc[fSzenario, 'Pmax_ResiLoad_Stunde'] = row_resi_load.idxmax()
      df_hourly_informations[iCounter].loc[fSzenario, 'Pmax_Erzeugungsueberschuss'] = row_resi_gen.max()
      df_hourly_informations[iCounter].loc[fSzenario, 'Pmax_Erzeugungsueberschuss_Stunde'] = row_resi_gen.idxmax()
      df_hourly_informations[iCounter].loc[fSzenario, 'Durchschnittliche_Residuallast'] = sum(row_resi_load)/8760

i = 0
for df_hourly_information in df_hourly_informations:
  df_hourly_information.to_csv(
     save_path + "Netzinfos" + "/" + "Netzinformationen" + str(lNetz_id[i]) + '.csv',
     sep=';',
     header=True,
     index=True,
     encoding='latin-1',
  )
  i = i+1