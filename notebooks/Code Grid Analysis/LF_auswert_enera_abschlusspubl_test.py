import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tkinter.filedialog
from pylab import rcParams
from tqdm import tqdm
import os
import re
import pprint

rcParams['figure.figsize'] = 30, 20

from IPython.display import display


def check_szenario(i):
 switch = {
  9: ["WC_2030"],
  10: ["K80_2030"],
  11: ["K95_2030"],
  12: ["BC_2030"],
  13: ["WC_2040"],
  14: ["K80_2040"],
  15: ["K95_2040"],
  16: ["BC_2040"],
  17: ["WC_2050"],
  18: ["K80_2050"],
  19: ["K95_2050"],
  20: ["BC_2050"]
 }
 return switch.get(i, 'Ungültige Szenario ID!')


def choose_folder():
 root = tkinter.Tk()
 folder = tkinter.filedialog.askdirectory(parent=root, title='Choose folder')

 list_filenames_lf = []
 list_filenames_cc = []
 list_filenames_fzg = []

 # suche alle Dateien Ausgabe_Zweige
 for r, dirs, files in os.walk(folder):
  for file in files:
   if file.endswith('Ausgabe_Zweige.csv'):
    list_filenames_lf.append(os.path.join(r, file))

 # suche alle Dateien Cluster Count
 for r, dirs, files in os.walk(folder):
  for file in files:
   if file.endswith('cluster_nnf_count.csv'):
    list_filenames_cc.append(os.path.join(r, file))

    # suche alle Dateien Flussrichtung für Korrektur Ausgabe Zweige
 for r, dirs, files in os.walk(folder):
  for file in files:
   if file.endswith('Flussrichtung_Zweige_Grundfall.csv'):
    list_filenames_fzg.append(os.path.join(r, file))

 root.destroy()
 return list_filenames_lf, list_filenames_cc, list_filenames_fzg


def get_results(netz_id, list_filenames_lf, list_filenames_cc, list_filenames_fzg):
 # Steuerparameter Auslastung oder Leistungsfluss
 # str_input = 'Ausl. N-0 vor Opt.'
 str_input = 'P N-0 vor Opt.'

 # filter Ausgabe_Zweige Pfade nach netz_id
 df_files = pd.DataFrame(list_filenames_lf)
 df_files = df_files[df_files[0].str.contains(str(netz_id) + "\\\\" + str(netz_id))]

 # Nur Ausgabezweige von Lastflussoptimierung (ohne Marktsimulation)
 df_files = df_files[df_files[0].str.contains("Lastflussoptimierung")]

 # filter Cluster Counter nach netz_id
 df_cc_path = pd.DataFrame(list_filenames_cc)
 df_cc_path = df_cc_path[df_cc_path[0].str.contains(str(netz_id) + '\\\\' + str(netz_id))]

 # filter Flussrichtung_Zweige_Grundfall nach netz_id
 df_fzg_path = pd.DataFrame(list_filenames_fzg)
 df_fzg_path = df_fzg_path[df_fzg_path[0].str.contains(str(netz_id) + '\\\\' + str(netz_id))]

 datalist = []

 # Loop über alle Szenarien (netz_id) mit Ausgabe Zweige
 for sz in df_files[0]:
  df = pd.read_csv(sz,
                   encoding='latin1',
                   delimiter=';',
                   skiprows=[0, 2, 3, 4, 5],
                   usecols=['Schlüssel', str_input, 'NNF']
                   )
  # Sortierung der NNF notwendig da Cluster Counter sortiert
  df = df.sort_values('NNF', axis=0)
  df.reset_index(inplace=True, drop=True)

  cols = [str_input]
  df[cols] = df[cols].apply(pd.to_numeric, errors='coerce')

  # erzeuge Datenliste mit allen Lastflussergebnissen pro Szenario
  datalist.append(
   df[df['Schlüssel'].str.startswith('SO-·Standort_0/T2-Trafo_HSMS')][['Schlüssel', str_input, 'NNF']].copy())
  del df

 number_par_trafos = []
 for data in datalist:
  number_par_trafos.append(len(data.groupby('Schlüssel').count()))

 df_res = pd.DataFrame()
 df_cc = pd.DataFrame()
 df_fzg = pd.DataFrame()

 for idx, data in enumerate(datalist):
  df = data[data['Schlüssel'] == "SO-·Standort_0/T2-Trafo_HSMS"][str_input] * number_par_trafos[idx]
  df.reset_index(inplace=True, drop=True)

  # find szenario name:
  seq = re.findall('\d{3,4}_\d{1,2}\\\\', df_files.iloc[idx][0])
  szenario_id = re.findall('\d{1,2}', (re.findall('_\d{1,2}', seq[0]))[0])[0]
  szenario_name = check_szenario(int(szenario_id))[0]

  # get cluster count file
  seq_cc = re.findall('\d{3,4}_\d{1,2}', seq[0])
  path = str(df_cc_path[df_cc_path[0].str.contains(seq_cc[0])].iloc[0][0])

  df_cc_raw = pd.read_csv(path,
                          encoding='latin1',
                          delimiter=';',
                          usecols=['Count']
                          )

  # find correkt FZG file
  seq_fzg = re.findall('\d{3,4}_\d{1,2}', seq[0])

  if len(df_fzg_path[df_fzg_path[0].str.contains(seq_fzg[0])]) >= 2:
   path_fzg = str(df_fzg_path[df_fzg_path[0].str.contains(seq_fzg[0])].iloc[1][0])
  else:
   path_fzg = str(df_fzg_path[df_fzg_path[0].str.contains(seq_fzg[0])].iloc[0][0])

  df_fzg_raw = pd.read_csv(path_fzg,
                           encoding='latin1',
                           delimiter=';',
                           usecols=['LZ', 'SO-·Standort_0/T2-Trafo_HSMS']
                           )

  df_fzg_raw = df_fzg_raw.sort_values('LZ', axis=0)
  df_fzg_raw.reset_index(inplace=True, drop=True)

  # fill result DataFrame and multiply with counter and flow direction
  df_res[szenario_name] = df
  df_cc[szenario_name] = df_cc_raw['Count']
  df_fzg[szenario_name] = df_fzg_raw['SO-·Standort_0/T2-Trafo_HSMS']
  del szenario_name

 df_result = df_res.mul(df_fzg)

 df_final = pd.DataFrame()
 for sz_no in df_result.columns:
  nnf_list = []
  for i in range(0, 50):
   dp_list = [1] * df_cc.at[i, sz_no]
   nnf_list.extend([ii * df_result.at[i, sz_no] for ii in dp_list])
  if len(nnf_list) == 8760:
   df_final[sz_no] = pd.Series(nnf_list)
  else:
   print('ERROR, PPP ist ein Doener')

 return df_final


pd.options.display.max_rows = 5000
# netz_liste = [2464, 2767, 1899, 186, 3756, 3544, 3005, 245]
netz_liste = [2464, 2767, 1899, 186, 3756, 3544, 3005, 245]
list_filenames_lf, list_filenames_cc, list_filenames_fzg = choose_folder()

for idx, netz_id in enumerate(netz_liste):

 if idx >= 1:
  res = get_results(netz_id, list_filenames_lf, list_filenames_cc, list_filenames_fzg)
  # if idx>4:
  #    idy = idx + 2
  # else:
  #    idy = idx + 1
  res.columns = pd.MultiIndex.from_product([[str(idx + 1)], res.columns])
  df_final = pd.concat([df_final, res], axis=1)
 else:
  res = get_results(netz_id, list_filenames_lf, list_filenames_cc, list_filenames_fzg)
  res.columns = pd.MultiIndex.from_product([[str(idx + 1)], res.columns])
  df_final = res