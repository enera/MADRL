import pandas as pd
import numpy as np
import tkinter as tk
from tkinter import filedialog
from tkinter import simpledialog
from tqdm import tqdm
import os
import re

def browse_button():
   # Allow user to select a directory
   root = tk.Tk()
   directory_path = filedialog.askdirectory(title="Bitte FGH Datenordner auswaehlen!")
   root.withdraw()
   return directory_path

# hier alle Zeitreihen eintragen die für Szenarien gemerged werden sollen:
lNetz_id = ['186', '245', '1899', '2464', '2767', '3005', '3544', '3756']
sName_szenario = ['9', '12', '16', '20']

# Datenordner auswählen und Szenario_ID abfragen:
#path = browse_button()
data_path = r"E:\Projekte\enera\Netze AP13"
save_path = r"E:\Projekte\Gajewski\Netze MA_2021\Auslastung der Netze/"
list_filenames_lf = []
list_filenames_cc = []
list_filenames_fzg = []

# suche alle Dateien Ausgabe_Zweige
for r, dirs, files in os.walk(data_path):
    for file in files:
        if file.endswith('Ausgabe_Zweige.csv') and len(file) == len('Ausgabe_Zweige.csv') and len(re.findall(r"Sensis", r)):
            list_filenames_lf.append(os.path.join(r, file))
        elif file.endswith('Flussrichtung_Zweige_Grundfall.csv') and len(file) == len('Flussrichtung_Zweige_Grundfall.csv') and len(re.findall(r"Sensis", r)):
            list_filenames_fzg.append(os.path.join(r, file))
        elif file.endswith('cluster_nnf_count.csv') and len(file) == len('cluster_nnf_count.csv') and len(re.findall(r"mm1h", r)):
            list_filenames_cc.append(os.path.join(r, file))

# Get for each szenario and each gird the the respective data which inclues the capacity utilization,
# the cluster counter, and the sign of the flow direction
for Netz_id in lNetz_id:
    # filter Ausgabe_Zweige Pfade nach netz_id
    df_files = pd.DataFrame(list_filenames_lf)
    df_files = df_files.append(pd.DataFrame(list_filenames_fzg))
    df_files = df_files.append(pd.DataFrame(list_filenames_cc))
    df_files = df_files[df_files[0].str.contains(str(Netz_id) + "\\\\" + str(Netz_id))]
    column = ['Pos. Congestion_Power [MW]', 'Neg. Congestion_Power [MW]', 'Abs. Congestion_Power [MW]']
    dfCongestionPower_res = pd.DataFrame(np.zeros((len(sName_szenario), len(column))), columns=column)
    dfCongestionPower_res = dfCongestionPower_res.set_axis(sName_szenario, axis='index')

    for Name_szenario in sName_szenario:
        # Get for each szenario the relevant loading, flow_direction and NNF counter CSVs

        # First get all the paths
        df_files_helper = df_files[df_files[0].str.contains(str(Netz_id) + '_' + str(Name_szenario))]
        loading_path = df_files_helper[df_files_helper[0].str.contains('Ergebnis_Lastflussoptimierung')]
        cluster_path = df_files_helper[df_files_helper[0].str.contains('mm1h' + '\\\\' + 'cluster_fixed')]
        if len(cluster_path) == 0:
            cluster_path = df_files_helper[df_files_helper[0].str.contains('mm1h_neu' + '\\\\' + 'cluster_fixed')]
        direction_path = df_files_helper[df_files_helper[0].str.contains('Ergebnis_Marktsimulation')]
        direction_path = direction_path[direction_path[0].str.contains('Flussrichtung')]

        # Get CSVs from path
        dfLoading_lines = pd.read_csv(str(loading_path.iat[0,0]),
                                 encoding='latin1',
                                 delimiter=';',
                                 skiprows = [0,2,3,4,5],
                                 usecols= ['Schlüssel','P N-0 vor Opt.', 'Ausl. N-0 vor Opt.', 'NNF']
                                )
        #Sortierung der NNF notwendig da Cluster Counter sortiert
        dfLoading_lines = dfLoading_lines.sort_values('NNF',axis=0)
        dfLoading_lines.reset_index(inplace=True, drop=True)

        cols = ['P N-0 vor Opt.', 'Ausl. N-0 vor Opt.']
        dfLoading_lines[cols] = dfLoading_lines[cols].apply(pd.to_numeric, errors='coerce')

        dfCluster_count = pd.read_csv(str(cluster_path.iat[0,0]), encoding='latin1', delimiter=';',usecols= ['Count'])
        dfFlow_direction = pd.read_csv(str(direction_path.iat[0,0]), encoding='latin1', delimiter=';')
        dfFlow_direction = dfFlow_direction.sort_values('LZ', axis=0)
        dfFlow_direction.reset_index(inplace=True, drop=True)
        dfFlow_direction.drop(['Sonderschaltung', 'Status'], inplace = True, axis = 1)
        dfFlow_direction = dfFlow_direction.set_index('LZ')

        # Setting up the data frames for the congestion power
        NNFs = np.unique(dfLoading_lines.loc[:, 'NNF'].to_numpy())
        dfCongestionPower = pd.DataFrame(np.zeros((len(NNFs), len(column))), columns=column)
        dfCongestionPower = dfCongestionPower.set_axis(NNFs, axis='index')

        # Initializing the resulting dataframe, which contains the line loading mulitplied by the flow direction and the number of NNFs
        dfLoading_lines_flow_direction = pd.DataFrame()

        iCounter = 0
        for NNF in NNFs:
            dfLoading_values_NNF = dfLoading_lines.loc[dfLoading_lines.index[dfLoading_lines.loc[:, 'NNF'] == NNF]]
            dfLoading_values_NNF = dfLoading_values_NNF.set_index('Schlüssel')
            dfLoading_values_NNF = dfLoading_values_NNF.sort_index(axis = 0)
            dfLoading_values_NNF['P N-0 vor Opt.'] = dfLoading_values_NNF['P N-0 vor Opt.'].mul(dfFlow_direction.loc[NNF,:])
            dfLoading_values_NNF['P N-0 vor Opt.'] = dfLoading_values_NNF['P N-0 vor Opt.'].mul(dfCluster_count.iloc[iCounter].to_numpy()[0])
            # dfLoading_values_NNF['Gew. Ausl. N-0 vor Opt.'] = dfLoading_values_NNF['Ausl. N-0 vor Opt.'].mul(dfCluster_count.iloc[iCounter].to_numpy()[0])
            dfNNF_CongestionPower = dfLoading_values_NNF.loc[dfLoading_values_NNF.index[dfLoading_values_NNF['Ausl. N-0 vor Opt.'] > 100]]

            if len(dfNNF_CongestionPower) > 0:
                dfNNF_CongestionPower_pos = dfNNF_CongestionPower.loc[dfNNF_CongestionPower.index[dfNNF_CongestionPower.loc[:, 'P N-0 vor Opt.'] > 0]].loc[:, 'P N-0 vor Opt.'] * (1 - 100 / dfNNF_CongestionPower['Ausl. N-0 vor Opt.'])
                dfNNF_CongestionPower_pos = dfNNF_CongestionPower_pos.sum()
                dfNNF_CongestionPower_neg = dfNNF_CongestionPower.loc[dfNNF_CongestionPower.index[dfNNF_CongestionPower.loc[:, 'P N-0 vor Opt.'] < 0]].loc[:, 'P N-0 vor Opt.'] * (1 - 100 / dfNNF_CongestionPower['Ausl. N-0 vor Opt.'])
                dfNNF_CongestionPower_neg = dfNNF_CongestionPower_neg.sum()
                dfCongestionPower.loc[NNF, 'Pos. Congestion_Power [MW]'] = dfNNF_CongestionPower_pos
                dfCongestionPower.loc[NNF, 'Neg. Congestion_Power [MW]'] = dfNNF_CongestionPower_neg
            else:
                dfCongestionPower.loc[NNF, ['Pos. Congestion_Power [MW]', 'Neg. Congestion_Power [MW]']] = 0
            dfCongestionPower.loc[NNF, 'Abs. Congestion_Power [MW]'] = dfCongestionPower.loc[NNF, 'Pos. Congestion_Power [MW]'] - dfCongestionPower.loc[NNF, 'Neg. Congestion_Power [MW]']

            dfLoading_lines_flow_direction = dfLoading_lines_flow_direction.append(dfLoading_values_NNF)
            iCounter = iCounter + 1
        dfCongestionPower_res.loc[Name_szenario, :] = dfCongestionPower.sum()

        # Calculate the weighted loading of the lines and transformers
        # TODO: In ein Excel Sheet schreiben
        #dfLoading_values_res = dfLoading_lines.loc[dfLoading_lines.index[dfLoading_lines.loc[:, 'NNF'] == NNFs[0]]]
        #dfLoading_values_res.drop(['NNF', 'P N-0 vor Opt.', 'Ausl. N-0 vor Opt.'], inplace=True, axis=1)
        #dfLoading_values_res[Name_szenario] = 0
        #dfLoading_values_res = dfLoading_values_res.set_index('Schlüssel')
        #dfLoading_values_res = dfLoading_values_res.sort_index(axis=0)

        #dfLoading_lines_flow_direction_sorted = dfLoading_lines_flow_direction.sort_index(axis=0)
        #dfLoading_lines_flow_direction_sorted = dfLoading_lines_flow_direction_sorted.reset_index()
        #dfLoading_values_res[Name_szenario] = dfLoading_lines_flow_direction_sorted['Gew. Ausl. N-0 vor Opt.'].\
        #    groupby(dfLoading_lines_flow_direction_sorted.index // len(NNFs)).sum().to_numpy()/8760

        dfLoading_lines_flow_direction.to_csv(
            save_path + '/' + str(Netz_id) + '/Loading'  + '_' +str(Name_szenario) + '.csv',
            sep=';',
            header=True,
            index=True,
            encoding='latin-1',
        )
        #dfLoading_values_res.to_csv(
        #    save_path + '/' + str(Netz_id) + '/Weighted_loading'  + '_' +str(Name_szenario) + '.csv',
        #    sep=';',
        #    header=True,
        #    index=True,
        #    encoding='latin-1',
        #)
    dfCongestionPower_res.to_csv(
        save_path + '/' + str(Netz_id) + '/Congestion_Power' + '.csv',
        sep=';',
        header=True,
        index=True,
        encoding='latin-1',
    )
