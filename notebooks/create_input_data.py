import pandas as pd
import numpy as np
import tkinter as tk
from tkinter import filedialog
from tkinter import simpledialog
from tqdm import tqdm
import os
import re


# get the expansion path of the scenario, excluded 9 since this is already included for all scenarios
def ausbaupfad(i):
    switch = {
        10: [10],
        11: [11],
        12: [12],
        13: [13],
        14: [10, 14],
        15: [11, 15],
        16: [12, 16],
        17: [13, 17],
        18: [14, 18],
        19: [15, 19],
        20: [16, 20]
    }
    return switch.get(i, 'Ungültige Szenario ID!')


# hier alle Zeitreihen eintragen die für Szenarien gemerged werden sollen:
lZeitreihen_name = [
    'ang_Kunden_GHD_nnf_1h.csv',
    'ang_Kunden_HH1_nnf_corrected_1h.csv',
    'EMob_Zeitreihe_nnf_1h_fixed.csv',
    'PV_Zeitreihe_nnf_1h.csv',
    'WEA_Zeitreihe_nnf_1h.csv',
    'WP_Zeitreihe_nnf_fixed.csv'
]
lNetz_id = ['186', '245', '1899', '2464', '2767', '3005', '3544', '3756']
sName_szenario = ['9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20']
sNames = ["HH_Load", "GHD", "EV", "WP", "PV", "WEA"]

# Datenordner auswählen und Szenario_ID abfragen:
# path = browse_button()
path = r"E:\Projekte\enera\Netze AP13\186\186_19\mm1h\cluster_fixed/"
sensi_path = r"E:\Projekte\enera\Netze AP13\186\186_19\Sensis/"
save_path = r"E:\Quellen\Studenten\Gajewski\inputs_186/"

lHH = []
lGHD = []
lEmob = []
lWP = []
lPV = []
lWEA = []

for r, dirs, files in os.walk(path):
    for file in files:
        if len(re.findall(r"ang_Kunden_HH.{1,4}_nnf_corrected_1h", file)) > 0:
            lHH.append(file)
            if len(lHH) == 1:
                dfHH = pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0)
            else:
                dfHH = pd.concat([dfHH, pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0)], axis=1)
                # dfHH.index = dfHH.index.values.astype(int)

        # if len(re.findall(r"ang_Kunden_GHD_nnf_1h_mm", file)) > 0:
        #     lGHD.append(file)
        #     if len(lGHD) == 1:
        #         dfGHD = pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0)
        #         dfGHD.index[0:3 ]
        #         dfGHD.index = dfGHD.index.values.astype(int)
        #     else:
        #         dfGHD = pd.concat([dfGHD,
        #                            pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0,
        #                                        skiprows=3,
        #                                        dtype=np.float32)], axis=1)
        #         dfGHD.index = dfGHD.index.values.astype(int)
        #
        # if len(re.findall(r"EMob_Zeitreihe_nnf_1h_fixed_mm", file)) > 0:
        #     lEmob.append(file)
        #     if len(lEmob) == 1:
        #         dfEMob = pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0,
        #                              skiprows=3,
        #                              dtype=np.float32)
        #         dfEMob.index = dfEMob.index.values.astype(int)
        #     else:
        #         dfEMob = pd.concat([dfEMob,
        #                             pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0,
        #                                         skiprows=3,
        #                                         dtype=np.float32)], axis=1)
        #         dfEMob.index = dfEMob.index.values.astype(int)
        # if len(re.findall(r"WP_Zeitreihe_nnf_fixed_mm", file)) > 0:
        #     lWP.append(file)
        #     if len(lWP) == 1:
        #         dfWP = pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=3,
        #                            dtype=np.float32)
        #         dfWP.index = dfWP.index.values.astype(int)
        #     else:
        #         dfWP = pd.concat([dfWP,
        #                           pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0,
        #                                       skiprows=3,
        #                                       dtype=np.float32)], axis=1)
        #         dfWP.index = dfWP.index.values.astype(int)
        # if len(re.findall(r"PV_Zeitreihe_nnf_1h_mm", file)) > 0:
        #     lPV.append(file)
        #     if len(lPV) == 1:
        #         dfPV = pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=3,
        #                            dtype=np.float32)
        #         dfPV.index = dfPV.index.values.astype(int)
        #     else:
        #         dfPV = pd.concat([dfPV,
        #                           pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0,
        #                                       skiprows=3,
        #                                       dtype=np.float32)], axis=1)
        #         dfPV.index = dfPV.index.values.astype(int)
        # if len(re.findall(r"WEA_Zeitreihe_nnf_1h_mm", file)) > 0:
        #     lWEA.append(file)
        #     if len(lWEA) == 1:
        #         dfWEA = pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0, skiprows=3,
        #                             dtype=np.float32)
        #         dfWEA.index = dfWEA.index.values.astype(int)
        #     else:
        #         dfWEA = pd.concat([dfWEA,
        #                            pd.read_csv(path + file, sep=';', encoding='latin-1', low_memory=False, index_col=0,
        #                                        skiprows=3,
        #                                        dtype=np.float32)], axis=1)
        #         dfWEA.index = dfWEA.index.values.astype(int)
        #
        if file == "cluster_nnf_assignment.csv":
            dfClusterAssignment = pd.read_csv(path + "res/" + file, sep=';', encoding='latin-1', low_memory=False,
                                              index_col=0,
                                              skiprows=0, dtype=np.float32)
            dfClusterAssignment.index = dfClusterAssignment.index.values.astype(int)
            dfClusterAssignment = dfClusterAssignment.astype(int)
            dictClAs = dict(zip(dfClusterAssignment.index, dfClusterAssignment.values))

        if file == "cluster_nnf_count.csv":
            dfNNF_Count = pd.read_csv(path + "res/" + file, sep=';', encoding='latin-1', low_memory=False, index_col=0,
                                      skiprows=0, dtype=np.float32)
            dfNNF_Count.index = dfNNF_Count.index.values.astype(int)
            dfNNF_Count = dfNNF_Count.astype(int)
            dictNNF = dict(zip(dfNNF_Count.index, dfNNF_Count.values))

dfHH.index.values[0:3] = '0'
dfHH.index = dfHH.index.values.astype(int)

dfHH_final = pd.DataFrame(np.zeros((len(dfClusterAssignment.index), len(dfHH.columns))), columns = dfHH.columns)
dfHH_final.index = dfClusterAssignment.index

# dfGHD_final = pd.DataFrame(np.zeros((len(dfClusterAssignment.index), len(dfGHD.columns))), columns = dfGHD.columns)
# dfGHD_final.index = dfClusterAssignment.index
#
# dfEMob_final = pd.DataFrame(np.zeros((len(dfClusterAssignment.index), len(dfEMob.columns))), columns = dfEMob.columns)
# dfEMob_final.index = dfClusterAssignment.index
#
# dfWP_final = pd.DataFrame(np.zeros((len(dfClusterAssignment.index), len(dfWP.columns))), columns = dfWP.columns)
# dfWP_final.index = dfClusterAssignment.index
#
# dfPV_final = pd.DataFrame(np.zeros((len(dfClusterAssignment.index), len(dfPV.columns))), columns = dfPV.columns)
# dfPV_final.index = dfClusterAssignment.index
#
# dfWEA_final = pd.DataFrame(np.zeros((len(dfClusterAssignment.index), len(dfWEA.columns))), columns = dfWEA.columns)
# dfWEA_final.index = dfClusterAssignment.index

for index in dfClusterAssignment.index:
    nnf = dictClAs[index]

    dfHH_final.loc[index, :] = dfHH.loc[nnf[0], :]
    print(index)


    # dfGHD_final.loc[index, :] = dfGHD.loc[nnf[0], :]
    # dfEMob_final.loc[index, :] = dfEMob.loc[nnf[0], :]
    # dfWP_final.loc[index, :] = dfWP.loc[nnf[0], :]
    # dfPV_final.loc[index, :] = dfPV.loc[nnf[0], :]
    # dfWEA_final.loc[index, :] = dfWEA.loc[nnf[0], :]

dfHH_final.to_pickle(
    save_path + "ang_Kunden_HH1_nnf_corrected_1h.pkl",
)

# dfGHD_final.to_pickle(
#     save_path + "ang_Kunden_GHD_nnf_1h.pkl",
# )
#
# dfEMob_final.to_pickle(
#     save_path + "EMob_Zeitreihe_nnf_1h.pkl",
# )
#
# dfWP_final.to_pickle(
#     save_path + "WP_Zeitreihe_nnf.pkl",
# )
#
# dfPV_final.to_pickle(
#     save_path + "PV_Zeitreihe_nnf_1h.pkl",
# )
#
# dfWEA_final.to_pickle(
#     save_path + "WEA_nnf_1h.pkl",
# )

dfHH_final.to_csv(
    save_path + "ang_Kunden_HH1_nnf_corrected_1h.csv",
    sep=';',
    header=True,
    index=True,
    encoding='latin-1',
)
#
# dfGHD_final.to_csv(
#     save_path + "ang_Kunden_GHD_nnf_1h.csv",
#     sep=';',
#     header=True,
#     index=True,
#     encoding='latin-1',
# )
#
# dfEMob_final.to_csv(
#     save_path + "EMob_Zeitreihe_nnf_1h.csv",
#     sep=';',
#     header=True,
#     index=True,
#     encoding='latin-1',
# )
#
# dfWP_final.to_csv(
#     save_path + "WP_Zeitreihe_nnf.csv",
#     sep=';',
#     header=True,
#     index=True,
#     encoding='latin-1',
# )
#
# dfPV_final.to_csv(
#     save_path + "PV_Zeitreihe_nnf_1h.csv",
#     sep=';',
#     header=True,
#     index=True,
#     encoding='latin-1',
# )
#
# dfWEA_final.to_csv(
#     save_path + "WEA_nnf_1h.csv",
#     sep=';',
#     header=True,
#     index=True,
#     encoding='latin-1',
# )

# dfAusgabeZweige = pd.read_csv(
#     sensi_path + "Ergebnis_Marktsimulation/" + "Ausgabe_Zweige.csv",
#     encoding = 'latin1',
#     delimiter = ';',
#     skiprows = [0, 2, 3, 4, 5],
#     usecols = ['Schlüssel', 'NNF', 'Ausl. N-0 vor Opt.', ' I N-0 vor Opt.']
# )

# for index in dfAusgabeZweige.index:
#     name = dfAusgabeZweige.loc[index, 'Schlüssel']
#     if len(re.findall("Trafo", name)) > 0:
#         if len(re.findall("reset", name)) > 0:
#             dfAusgabeZweige.loc[index, 'Schlüssel'] = 'Trafo' + re.findall("Trafo(.*)_reset", name)[0]
#         else:
#             dfAusgabeZweige.loc[index, 'Schlüssel'] = 'Trafo' + re.findall("Trafo(.*)", name)[0]
#     if name[:4] == 'L-··':
#         if len(re.findall("reset", name)) > 0:
#             dfAusgabeZweige.loc[index, 'Schlüssel'] = re.findall("L-··(.*)_reset", name)[0]
#         else:
#             dfAusgabeZweige.loc[index, 'Schlüssel'] = re.findall("L-··(.*)·", name)[0]
#
# dfAusgabeZweige2 = pd.read_pickle(r"E:\Quellen\Studenten\Gajewski\inputs/Ausgabe_Zweige.pkl")
# dfAusgabeZweige1 = pd.read_pickle(r"E:\Quellen\Studenten\Gajewski\inputs_186/Ausgabe_Zweige.pkl")
#
# num_assets = int(len(dfAusgabeZweige.index)/50)
# dfAusgabeZweige_all = pd.DataFrame(np.zeros((num_assets*8760, len(dfAusgabeZweige.columns))), columns = dfAusgabeZweige2.columns)
#
# for iCounter, index in enumerate(dfClusterAssignment.index):
#     dfAusgabeZweige_all.loc[244*iCounter:244*(iCounter+1), 'Time_step'] = dfClusterAssignment.index[iCounter]
#     nnf = dictClAs[index]
#     dfAusgabeZweige_all.loc[244*iCounter:244*(iCounter+1), ['Name', 'Loading_percent', 'Loading_A']] = \
#         dfAusgabeZweige.loc[dfAusgabeZweige.loc[:, 'NNF'].values == nnf[0],
#                             ['Schlüssel', 'Ausl. N-0 vor Opt.', ' I N-0 vor Opt.']].values
#
# dfAusgabeZweige_all.to_pickle(save_path + "Ausgabe_Zweige.pkl")


dfKnoten = pd.read_csv(
    sensi_path + "Ergebnis_Marktsimulation/" + "Ausgabe_Knoten.csv",
    encoding = 'latin1',
    delimiter = ';',
    skiprows = [0, 2, 3, 4, 5],
    usecols = ['Schlüssel', 'Bez. SO', 'Un', ' Netzgr.']
)

print("Hello World")